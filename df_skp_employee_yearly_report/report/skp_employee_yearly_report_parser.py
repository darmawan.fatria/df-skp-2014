import pooler
import time
from datetime import datetime
import tools
import logging
from report import report_sxw
from datetime import datetime
from report_webkit import webkit_report

class skp_employee_yearly_report_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context=None):
        super(skp_employee_yearly_report_parser, self).__init__(cr, uid, name, context=context)
    def get_skp_employee_yearly_recapitulation_report_raw(self,filters,context=None):
        
        
        period_year=filters['form']['period_year']
        company_id=filters['form']['company_id']
        biro_id=filters['form']['biro_id']
        kepala_opd=filters['form']['is_kepala_opd']
        data_filter= [] 
        where_clause=" and 1=1 "
        
        if kepala_opd:
            where_clause = where_clause + " and p.is_kepala_opd "
        
        if company_id:
            if company_id[0] != 1:
               where_clause = where_clause + " and p.company_id =  "+str(company_id[0])
        if biro_id:
                where_clause = where_clause + " and p.biro_id =  "+str(biro_id[0])
                
        domain = period_year,period_year,
            
        query = """
select p.name,p.nip,
                   c.name OPD,
                   biro.name biro,
                   dept.name Unit_Kerja,
                   job.name Jabatan,
				   es.name eselon,
                   sum(recap.jml_skp) jumlah_skp,
                   sum(recap.jml_skp_done) jumlah_skp_yg_dinilai,
                   sum(recap.nilai_skp),
                   sum(recap.fn_nilai_tambahan),
                   sum(recap.fn_nilai_kreatifitas),
                   sum(r_month.jml_perilaku) jml_perilaku_yg_dinilai,
                   sum(r_month.nilai_pelayanan) nilai_pelayanan,
                   sum(r_month.nilai_disiplin) nilai_disiplin,
                   sum(r_month.nilai_integritas) nilai_integritas,
                   sum(r_month.nilai_komitmen) nilai_komitmen,
                   sum(r_month.nilai_kerjasama) nilai_kerjasama,
                   sum(r_month.nilai_kepemimpinan) nilai_kepemimpinan,
                   sum(r_month.nilai_perilaku) nilai_perilaku,
                   sum(recap.nilai_skp_percent) nilai_skp_percent,
                   sum(r_month.nilai_perilaku_percent) nilai_perilaku_percent,
                   sum(r_month.nilai_perilaku_percent) + sum(recap.nilai_skp_percent)  nilai_total,
                   p.user_id
                   
            from res_partner p
            left join res_company c on c.id = p.company_id
            left join hr_employee_biro biro on biro.id = p.biro_id
			left join hr_employee_eselon es on es.id = p.eselon_id
            left join hr_department dept on dept.id = p.department_id
            left join hr_job job on  job.id = p.job_id   
            left join skp_employee_yearly_recapitulation recap on recap.user_id = p.user_id and recap.target_period_year = %s
            left join 
            (select r_month.user_id,r_month.target_period_year,
		  sum(r_month.jml_perilaku) jml_perilaku ,
		   sum(r_month.nilai_pelayanan)/12 nilai_pelayanan ,
                   sum(r_month.nilai_disiplin)/12 nilai_disiplin,
                   sum(r_month.nilai_integritas)/12 nilai_integritas,
                   sum(r_month.nilai_komitmen)/12 nilai_komitmen,
                   sum(r_month.nilai_kerjasama)/12 nilai_kerjasama,
                   sum(r_month.nilai_kepemimpinan)/12 nilai_kepemimpinan,
                   sum(r_month.nilai_perilaku)/12 nilai_perilaku,
                   sum(r_month.nilai_perilaku_percent)/12 nilai_perilaku_percent
	     from skp_employee r_month
	     group by r_month.user_id,r_month.target_period_year
                   ) r_month
            on r_month.user_id = p.user_id and r_month.target_period_year = %s
            where p.employee 
            and   p.user_id notnull
            """+where_clause+""" 
            group by p.name,p.nip,
                   c.name,
                   biro.name,
                   dept.name,
                   job.name,es.name,
                   p.user_id  
        """
        list_data = []
        self.cr.execute(query,domain)
        result = self.cr.fetchall()
        no_idx=1;
        for  employee_name,employee_nip,company_name,biro,unit_kerja,jabatan,eselon, \
        jumlah_skp,jumlah_skp_yg_dinilai,nilai_skp,fn_nilai_tambahan,fn_nilai_kreatifitas, \
        jml_perilaku_yg_dinilai,nilai_pelayanan,nilai_disiplin,nilai_integritas,nilai_komitmen,nilai_kerjasama,nilai_kepemimpinan, \
        nilai_perilaku,nilai_skp_percent,nilai_perilaku_percent,nilai_total, \
        user_id in result:
            new_dict = {}
            new_dict['no_idx'] = no_idx
            new_dict['employee_name'] = employee_name
            new_dict['employee_nip'] = employee_nip
            new_dict['company_name'] = company_name
            new_dict['biro'] = biro
            new_dict['unit_kerja'] = unit_kerja
            new_dict['jabatan'] = jabatan
            new_dict['eselon'] = eselon
            
            new_dict['jumlah_skp'] = jumlah_skp
            new_dict['jumlah_skp_yg_dinilai'] = jumlah_skp_yg_dinilai
            new_dict['nilai_skp'] = nilai_skp
            new_dict['fn_nilai_tambahan'] = fn_nilai_tambahan
            new_dict['fn_nilai_kreatifitas'] = fn_nilai_kreatifitas
            new_dict['jml_perilaku_yg_dinilai'] = jml_perilaku_yg_dinilai
            new_dict['nilai_pelayanan'] = nilai_pelayanan
            new_dict['nilai_disiplin'] = nilai_disiplin
            new_dict['nilai_integritas'] = nilai_integritas
            new_dict['nilai_komitmen'] = nilai_komitmen
            new_dict['nilai_kerjasama'] = nilai_kerjasama
            new_dict['nilai_kepemimpinan'] = nilai_kepemimpinan
            new_dict['nilai_perilaku'] = nilai_perilaku
            new_dict['nilai_skp_percent'] = nilai_skp_percent
            new_dict['nilai_perilaku_percent'] = nilai_perilaku_percent
            new_dict['nilai_total'] = nilai_total
            
            list_data.append(new_dict)
            no_idx+=1
        return list_data
    
    def get_skp_employee_yearly_report_raw(self,filters,context=None):
        skp_employee_pool = self.pool.get('skp.employee.yearly.recapitulation')
        
        period_year=filters['form']['period_year']
        company_id=filters['form']['company_id']
        biro_id=filters['form']['biro_id']
        
        data_filter= [] 
        
        if company_id :
            data_filter = [
                          ('target_period_year','=','2014'),
                          ('employee_company_id','=',company_id[0]),]
        else :
          if self.uid == 1:
            data_filter = [
                          ('target_period_year','=',period_year),
                         ]

          else :
            users_pool = self.pool.get('res.users')
            user_uid_obj = users_pool.browse(self.cr,self.uid,self.uid,context=None);
            company_to_find =[];
            company_to_find.append(user_uid_obj.company_id.id);
            for child_company in user_uid_obj.company_id.child_ids:
                company_to_find.append(child_company.id);
                if child_company.child_ids:
                    for child_of_mine in child_company.child_ids:
                        company_to_find.append(child_of_mine.id);
            data_filter = [
                           ('target_period_year','=',period_year),
                           ('employee_company_id','in',company_to_find),];
        
        if biro_id:
            data_filter.append(('biro_id','=',biro_id[0]),)
        
        
        skp_employee_ids = skp_employee_pool.search(self.cr,self.uid,data_filter);
        results = skp_employee_pool.browse(self.cr, self.uid, skp_employee_ids)
        return results
    def get_opd_name_filter(self,prefiks,filters):
        if not filters['form']['company_id'] : return prefiks;

        title_company_name = filters['form']['company_id'][1]
        return prefiks+ " " + title_company_name
    def get_title(self,prefiks,filters):
        if not filters['form']['company_id'] : return prefiks;

        title_company_name = filters['form']['company_id'][1]
        return prefiks+ " " + title_company_name
    def get_period(self,filters):
        bulan ='';
        year='';
        if filters['form']['period_year']:
            year = filters['form']['period_year']
        return bulan+ " " + year
        
   
    
