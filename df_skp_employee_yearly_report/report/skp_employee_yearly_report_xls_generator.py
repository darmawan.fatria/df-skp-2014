import time
import xlwt
import cStringIO
from xlwt import Workbook, Formula
from report_engine_xls import report_xls
import skp_employee_yearly_report_xls_generator
import skp_employee_yearly_report_parser
import sys
from skp_employee_yearly_report_parser import skp_employee_yearly_report_parser

class skp_employee_yearly_report_xls_generator(report_xls):
	
    def generate_xls_report(self, parser, filters, obj, workbook):
	worksheet = workbook.add_sheet(('Rekapitulasi Kinerja Tahunan'))
        worksheet.panes_frozen = True
        worksheet.remove_splits = True
        worksheet.portrait = True # Landscape
        worksheet.fit_wiresult_datah_to_pages = 1
        worksheet.col(1).wiresult_datah = len("ABCDEFG")*1024
        int_number_style=  xlwt.easyxf("borders: top thin, bottom thin, left thin, right thin;",num_format_str='#,##0;(#,##0)')
		
	# Specifying columns, the order doesn't matter
	# lamda d,f,p: is a function who has filter,data,parser as the parameters it is expected to the value of the column
        cols_specs = [
	    
	    
	    # Infos
	    ('Company', 22, 0, 'text', lambda x, d, p: p.get_title('',filters)),
	    ('Title',  22, 0, 'text', lambda x, d, p: 'REKAPITULASI PENILAIAN PRESTASI KERJA TAHUNAN'),
	    ('Period',  22, 0, 'text', lambda x, d, p: p.get_period(filters)),
	    
	    

	    # Main Headers / Rows
	    
	    ('No', 1, 20, 'number', lambda x, d, p: 0,xlwt.Row.set_cell_number,int_number_style),
	    ('Nama Pegawai', 1, 250, 'text', lambda x, d, p:  d['employee_name'] or '' ),
	    ('NIP', 1, 100, 'text', lambda x, d, p:  d['employee_nip'] or '' ),
	    ('Jabatan', 1, 150, 'text', lambda x, d, p:  d['jabatan'] or ''),
	    ('Nama OPD', 1, 150, 'text', lambda x, d, p:  d['company_name'] or ''),
	    ('Unit Kerja', 1, 150, 'text', lambda x, d, p:  d['unit_kerja'] or '' ),
	    ('Biro', 1, 150, 'text', lambda x, d, p:  d['biro'] or '' ),
		('Eselon', 1, 150, 'text', lambda x, d, p:  d['eselon'] or '' ),
	    
	    
	    ('Nilai SKP (%)', 1, 50, 'number', lambda x, d, p:  d['nilai_skp_percent'] ),
	    ('Jumlah SKP', 1, 50, 'number', lambda x, d, p:  d['jumlah_skp'],xlwt.Row.set_cell_number,int_number_style ),
	    ('Nilai SKP', 1, 50, 'number', lambda x, d, p:  d['nilai_skp'] ),
	    ('Jumlah SKP Yg Selesai', 1, 50, 'number', lambda x, d, p:  d['jumlah_skp_yg_dinilai'],xlwt.Row.set_cell_number,int_number_style ),
	    ('Jumlah Perilaku Yg Selesai', 1, 50, 'number', lambda x, d, p:  d['jml_perilaku_yg_dinilai'],xlwt.Row.set_cell_number,int_number_style ),
	    ('Nilai Perilaku', 1, 50, 'number', lambda x, d, p:  d['nilai_perilaku'] ),
	    ('Nilai Perilaku (%)', 1, 50, 'number', lambda x, d, p:  d['nilai_perilaku_percent'] ),
	    ('Nilai Tambahan', 1, 50, 'number', lambda x, d, p:  d['fn_nilai_tambahan'] ),
	    ('Nilai Kreatifitas', 1, 50, 'number', lambda x, d, p:  d['fn_nilai_kreatifitas'] ),
	    ('Nilai Total', 1, 75, 'number', lambda x, d, p:  d['nilai_total'] ),
	    
	    ('Orientasi Pelayanan', 1, 50, 'number', lambda x, d, p:  d['nilai_pelayanan'] ),
	    ('Integritas',1,50, 'number', lambda x, d, p:  d['nilai_integritas'] ),
	    ('Komitmen',1,50, 'number', lambda x, d, p:  d['nilai_komitmen'] ),
	    ('Disiplin',1,50, 'number', lambda x, d, p:  d['nilai_disiplin'] ),
	    ('Kerjasama',1,50, 'number', lambda x, d, p:  d['nilai_kerjasama'] ),
	    ('Kepemimpinan',1,50, 'number', lambda x, d, p:  d['nilai_kepemimpinan'] ),
	    
        # Misc
	    ('single_empty_column', 1, 0, 'text', lambda x, d, p: ''),
	    ('triple_empty_column', 3, 0, 'text', lambda x, d, p: ''),
	    ('quadruple_empty_column', 4, 0, 'text', lambda x, d, p: ''),
	]
    

        row_spec_value = ['No','Nama Pegawai','NIP','Jabatan','Eselon','Unit Kerja','Biro','Nama OPD',
						  		'Jumlah SKP Yg Selesai','Jumlah SKP','Nilai SKP','Nilai Tambahan','Nilai Kreatifitas',
						  		'Jumlah Perilaku Yg Selesai','Nilai Perilaku',
						  		'Orientasi Pelayanan','Integritas','Komitmen','Disiplin','Kerjasama','Kepemimpinan',
						  		'Nilai SKP (%)','Nilai Perilaku (%)',
						  		'Nilai Total']
        # Row templates (Order Matters, this joins the columns that are specified in the second parameter)
        company_template = self.xls_row_template(cols_specs, ['single_empty_column','Company'])
        title_template = self.xls_row_template(cols_specs, ['single_empty_column','Title'])
        period_template = self.xls_row_template(cols_specs, ['single_empty_column','Period'])
        row_template = self.xls_row_template(cols_specs,row_spec_value)
        empty_row_template = self.xls_row_template(cols_specs, ['single_empty_column'])

        # Styles (It's used for writing rows / headers)
        row_normal_style=  xlwt.easyxf('borders: top thin, bottom thin, left thin, right thin;',num_format_str='#,##0.00;(#,##0.00)')
        row_normal_odd_style=  xlwt.easyxf('pattern: pattern solid, fore_color silver_ega;',num_format_str='#,##0.00;(#,##0.00)')
        info_style = xlwt.easyxf('font: height 200, name Arial, colour_index black, bold on, italic off; align: wrap on, vert centre,horiz centre;pattern: pattern solid, fore_color white;', num_format_str='#,##0.00;(#,##0.00)')
        top_style = xlwt.easyxf('font: height 200, name Arial, colour_index white, bold on, italic off; align: wrap on, vert centre, horiz center;pattern: pattern solid, fore_color orange;', num_format_str='#,##0.00;(#,##0.00)')
        header_style = xlwt.easyxf('font: height 200, name Arial, colour_index black, bold on, italic off; align: wrap on, vert centre;pattern: pattern solid, fore_color white;borders: top thin, bottom thin, left thin, right thin;', num_format_str='#,##0.00;(#,##0.00)')
        sum_style = xlwt.easyxf('font: height 200, name Arial, colour_index white, bold on, italic off; align: wrap on, vert centre;pattern: pattern solid, fore_color gray50;', num_format_str='#,##0.00;(#,##0.00)')

        # Write infos
        # xls_write_row(worksheet, filters, data parser, row_number, template, style)
        self.xls_write_row(worksheet, filters, None, parser, 0, title_template, info_style)
        self.xls_write_row(worksheet, filters, None, parser, 1, company_template, info_style)
        self.xls_write_row(worksheet, filters, None, parser, 2, period_template, info_style)
        

        
            # Write headers (It uses the first parameter of cols_specs)
        self.xls_write_row_header(worksheet, 4, row_template, header_style, set_column_size=True)

        row_count = 5
        result = parser.get_skp_employee_yearly_recapitulation_report_raw(filters);
        style = row_normal_style
        idx=1
        for skp_employee_yearly_data in result:
            # Write Rows
              	self.xls_write_row_with_indeks(worksheet, filters, skp_employee_yearly_data, parser, row_count, row_template, style,idx)
              	idx+=1
						
                row_count+=1

        
    # Override from report_engine_xls.py	
    def create_source_xls(self, cr, uid, ids, filters, report_xml, context=None): 
        if not context: context = {}
	
	# Avoiding context's values change
        context_clone = context.copy()
	
        rml_parser = self.parser(cr, uid, self.name2, context=context_clone)
        objects = self.getObjects(cr, uid, ids, context=context_clone)
        rml_parser.set_context(objects, filters, ids, 'xls')
        io = cStringIO.StringIO()
        workbook = xlwt.Workbook(encoding='utf-8')
        self.generate_xls_report(rml_parser, filters, rml_parser.localcontext['objects'], workbook)
        workbook.save(io)
        io.seek(0)
        return (io.read(), 'xls')

#Start the reporting service
skp_employee_yearly_report_xls_generator(
    #name (will be referred from skp_employee_yearly_report.py, must add "report." as prefix)
    'report.skp.employee.yearly.xls',
    #model
    'skp.employee.yearly.report',
    #file
    'addons/df_skp_employee_yearly_report/report/skp_employee_yearly_report.xls',
    #parser
    parser=skp_employee_yearly_report_parser,
    #header
    header=True
)