from osv import fields, osv
import time

class skp_employee_yearly_report(osv.osv_memory):
    
    _name = "skp.employee.yearly.report"
    _columns = {
        'company_id'        : fields.many2one('res.company', 'OPD', ),
        'biro_id'        : fields.many2one('hr.employee.biro', 'Biro'),
        'is_kepala_opd'   : fields.boolean('Hanya Kepala OPD'),
        'period_year'       : fields.char('Periode Tahun', size=4, required=True),
    }
    _defaults = {
        'period_year':lambda *args: time.strftime('%Y'),
        'is_kepala_opd':False
        
    }
    
    def get_skp_employee_yearly_report(self, cr, uid, ids, context={}):
        value = self.read(cr, uid, ids)[0]
        datas = {
            'ids': context.get('active_ids',[]),
            'model': 'skp.employee.yearly.report',
            'form': value
        }
	
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'skp.employee.yearly.xls',
            'report_type': 'webkit',
            'datas': datas,
        }
    
skp_employee_yearly_report()
