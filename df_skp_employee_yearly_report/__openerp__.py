{
    "name": "Rekapitulasi SKP Tahunan",
    "version": "1.0",
    "author": "Darmawan Fatriananda",
    "category": "Penilaian Prestasi Kerja / Rekapitulasi",
    "description": "Rekapitulasi SKP Tahunan",
    "website" : "www.mediasee.net",
    "license" : "GPL-3",
    "depends": ['df_skp_employee',],
    'update_xml': ["skp_employee_yearly_report.xml",],
    'demo_xml': [],
    'installable': True,
    'active': False,
}
