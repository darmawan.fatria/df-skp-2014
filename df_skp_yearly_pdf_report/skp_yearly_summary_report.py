import time
import netsvc
from tools.translate import _
import tools
from datetime import datetime, timedelta
from osv import fields, osv
from dateutil.relativedelta import relativedelta


import decimal_precision as dp

class skp_yearly_summary_report(osv.osv_memory):
    _name = "skp.yearly.summary.report"
    
    _columns = {
        'period_year'       : fields.char('Periode Tahun', size=4, required=True),
        'print_date'       : fields.date('Tanggal Pembuatan',  required=True),
        'user_id'        : fields.many2one('res.users', 'Pegawai Yang Dinilai'),
    } 
    _defaults = {
        'period_year':lambda *args: time.strftime('%Y'),
        'print_date': lambda *args: time.strftime('%Y-%m-%d'),
        'user_id': lambda self, cr, uid, ctx: uid,
    }
    def compute_rep(self, cr, uid, ids, context={}):
        value = self.read(cr, uid, ids)[0]
        
        if value:
            period_year = value['period_year']
            user_id = value['user_id'][0]
            skp_yearly_pool = self.pool.get('skp.employee.yearly.recapitulation')
            skp_yearly_ids=skp_yearly_pool.search(cr, uid, [('target_period_year','=',period_year),('user_id','=',user_id)], context=None)
            if not skp_yearly_ids :
                raise osv.except_osv(_('Laporan Tidak Bisa Di Download'),
                                        _('Belum ada rekapitulasi Tahunan pegawai.'))
        datas = {
             'ids': context.get('active_ids',[]),
             'model': 'skp.yearly.summary.report',
             'form': value
               }
        
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'skp.yearly.summary.report.form',
            #'report_type': 'webkit',
            'datas': datas,
            }
skp_yearly_summary_report()