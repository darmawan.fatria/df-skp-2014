from openerp.report import report_sxw
import pooler
from datetime import datetime,timedelta
from dateutil.relativedelta import relativedelta
import time
from operator import itemgetter
from itertools import groupby
from report_webkit import webkit_report
from osv import fields, osv
from tools.translate import _
import netsvc
import tools
import decimal_precision as dp
import math
import locale

class skp_yearly_summary_pdf_report(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context=None):
        super(skp_yearly_summary_pdf_report, self).__init__(cr, uid, name, context=context)
        locale.setlocale(locale.LC_ALL, 'id_ID.utf8')
        self.localcontext.update({
            'get_atribut_kepegawaian' : self.get_atribut_kepegawaian,
            'get_concat_nilai_skp' : self.get_concat_nilai_skp,
            'get_recap_yearly_report_raw' : self.get_recap_yearly_report_raw,
            'get_recap_perilaku_yearly_report_raw':self.get_recap_perilaku_yearly_report_raw,
            'parsing_nilai_kepemimpinan':self.parsing_nilai_kepemimpinan,
            'get_rata_rata_nilai_perilaku':self.get_rata_rata_nilai_perilaku,
             'get_concat_nilai_perilaku':self.get_concat_nilai_perilaku,
            'get_concat_with_format_date':self.get_concat_with_format_date, 
            'get_format_date':self.get_format_date,
            'get_total_nilai':self.get_total_nilai,
            'parsing_indeks_nilai':self.parsing_indeks_nilai
            
        })
        
    def get_atribut_kepegawaian(self,filters,context=None):
        period_year=filters['form']['period_year']
        user_id = False
        data_pegawai=None
        if filters['form']['user_id']:
            user_id=filters['form']['user_id'][0]
        user_pool = self.pool.get('res.users')
        if user_id:
            result = user_pool.browse(self.cr, self.uid, user_id)
            if  result and result.partner_id:
                data_pegawai = result.partner_id
        return data_pegawai;
    def get_recap_yearly_report_raw(self,filters,context=None):
        period_year=filters['form']['period_year']
        user_id = False
        if filters['form']['user_id']:
            user_id=filters['form']['user_id'][0]
        skp_yearly_pool = self.pool.get('skp.employee.yearly.recapitulation')
        skp_yearly_ids=skp_yearly_pool.search(self.cr, self.uid, [('target_period_year','=',period_year),('user_id','=',user_id)], context=None)
        results = skp_yearly_pool.browse(self.cr, self.uid, skp_yearly_ids)
        if results:
            return results[0];
        
    def get_recap_perilaku_yearly_report_raw(self,filters,context=None):
        period_year=filters['form']['period_year']
        user_id = False
        if filters['form']['user_id']:
            user_id=filters['form']['user_id'][0]
        
        nilai_skp_tambahan_percent=nilai_skp=jml_skp=nilai_skp_percent=jml_perilaku=nilai_perilaku=nilai_perilaku_percent=0
        count=nilai_pelayanan=nilai_integritas=nilai_komitmen=nilai_disiplin=nilai_kerjasama=nilai_kepemimpinan=0
        nilai_total=0
        skp_employee_pool = self.pool.get('skp.employee')
        partner_pool = self.pool.get('res.partner')
        skp_yearly_ids=skp_employee_pool.search(self.cr, self.uid, [('target_period_year','=',period_year),('user_id','=',user_id)], context=None)
        data = {}
        data['indeks_nilai_pelayanan']=''
        data['indeks_nilai_integritas']=''
        data['indeks_nilai_komitmen']=''
        data['indeks_nilai_disiplin']=''
        data['indeks_nilai_kerjasama']=''
        data['indeks_nilai_kepemimpinan']=''
        data['nilai_perilaku_percent']=0
        data['nilai_perilaku']=0
        data['nilai_perilaku_percent']=0
        data['nilai_pelayanan']=0
        data['nilai_integritas']=0
        data['nilai_komitmen']=0
        data['nilai_disiplin']=0
        data['nilai_kerjasama']=0
        employee_id=None
        for skp_obj in  skp_employee_pool.read(self.cr, self.uid, skp_yearly_ids, ['id','jml_perilaku','nilai_perilaku','nilai_perilaku_percent'
                                                                           ,'nilai_pelayanan','nilai_integritas','nilai_komitmen'
                                                                           ,'nilai_disiplin','nilai_kerjasama','nilai_kepemimpinan'
                                                                           ,'nilai_total'
                                                                           ,'employee_id'
                                                                           ], context=context) :
            if skp_obj['jml_perilaku'] and skp_obj['jml_perilaku'] > 0 :
               jml_perilaku+=skp_obj['jml_perilaku']
               nilai_perilaku+=skp_obj['nilai_perilaku']
               nilai_perilaku_percent+=skp_obj['nilai_perilaku_percent']
               nilai_pelayanan+=skp_obj['nilai_pelayanan']
               nilai_integritas+=skp_obj['nilai_integritas']
               nilai_komitmen+=skp_obj['nilai_komitmen']
               nilai_disiplin+=skp_obj['nilai_disiplin']
               nilai_kerjasama+=skp_obj['nilai_kerjasama']
               nilai_kepemimpinan+=skp_obj['nilai_kepemimpinan']
               employee_id = skp_obj['employee_id'] and skp_obj['employee_id'][0]
               count+=1
        jml_perilaku=12
        rata_rata=5
        total_perilaku = 0
        if count >0 :
                data['nilai_perilaku']=nilai_perilaku/jml_perilaku
                data['nilai_perilaku_percent']=nilai_perilaku_percent/jml_perilaku
                data['nilai_pelayanan']=nilai_pelayanan/jml_perilaku
                data['nilai_integritas']=nilai_integritas/jml_perilaku
                data['nilai_komitmen']=nilai_komitmen/jml_perilaku
                data['nilai_disiplin']=nilai_disiplin/jml_perilaku
                data['nilai_kerjasama']=nilai_kerjasama/jml_perilaku
                total_perilaku = data['nilai_pelayanan']+data['nilai_integritas']+data['nilai_komitmen']+data['nilai_disiplin']+data['nilai_kerjasama']
                data['nilai_kepemimpinan'] =None
                data['indeks_nilai_kepemimpinan']=''
                if employee_id:
                    job_type=''
                    
                    for employee_id_obj in partner_pool.read(self.cr, self.uid, [employee_id], ['id','job_type',], context=context) :
                        job_type = employee_id_obj['job_type']
                    
                    if job_type == 'struktural': 
                        data['nilai_kepemimpinan']=nilai_kepemimpinan/jml_perilaku
                        data['indeks_nilai_kepemimpinan']=self.get_indeks_nilai(self.cr,self. uid, data['nilai_kepemimpinan'], context)
                        total_perilaku = total_perilaku +data['nilai_kepemimpinan']
                        rata_rata=6
                   
                data['indeks_nilai_pelayanan']=self.get_indeks_nilai(self.cr,self. uid, data['nilai_pelayanan'], context)
                data['indeks_nilai_integritas']=self.get_indeks_nilai(self.cr,self. uid, data['nilai_integritas'], context)
                data['indeks_nilai_komitmen']=self.get_indeks_nilai(self.cr,self. uid, data['nilai_komitmen'], context)
                data['indeks_nilai_disiplin']=self.get_indeks_nilai(self.cr,self. uid, data['nilai_disiplin'], context)
                data['indeks_nilai_kerjasama']=self.get_indeks_nilai(self.cr,self. uid, data['nilai_kerjasama'], context)
                
                data['total_perilaku']=total_perilaku
                data['rata_rata_total_perilaku']=total_perilaku/rata_rata
                data['nilai_perilaku_percent'] = data['rata_rata_total_perilaku']*0.4
        return data
    def get_concat_nilai_perilaku(self,nilai_perilaku,pengali,context=None):
        str_nilai_perilaku=''
        
        if nilai_perilaku:
            str_nilai_perilaku = nilai_perilaku
        ret_skp = "%s x %s" % (str_nilai_perilaku, pengali)
        return ret_skp
    def get_concat_nilai_skp(self,nilai_skp,pengali,tugas_tambahan,kreatifitas,context=None):
        str_nilai_skp='-'
        str_tugas_tambahan='0'
        str_kreatifitas='0'
        if nilai_skp:
            str_nilai_skp = nilai_skp
        if tugas_tambahan:
            str_tugas_tambahan = str(int(tugas_tambahan))
        if kreatifitas:
            str_kreatifitas = str(int(kreatifitas))
        ret_skp = "(%s + %s + %s ) x %s " % (str_nilai_skp, str_tugas_tambahan,str_kreatifitas,pengali)
        return ret_skp
    def get_total_nilai(self,nilai_skp_percent,nilai_perilaku_percent):
        return nilai_skp_percent+nilai_perilaku_percent
    def parsing_indeks_nilai(self,nilai):
        ret_val=''
        lookup_nilai_pool = self.pool.get('acuan.penailaian')
        lookup_nilai_id = lookup_nilai_pool.search(self.cr, self.uid, [('kategori_nilai', '=', 'threshold'), ('active', '=', True), ('type', '=', 'lain_lain')
                                                            , ('code', 'in', ('index_nilai_a','index_nilai_b','index_nilai_c','index_nilai_d','index_nilai_e'))
                                                            ,('nilai_bawah', '<=', nilai), ('nilai_atas', '>=', nilai)], context=None)
        if lookup_nilai_id :
            for lookup_nilai_obj in lookup_nilai_pool.read(self.cr, self.uid,lookup_nilai_id, ['id','name'], context=None) :
                ret_val = lookup_nilai_obj['name']
        
        return ret_val
    def parsing_nilai_kepemimpinan(self,skp_yearly_obj,type):
       val =None
       if skp_yearly_obj and skp_yearly_obj.employee_id and skp_yearly_obj.employee_id.job_type == 'struktural' :
            if type==1 :
                val = skp_yearly_obj.nilai_kepemimpinan
            if type==2 :
                val = skp_yearly_obj.indeks_nilai_kepemimpinan
       return val
    def get_rata_rata_nilai_perilaku(self,skp_yearly_obj):
       val =0
       if not skp_yearly_obj : return 0;
       if skp_yearly_obj and skp_yearly_obj.total_perilaku == 0 : return 0;
       
       if skp_yearly_obj and skp_yearly_obj.employee_id and skp_yearly_obj.employee_id.job_type == 'struktural' :
           val = skp_yearly_obj.total_perilaku/6
       else :
            val = skp_yearly_obj.total_perilaku/5
            
       return val
       
    def get_concat_with_format_date(self,str_prefix,filters,context=None):
        print_date=filters['form']['print_date'];
        try:
            formatted_print_date = time.strftime('%d %B %Y', time.strptime(print_date,'%Y-%m-%d'))
        except:
            formatted_print_date=''
        return str_prefix+' '+formatted_print_date
    def get_format_date(self,print_date,addDays,context=None):
        dateFormat='%d %B %Y'
        try :
            timeNow = datetime.strptime(print_date,'%Y-%m-%d')
            if addDays!=0 :
                anotherTime = timeNow + timedelta(days=addDays)
            else :
                anotherTime =timeNow
            formatted_print_date = anotherTime.strftime(dateFormat)
        except :
            formatted_print_date=''
        return formatted_print_date
    def get_indeks_nilai(self,cr,uid,nilai,context=None):
        ret_val=''
        lookup_nilai_pool = self.pool.get('acuan.penailaian')
        lookup_nilai_id = lookup_nilai_pool.search(self.cr, self.uid, [('kategori_nilai', '=', 'threshold'), ('active', '=', True), ('type', '=', 'lain_lain')
                                                            , ('code', 'in', ('index_nilai_a','index_nilai_b','index_nilai_c','index_nilai_d','index_nilai_e'))
                                                            ,('nilai_bawah', '<=', nilai), ('nilai_atas', '>=', nilai)], context=None)
        if lookup_nilai_id :
            for lookup_nilai_obj in lookup_nilai_pool.read(self.cr, self.uid,lookup_nilai_id, ['id','name'], context=context) :
                ret_val = lookup_nilai_obj['name']
        
        return ret_val
    
report_sxw.report_sxw('report.skp.yearly.summary.report.form',
                        'skp.yearly.summary.report', 
                        'addons/df_skp_recap_yearly_pdf_report/report/skp_yearly_summary_pdf_report.mako', parser=skp_yearly_summary_pdf_report)
# report_sxw.report_sxw('report.penjualan.form', 'report.keuangan', 'addons/ad_laporan_keuangan/report/salesreport.mako', parser=ReportKeu)
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
