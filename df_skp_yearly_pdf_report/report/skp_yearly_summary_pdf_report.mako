<!DOCTYPE html SYSTEM "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<style>
body{
    font-family: verdana,arial,sans-serif;
	font-size:12px;
    word-wrap: break-word;
}

table.header_table {
	width: 100%;
    border-width: 0px;
	border-collapse: collapse;
    font-size:15px;
    
    
}
table.header_table td {
	padding: 5px;
    text-align: center;
}
table.sub_header_table {
    width: 100%;
	border-collapse: collapse;
    font-size:13px;
}
table.sub_header_table td {
	padding: 5px;
    text-align: left;
}
table.content_summary {
	width: 100%;
    border-collapse: collapse !important;    
    
}
table.content_summary td {
    border: 1px solid #000 !important;
    padding: 3px;
    vertical-align: top;
    text-align: left;
}
.atribut_peg_no{
    width:6px;
}
.atribut_peg_data{
     width:70%;
}
.nilai_summary_no{
    width:6px;
}    
.text_left{
	text-align: left;
}
.text_center{
	text-align: center;
}
.to_ttd_tgl{
	font-size:10px;
    padding-top: 100px;
    padding-left:400px;
    padding-bottom:10px;
}
.ttd_right{
    font-size:10px;
    padding-left: 400px;
    text-align: center;
    padding-bottom: 10px;
    
}
.ttd_left{
    font-size:10px;
    padding-right: 400px;   
    text-align: center;
    padding-bottom: 10px;
    
}
.ttd_tgl{
	font-size:10px;
     padding-top: 100px; 
     padding-left:30px;
     padding-bottom:10px;
}
.break_me { page-break-before: always; }
</style>
</head>
<body>
<%
		e = get_atribut_kepegawaian(data)
		data_rekap_tahunan_pegawai = get_recap_yearly_report_raw(data);
		data_rekap_perilaku_tahunan = get_recap_perilaku_yearly_report_raw(data)
%>
    <table class="header_table">
            <tr>
                <td>FORMULIR PENILAIAN PRESTASI KERJA</td>
            </tr>
            <tr>
                <td  > PEGAWAI NEGERI SIPIL</td>
            </tr>
    </table>
    <br /><br />
    <table class="sub_header_table">
            <tr>
                 <td  width="50%"> </td>
                 <td>  JANGKA WAKTU PENILAIAN   </td>
            </tr>
            <tr>
                <td  >  ${e.company_id and e.company_id.name or ''} </td>
                <td>  	1 Januari sd 31 Desember  ${data['form']['period_year']}</td>
            </tr>
    </table>
    
    
    <table class="content_summary" >
        <tr>
            <td  rowspan="6" class="atribut_peg_no" >1. </td>
            <td  colspan="2">  YANG DINILAI</td>
        </tr>
        <tr>
            <td  >a. Nama</td>
            <td class="atribut_peg_data" >  ${e.fullname}</td>
        </tr>
        <tr>
             <td  >b. N I P </td>
            <td class="atribut_peg_data" >  ${e.nip}</td>
        </tr>
        <tr>
            <td  >c. Pangkat, Golongan Ruang </td>
            <td class="atribut_peg_data" >  ${  e.golongan_id and e.golongan_id.description or ''}</td>
        </tr>
        <tr>
            <td  >d. Jabatan / Pekerjaan </td>
            <td class="atribut_peg_data" >  ${e.job_id and e.job_id.name or ''}</td>
        </tr>
        <tr>
            <td  >e. Unit Organisasi </td>
            <td class="atribut_peg_data" >  ${e.department_id and e.department_id.name or ''}</td>
        </tr>
        
        <tr>
            <td   rowspan="6" class="atribut_peg_no"  >2. </td>
            <td colspan="2" width="6px"  >  PEJABAT PENILAI</td>
        </tr>
        <tr>
            <td  >a. Nama </td>
            <td class="atribut_peg_data" >  ${e.user_id_atasan and e.user_id_atasan.fullname or ''} </td>
        </tr>
        <tr>
             <td  >b. N I P </td>
            <td class="atribut_peg_data" >  ${e.user_id_atasan and e.user_id_atasan.nip or ''}   </td>
        </tr>
        <tr>
            <td  >c. Pangkat, Golongan Ruang </td>
            <td class="atribut_peg_data" >  ${e.user_id_atasan and e.user_id_atasan.golongan_id and e.user_id_atasan.golongan_id.description  or ''}  </td>
        </tr>
        <tr>
            <td  >d. Jabatan / Pekerjaan </td>
            <td class="atribut_peg_data" >  ${e.user_id_atasan and e.user_id_atasan.job_id and e.user_id_atasan.job_id.name  or ''} </td>
        </tr>
        <tr>
            <td  >e. Unit Organisasi </td>
            <td class="atribut_peg_data" >  ${e.user_id_atasan and e.user_id_atasan.department_id and e.user_id_atasan.department_id.name  or ''} </td>
        </tr>
        
        <tr>
            <td  rowspan="6" class="atribut_peg_no"   >3. </td>
            <td  colspan="2" width="6px" >  ATASAN PEJABAT PENILAI</td>
        </tr>
        <tr>
            <td  >a. Nama </td>
            <td class="atribut_peg_data" >  ${e.user_id_banding and e.user_id_banding.fullname or ''} </td>
        </tr>
        <tr>
             <td  >b. N I P </td>
            <td class="atribut_peg_data" >  ${e.user_id_banding and e.user_id_banding.nip or ''}   </td>
        </tr>
        <tr>
            <td  >c. Pangkat, Golongan Ruang </td>
            <td class="atribut_peg_data" >  ${e.user_id_banding and e.user_id_banding.golongan_id and e.user_id_banding.golongan_id.description  or ''}  </td>
        </tr>
        <tr>
            <td  >d. Jabatan / Pekerjaan </td>
            <td class="atribut_peg_data" >  ${e.user_id_banding and e.user_id_banding.job_id and e.user_id_banding.job_id.name  or ''} </td>
        </tr>
        <tr>
            <td  >e. Unit Organisasi </td>
            <td class="atribut_peg_data" >  ${e.user_id_banding and e.user_id_banding.department_id and e.user_id_banding.department_id.name  or ''} </td>
        </tr>
    </table>
    <br/>
    <table class="content_summary" >
        <tr>
            <td  rowspan="11" class="nilai_summary_no" >4. </td>
            <td  colspan="4">  UNSUR YANG DINILAI</td>
            <td style="text-align:center;" >  JUMLAH</td>
        </tr>
        <tr>
        	<%
        		str_nilai_skp = get_concat_nilai_skp(data_rekap_tahunan_pegawai and formatLang(data_rekap_tahunan_pegawai.nilai_skp),'60%',data_rekap_tahunan_pegawai and data_rekap_tahunan_pegawai.fn_nilai_tambahan, data_rekap_tahunan_pegawai and data_rekap_tahunan_pegawai.fn_nilai_kreatifitas)
        		str_nilai_perilaku = get_concat_nilai_perilaku(data_rekap_tahunan_pegawai and formatLang(data_rekap_perilaku_tahunan['rata_rata_total_perilaku']),'40%')
        	%>
            <td  style="height:40px;vertical-align: middle;" colspan="4">  a. Sasaran Kerja Pegawai (SKP) &nbsp;&nbsp; ( ${str_nilai_skp} )</td>
            <td  style="text-align:center;vertical-align: middle;" >  ${data_rekap_tahunan_pegawai and formatLang(data_rekap_tahunan_pegawai.nilai_skp_tambahan_percent) or ''} </td>
        </tr>
        <tr>
            <td  rowspan="9">  b. Perilaku Kerja</td>
            <td  >  1. Orientasi Pelayanan </td>
            <td  style="text-align:right;">   ${data_rekap_tahunan_pegawai and formatLang(data_rekap_perilaku_tahunan['nilai_pelayanan']) or ''} </td>
            <td  style="text-align:center;">  ${data_rekap_tahunan_pegawai and data_rekap_perilaku_tahunan['indeks_nilai_pelayanan'] or ''} </td>
            <td  >   </td>
        </tr>
        <tr>
            <td  >  2. Integritas </td>
            <td  style="text-align:right;">   ${data_rekap_tahunan_pegawai and formatLang(data_rekap_perilaku_tahunan['nilai_integritas']) or ''} </td>
            <td  style="text-align:center;">  ${data_rekap_tahunan_pegawai and data_rekap_perilaku_tahunan['indeks_nilai_integritas'] or ''} </td>
            <td  >   </td>
        </tr>
        <tr>
            <td  >  3. Komitmen </td>
            <td  style="text-align:right;">   ${data_rekap_tahunan_pegawai and formatLang(data_rekap_perilaku_tahunan['nilai_komitmen']) or ''} </td>
            <td  style="text-align:center;">  ${data_rekap_tahunan_pegawai and data_rekap_perilaku_tahunan['indeks_nilai_komitmen'] or ''} </td>
            <td  >   </td>
        </tr>
        <tr>
            <td  >  4. Disiplin </td>
            <td  style="text-align:right;">   ${data_rekap_tahunan_pegawai and formatLang(data_rekap_perilaku_tahunan['nilai_disiplin']) or ''} </td>
            <td  style="text-align:center;">  ${data_rekap_tahunan_pegawai and data_rekap_perilaku_tahunan['indeks_nilai_disiplin'] or ''} </td>
            <td  >   </td>
        </tr>
        <tr>
            <td  >  5. Kerjasama </td>
            <td  style="text-align:right;">   ${data_rekap_tahunan_pegawai and formatLang(data_rekap_perilaku_tahunan['nilai_kerjasama']) or ''} </td>
            <td  style="text-align:center;">  ${data_rekap_tahunan_pegawai and data_rekap_perilaku_tahunan['indeks_nilai_kerjasama'] or ''} </td>
            <td  >   </td>
        </tr>
        <tr>
            <td  >  6. Kepemimpinan </td>
            <td  style="text-align:right;">   ${data_rekap_tahunan_pegawai and data_rekap_perilaku_tahunan['nilai_kepemimpinan'] and formatLang(data_rekap_perilaku_tahunan['nilai_kepemimpinan']) or ''}</td>
            <td  style="text-align:center;">  ${data_rekap_tahunan_pegawai and data_rekap_perilaku_tahunan['indeks_nilai_kepemimpinan'] or ''} </td>
            <td  >   </td>
        </tr>
        <tr>
            <td  >  7. Jumlah </td>
            <td  style="text-align:right;">  ${ data_rekap_tahunan_pegawai and formatLang(data_rekap_perilaku_tahunan['total_perilaku']) or '' } </td>
            <td  >  - </td>
            <td  >   </td>
        </tr>
        <tr>
            <td  >  8. Nilai rata-rata </td>
            <td  style="text-align:right;">  ${ data_rekap_tahunan_pegawai and formatLang(data_rekap_perilaku_tahunan['rata_rata_total_perilaku']) or '' } </td>
            <td  >  - </td>
            <td  >   </td>
        </tr>
        <tr>
            <td  colspan="3">  9. Nilai Perilaku Kerja &nbsp;&nbsp; (${str_nilai_perilaku} )</td>
            <td  style="text-align:center;">  ${ data_rekap_tahunan_pegawai and formatLang(data_rekap_perilaku_tahunan['nilai_perilaku_percent']) or '' } </td>
            
        </tr>
        
         <tr>
            <td  style="height:40px;text-align:center;vertical-align: middle;" colspan="5" rowspan="2">  Nilai Prestasi Kerja</td>
            <td  style="text-align:center;vertical-align: middle;" >  ${ data_rekap_tahunan_pegawai and data_rekap_perilaku_tahunan and formatLang(get_total_nilai(data_rekap_tahunan_pegawai.nilai_skp_tambahan_percent,data_rekap_perilaku_tahunan['nilai_perilaku_percent'])) or '' }  </td>
        </tr>
        <tr>
            <td  style="text-align:center;vertical-align: middle;" >  ${ data_rekap_tahunan_pegawai and data_rekap_perilaku_tahunan and parsing_indeks_nilai(get_total_nilai(data_rekap_tahunan_pegawai.nilai_skp_tambahan_percent,data_rekap_perilaku_tahunan['nilai_perilaku_percent'])) or ''}  </td>
        </tr>
    </table>
    <p class="break_me"> 
    <br />
     <table class="content_summary" >
        <tr>
            <td   class="atribut_peg_no" >5. </td>
            <td  >  KEBERATAN DARI PEGAWAI NEGERI SIPIL YANG DINILAI (APABILA ADA)
                    <div class="ttd_tgl">Tanggal,&nbsp; ............................<div>                 
            </td>
        </tr>
        <tr>
            <td   class="atribut_peg_no" >6. </td>
            <td  >  TANGGAPAN PEJABAT PENILAI ATAS KEBERATAN
                    <div class="to_ttd_tgl">Tanggal,&nbsp; ............................<div>                 
            </td>
        </tr>
        <tr>
            <td   class="atribut_peg_no" >7. </td>
            <td  >  KEPUTUSAN ATASAN PEJABAT PENILAI ATAS KEBERATAN
                    <div class="to_ttd_tgl">Tanggal,&nbsp; ............................<div>                 
            </td>
        </tr>
        <tr>
            <td   class="atribut_peg_no" >8. </td>
            <td  >  REKOMENDASI
                         <br/><br/><br/><br/><br/>
            </td>
        </tr>
        <tr>
            <td   class="atribut_peg_no" >10. </td>
            <td  >  
                   <div class="ttd_right"> 
                       <div > 9. &nbsp; DIBUAT TANGGAL, ${get_format_date(data['form']['print_date'],0)}   </div>    
                       <div > PEJABAT PENILAI</div>    
                       <br/><br/><br/><br/>                     
                       <div > ( &nbsp; ${e.user_id_atasan and e.user_id_atasan.fullname or ''} &nbsp; ) </div>  
                       <div > NIP. &nbsp; ${e.user_id_atasan and e.user_id_atasan.nip or ''} </div>
                   </div>    
                    <div class="ttd_left"> 
                       <div > 10. &nbsp; DITERIMA TANGGAL,${get_format_date(data['form']['print_date'],7)} </div>    
                       <div > PEGAWAI NEGERI SIPIL YANG DINILAI</div>    
                       <br/><br/><br/><br/>                     
                       <div > ( &nbsp; ${e and e.fullname or ''} &nbsp; ) </div>  
                       <div > NIP. &nbsp; ${e and e.nip or ''} </div>
                   </div> 
                    <div class="ttd_right"> 
                       <div > 11. &nbsp; DITERIMA TANGGAL,${get_format_date(data['form']['print_date'],14)} </div>    
                       <div > ATASAN PEJABAT YANG MENILAI</div>    
                       <br/><br/><br/><br/>                     
                       <div > ( &nbsp; ${e.user_id_banding and e.user_id_banding.fullname or ''} &nbsp; ) </div>  
                       <div > NIP. &nbsp; ${e.user_id_banding and e.user_id_banding.nip or ''} </div>    
                   </div> 
            </td>
            
        </tr>
    </table>


  









</body>
</html>
