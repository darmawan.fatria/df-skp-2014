from openerp.osv import fields, osv
from datetime import datetime,timedelta
import time
from mx import DateTime
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _

# ====================== Popup Class Object ================================= #

class pengajuan_izin_belajar(osv.osv):
    _name = 'pengajuan.izin.belajar'
    _description = 'Pengjuan (Pembuatan) Izin Belajar'
    
    
    def _get_status_masa_kerja(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        
        for o in self.browse(cr, uid, ids, context=context):
            lolos_masa_kerja=False
            if o.user_id and o.user_id.partner_id:
                lolos_masa_kerja= self.validate_atribut_kepegawaian(cr,uid,o.user_id.partner_id,context)   
            
            res[skp_employee_obj.id] = lolos_masa_kerja
        return res
    def _get_masa_perkuliahan(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        masa_perkuliahan=0
        for o in self.browse(cr, uid, ids, context=context):
            if o.start_date and o.end_date:
                awal_masa_kuliah = time.strftime('%Y', time.strptime(o.start_date,'%Y-%m-%d'))
                akhir_masa_kuliah = time.strftime('%Y', time.strptime(o.end_date,'%Y-%m-%d'))
                masa_perkuliahan = int(akhir_masa_kuliah) - int(awal_masa_kuliah)
            res[o.id]=masa_perkuliahan
        return res
    _columns = {
        'formasi_id'     : fields.many2one('formasi.pendidikan.lanjutan', 'Formasi', readonly=True),
        'user_id': fields.many2one('res.users', 'Pegawai',required=True),
       
        'nama_sekolah_id': fields.many2one('hr.employee.school', 'Nama Perguruan Tinggi',required=True),
        'lokasi_sekolah': fields.many2one('partner.employee.school.location', 'Lokasi Perguruan Tinggi',required=True),
        'tahun_pengajuan' : fields.char('Tahun Pengajuan Izin Belajar',size=4),
        'tanggal_pengajuan' : fields.date('Tanggal Pengajuan Izin Belajar',required=True),
        'start_date' : fields.date('Masa Awal Pendidikan',required=True),
        'end_date' : fields.date('Masa Akhir Pendidikan',required=True),
         'masa_perkuliahan': fields.function(_get_masa_perkuliahan, method=True,readonly=True , store=False,
                                         type="integer",string='Masa Perkuliahan'),
        #syarat
        'akreditasi'     : fields.selection([('A', 'A'), 
                                             ('B', 'B'),
                                             ('C', 'C'),
                                            ], 'Akreditasi',required=True),
        'no_sk_banpt' : fields.char('No SK Ban PT',size=100,placeholder="014/SK/BAN-PT/AK-XII/Dpl-III/I/2013",required=True),
        'tahun_sk_banpt' : fields.char('Tahun SK Ban PT',size=4,required=True,placeholder="2014"),
        'tahun_kadaluarsa_sk_banpt' : fields.char('Tahun Kadaluarsa SK Ban PT',size=4,placeholder="2014"),
        'tanggal_sk_banpt' : fields.date('Tanggal SK Ban PT',required=True),
        'tanggal_sk_jam_kuliah' : fields.date('Tanggal SK Jam Kuliah',),
        'no_sk_jam_kuliah' : fields.char('No SK Jam Perkuliahan',size=40),
        'no_sk_jam_kuliah_attach':fields.binary('Lampiran SK Jam Perkuliahan'),
        
        'no_sk_lulus' : fields.char('No SK Lulus Seleksi',size=100,placeholder="049/PPs-MM/UNWIM/2014",required=True),
        'tahun_akademik' : fields.char('Tahun Akademik',size=9,required=True,placeholder="2014/2015"),
        'tanggal_sk_lulus' : fields.date('Tanggal SK Lulus',required=True),
        'no_sk_lulus_attach':fields.binary('Lampiran SK Lulus Seleksi'),
        'notes'     : fields.text('Catatan Tambahan'),
        
        'status_masa_kerja'     : fields.boolean('Status Masa Kerja Kerja',readonly=True),
        'status_masa_kerja_desc' : fields.char('Keterangan Status Masa Kerja',size=250,readonly=True),
        'status_pangkat_minimum'     : fields.boolean('Status Pangkat Minimum',readonly=True),
        'status_pangkat_minimum_desc' : fields.char('Keterangan Pangkat Minimum',size=250,readonly=True),
        'status_hukuman_disiplin'     : fields.boolean('Status Tidak Hukuman Disiplin',readonly=True),
        'status_hukuman_disiplin_desc' : fields.char('Keterangan Hukuman Disiplin',size=250,readonly=True),
        'nilai_skp'     : fields.float('Nilai SKP Terakhir',readonly=True),
        'nilai_skp_desc' : fields.char('Keterangan Nilai SKP',size=250,readonly=True),
    }
   
    def onchange_lokasi_akreditasi(self, cr, uid, ids, lokasi,akreditasi, context=None):
        res = {'value': {'lokasi_sekolah': False,'akreditasi': False}}
        
        if lokasi and akreditasi:
            loc_pool = self.pool.get('partner.employee.school.location')
            loc_obj = loc_pool.browse(cr, uid, [lokasi])[0]
            if akreditasi > loc_obj.akreditasi :
                res = {'value': {'lokasi_sekolah': False,'akreditasi': False}}
                raise osv.except_osv(_('Maaf Tidak Bisa Mendaftar Izin Belajar'),
                                _('Lokasi Perguruan Tinggi, Tidak Memenuhi Minimum Syarat Akreditasi Program Studi Perguruan Tinggi Tersebut '))
                
        res = {'value': {'lokasi_sekolah': lokasi,'akreditasi': akreditasi}}    
        return res
    def onchange_tahun_sk_banpt(self, cr, uid, ids, tanggal_sk_banpt ,context=None):
        res = {'value': {'tanggal_sk_banpt': False,'tahun_kadaluarsa_sk_banpt': False}}
        
        if tanggal_sk_banpt:
            tahun_sk_banpt = time.strftime('%Y', time.strptime(tanggal_sk_banpt,'%Y-%m-%d'))
            res = {'value': {'tahun_sk_banpt': tahun_sk_banpt,'tahun_kadaluarsa_sk_banpt': tahun_sk_banpt}}    
        return res
    def onchange_tahun_pengajuan(self, cr, uid, ids, tanggal_pengajuan ,context=None):
        res = {'value': {'tahun_pengajuan': False,}}
        
        if tanggal_pengajuan:
            thn_pengajuan = time.strftime('%Y', time.strptime(tanggal_pengajuan,'%Y-%m-%d'))
            res = {'value': {'tahun_pengajuan': thn_pengajuan,}}  
        return res
    
            
    def action_create_izin_belajar(self, cr, uid, ids, context=None):
        """  Pengajuan Izin Belajar
        """
        
        vals = {}
        izin_belajar_pool = self.pool.get('izin.belajar')
        formasi_pool = self.pool.get('formasi.pendidikan.lanjutan')
        
        for pengajuan_obj in self.browse(cr, uid, ids, context=context) :
            vals = {}
            name='/'
            company_id = pengajuan_obj.user_id.company_id
            if pengajuan_obj and pengajuan_obj.formasi_id :
                name = pengajuan_obj.formasi_id.name
                if pengajuan_obj.user_id and pengajuan_obj.user_id.partner_id:
                    name=name+' '+pengajuan_obj.user_id.partner_id.name
            if not pengajuan_obj.status_masa_kerja :
                raise osv.except_osv(_('Maaf Tidak Bisa Mendaftar Izin Belajar'),
                                _('Persyaratan Masa Kerja Tidak Lolos, Silahkan Cek Data Kepegawaian Anda'))
            if not pengajuan_obj.status_pangkat_minimum :
                raise osv.except_osv(_('Maaf Tidak Bisa Mendaftar Izin Belajar'),
                                _('Persyaratan Pangkan Minimum Tidak Lolos, Silahkan Cek Data Riwayat Kepangkatan Kepegawaian Anda'))
            if company_id and not company_id.user_id_pl_opd :
                raise osv.except_osv(_('Maaf Konfigurasi OPD Tidak Lengkap'),
                                _('Konfigurasi Petugas Verifikasi Permohonan Untuk OPD Anda Belum Di Atur, Silahkan Hubungi Admin'))
            if company_id and not company_id.user_id_pl_bkd :
                raise osv.except_osv(_('Maaf Konfigurasi OPD Tidak Lengkat'),
                                _('Konfigurasi Petugas Verifikasi Dari BKD Untuk OPD Anda Belum Di Atur, Silahkan Hubungi Admin'))
            if pengajuan_obj.akreditasi > pengajuan_obj.lokasi_sekolah.akreditasi :
                raise osv.except_osv(_('Maaf Tidak Bisa Mendaftar Izin Belajar'),
                                _('Lokasi Perguruan Tinggi, Tidak Memenuhi Minimum Syarat Akreditasi Program Studi Perguruan Tinggi Tersebut '))
            izin_belajar_ids = izin_belajar_pool.search(cr,uid,[('formasi_id','=',pengajuan_obj.formasi_id.id),('user_id','=',pengajuan_obj.user_id.id),('active','=',True)])
            if izin_belajar_ids and len(izin_belajar_ids) >0 :
                raise osv.except_osv(_('Maaf Tidak Bisa Mendaftar Izin Belajar'),
                                _('Anda Sudah Mendaftar Untuk Formasi Ini,Pendaftaran Tidak Boleh Lebih Dari 1 Kali'))
                
            vals.update({    #Data Izin Belajar
                            'name'     : name,
                            'formasi_id'     : pengajuan_obj.formasi_id.id,
                            'nama_sekolah_id'     : pengajuan_obj.nama_sekolah_id.id,
                            'lokasi_sekolah'     : pengajuan_obj.lokasi_sekolah and pengajuan_obj.lokasi_sekolah.id,
                            'tahun_pengajuan'     : pengajuan_obj.tahun_pengajuan,
                            'tanggal_pengajuan'     : pengajuan_obj.tanggal_pengajuan,
                            'start_date'     : pengajuan_obj.start_date,
                            'end_date'     : pengajuan_obj.end_date,
                            'akreditasi'     : pengajuan_obj.akreditasi,
                            'no_sk_banpt'     : pengajuan_obj.no_sk_banpt,
                            'tahun_sk_banpt'     : pengajuan_obj.tahun_sk_banpt,
                            'tanggal_sk_banpt'     : pengajuan_obj.tanggal_sk_banpt,
                            'no_sk_jam_kuliah'     : pengajuan_obj.no_sk_jam_kuliah,
                            'no_sk_jam_kuliah_attach'     : pengajuan_obj.no_sk_jam_kuliah_attach,
                            'tahun_akademik'     : pengajuan_obj.tahun_akademik,
                            'tahun_kadaluarsa_sk_banpt'     : pengajuan_obj.tahun_kadaluarsa_sk_banpt,
                            'tanggal_sk_jam_kuliah'     : pengajuan_obj.tanggal_sk_jam_kuliah,
                            
                            'no_sk_lulus'     : pengajuan_obj.no_sk_lulus,
                            'tanggal_sk_lulus'     : pengajuan_obj.tanggal_sk_lulus,
                            'no_sk_lulus_attach'     : pengajuan_obj.no_sk_lulus_attach,
                            'notes'     : pengajuan_obj.notes,
                            #persyaratan
                            'status_masa_kerja':pengajuan_obj.status_masa_kerja,
                            'status_pangkat_minimum':pengajuan_obj.status_pangkat_minimum,
                            'status_hukuman_disiplin':pengajuan_obj.status_hukuman_disiplin,
                            'nilai_skp':pengajuan_obj.nilai_skp,
                            
                            
                            #Data Pegawai
                            
                            'user_id'     : pengajuan_obj.user_id.id,
                            'company_id'     : pengajuan_obj.user_id.company_id.id,
                            'employee_id'     : pengajuan_obj.user_id.partner_id and  pengajuan_obj.user_id.partner_id.id,
                            'department_id'     : pengajuan_obj.user_id.partner_id and pengajuan_obj.user_id.partner_id.department_id and pengajuan_obj.user_id.partner_id.department_id.id,
                            'golongan_id'     : pengajuan_obj.user_id.partner_id and pengajuan_obj.user_id.partner_id.golongan_id and pengajuan_obj.user_id.partner_id.golongan_id.id,
                            'job_id'     : pengajuan_obj.user_id.partner_id and pengajuan_obj.user_id.partner_id.job_id and pengajuan_obj.user_id.partner_id.job_id.id,
                            'user_id_opd':company_id.user_id_pl_opd.id,
                            'user_id_bkd':company_id.user_id_pl_bkd.id,
                            #Default
                            'state':'verifikasi_opd',
                            'active':True,
                                         })
            izin_belajar_pool.create(cr, uid,  vals, context)
            
            #pengurangan jumlah kuota
            
            #sisa_kuota =    pengajuan_obj.formasi_id.jumlah_kuota - 1
            #formasi_pool.write(cr,1,[pengajuan_obj.formasi_id.id], {'jumlah_kuota':sisa_kuota}, context=context) 
        return True;
        
    
pengajuan_izin_belajar()
