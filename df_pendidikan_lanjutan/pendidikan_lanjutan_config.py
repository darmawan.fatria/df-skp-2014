from openerp.osv import fields, osv
from datetime import datetime,timedelta
import time
from mx import DateTime
import openerp.addons.decimal_precision as dp

# ====================== Popup Class Object ================================= #

class pendidikan_lanjutan_config(osv.osv):
    _name = 'pendidikan.lanjutan.config'
    _description = 'Persyaratan Pelayanan Pendidikan Lanjutan'
    _columns = {
        'name' : fields.char('Nama Persyaratan',size=100,required=True),
        'code' : fields.char('Kode Persyaratan',size=16,required=True),
        'description' : fields.text('Deskripsi',),
        'int_value' : fields.integer('Nilai Integer'),
        'type'     : fields.selection([('ib', 'Izin Belajar'), ('tb', 'Tugas Belajar'),
                                       ], 'Jenis Pelayanan Pendidikan'
                                      ,required=True),
        'active' : fields.boolean('Aktif'),
       
    }
   
    
pendidikan_lanjutan_config()


class partner_employee_study_degree(osv.osv):
    _inherit= "partner.employee.study.degree"
    
    _columns = {
    'pangkat_minimum': fields.many2one('hr.employee.golongan', 'Pangkat Minimum',required=True,), 
    'masa_kerja_pangkat' : fields.integer('Masa Kerja Pangkat (Tahun)',help="Pangkat Minimum Selama Berapa Tahun?"),
    'izin_belajar_desc' : fields.text('Deskpripsi Persyaratan Izin Belajar',),
    'employee_id': fields.many2one('res.partner', 'Penanggung Jawab Izin Belajar',required=True,), 
               
    }
   
partner_employee_study_degree()

class partner_employee_school_location(osv.osv):
    _name = 'partner.employee.school.location'
    _description = 'Lokasi Perguruan Tinggi'
    _columns = {
        'name' : fields.char('Lokasi',size=80,required=True),
        'akreditasi'     : fields.selection([('A', 'A'), 
                                             ('B', 'B'),
                                             ('C', 'C'),
                                            ], 'Minimum Akrediatasi',required=True),
    }
   
    
partner_employee_school_location()
