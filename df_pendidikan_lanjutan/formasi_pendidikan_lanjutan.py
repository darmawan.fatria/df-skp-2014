# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from datetime import date,datetime,timedelta
import time
#from mx import DateTime
from openerp.tools.translate import _

# ====================== res partner ================================= #

class formasi_pendidikan_lanjutan(osv.osv):
    _name = 'formasi.pendidikan.lanjutan'
    _description='Formasi Pendidikan Lanjutan untuk Izin Belajar Dan Tugas Belajar'
    
    
    def onchange_formasi_name(self, cr, uid, ids, level_pendidikan_id,program_studi_id,nama_sekolah_id,tahun_pembuatan, context=None):
        
        res = {'value': {'name': '-',
            }}
        studi_pool=self.pool.get('hr.employee.study')
        sekolah_pool=self.pool.get('hr.employee.school')
        jenjang_pool=self.pool.get('partner.employee.study.degree')
        
        tahun_formasi=nama_sekolah=nama_program_studi=nama_jenjang=''
        if program_studi_id:
            studi_obj = studi_pool.browse(cr, uid, [program_studi_id,])[0]
            nama_program_studi = studi_obj and studi_obj.name
        if level_pendidikan_id:
            jenjang_obj = jenjang_pool.browse(cr, uid, [level_pendidikan_id,])[0]
            nama_jenjang = jenjang_obj and jenjang_obj.name
        if nama_sekolah_id:
            sekolah_obj = sekolah_pool.browse(cr, uid, [nama_sekolah_id,])[0]
            nama_sekolah = sekolah_obj and sekolah_obj.name
        if tahun_pembuatan:
            tahun_formasi=tahun_pembuatan
        if nama_sekolah_id: 
            res['value']['name'] = nama_jenjang+' '+nama_program_studi+' '+nama_sekolah+' '+tahun_formasi
        else :
            res['value']['name'] = nama_jenjang+' '+nama_program_studi+' '+tahun_formasi
        return res
    def _get_sisa_kuota(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        sisa_kuota=0
        for o in self.browse(cr,uid,ids,context=None) :
            sisa_kuota = o.jumlah_kuota
            if o.pendaftar_ids :
                for p in o.pendaftar_ids:
                    if p.state in ('verifikasi_opd','verifikasi_bkd','confirm') :
                        sisa_kuota=sisa_kuota-1
            res[o.id]=sisa_kuota
        return res
    _columns = {
        'name': fields.char('Nama Formasi',size=456),
        'company_id': fields.many2one('res.company', 'OPD'),
        'level_pendidikan_id': fields.many2one('partner.employee.study.degree', 'Jenjang Pendidikan',required=True),
        'program_studi_id': fields.many2one('hr.employee.study', 'Program Studi',required=True),
        'nama_sekolah_id': fields.many2one('hr.employee.school', 'Nama Sekolah'),
        'tahun_pembuatan' : fields.char('Tahun Pembuatan Formasi',size=4),
        'start_date' : fields.date('Tanggal Berlaku Awal'),
        'end_date' : fields.date('Tanggal Berlaku Akhir'),
        'active' : fields.boolean('Aktif'),
        'jumlah_kuota' : fields.integer('Jumlah Kuota'),
        'type'     : fields.selection([('ib', 'Izin Belajar'), ('tb', 'Tugas Belajar'),
                                       ], 'Jenis Formasi'
                                      ,required=True
                                                     ),
        'pendaftar_ids': fields.one2many('izin.belajar','formasi_id', 'Pendaftar',readonly=True ), 
        'sisa_kuota': fields.function(_get_sisa_kuota, method=True,readonly=True , store=False,
                                         type="integer",string='Sisa Kuota'),
    }
     
    def action_pengajuan_izin_belajar_popup(self, cr, uid, ids, context=None):
        """ Buat Pengajuan Izin Belajar Oleh Pegawai
        """
        
        if not ids: return []
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_pendidikan_lanjutan', 'action_create_izin_belajar_form_view')
        formasi_obj = self.browse(cr, uid, ids[0], context=context)
        user_obj = self.pool.get('res.users').browse(cr,uid,uid,context=context)
        today = date.today()
        
        if not user_obj.partner_id :
             raise osv.except_osv(_('Proses Pengajuan Tidak Bisa Dilanjutkan'),
                                _('Silahkan Lengkapi Data Kepegawaian Anda, Atau Hubungi Admin'))
        #cek presyaratan
        status_masa_kerja_desc,status_masa_kerja = self.validate_masa_kerja(cr,uid,user_obj.partner_id,context=None)
        status_pangkat_minimum_desc,status_pangkat_minimum = self.validate_pangkat_minimum(cr, uid, user_obj.partner_id, formasi_obj, context)
        status_hukuman_disiplin =  user_obj.partner_id.status_hukuman_disiplin
        status_hukuman_disiplin_desc = 'Lolos'
        if not status_hukuman_disiplin:
            status_hukuman_disiplin_desc = 'Anda Masih Dalam Status Terkena Hukuman Disiplin'
        nilai_skp =user_obj.partner_id.last_year_nilai_skp
        return {
                'name':_("Pengajuan Izin Belajar"),
                'view_mode': 'form',
                'view_id': view_id,
                'view_type': 'form',
                'res_model': 'pengajuan.izin.belajar',
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'new',
                'domain': '[]',
                'context': {
                    'default_formasi_id':formasi_obj.id,
                    'default_user_id':uid,
                    'default_tanggal_pengajuan':today.strftime('%Y-%m-%d'),
                    'default_tahun_pengajuan':today.year,
                    'default_tahun_akademik':str(today.year)+'/'+str(today.year+1),
                    'default_status_masa_kerja':status_masa_kerja,
                    'default_status_pangkat_minimum':status_pangkat_minimum,
                    'default_status_hukuman_disiplin':status_hukuman_disiplin,
                    'default_nilai_skp' :nilai_skp,
                    'default_status_masa_kerja_desc':status_masa_kerja_desc,
                    'default_status_pangkat_minimum_desc':status_pangkat_minimum_desc,
                    'default_status_hukuman_disiplin_desc':status_hukuman_disiplin_desc,
                    'default_nilai_skp_desc':'LALLALALALALLALA',
                }
            }
        return False
    def validate_masa_kerja(self,cr,uid,employee,context=None):
        persyaratan_pool = self.pool.get('pendidikan.lanjutan.config')
        masa_kerja=False
        masa_kerja_desc='Masa Kerja Anda Tidak Mencukupi Persyaratan Untuk Formasi ini'
        if employee and employee.masa_kerja:
            masa_kerja_ids=persyaratan_pool.search(cr,uid,[('code','=','masa.kerja'),('type','=','ib'),('active','=',True)])
            if masa_kerja_ids:
                persyaratan_obj=persyaratan_pool.browse(cr,uid,masa_kerja_ids[0])
                if persyaratan_obj and persyaratan_obj.int_value:
                    if employee.masa_kerja > persyaratan_obj.int_value:
                        masa_kerja=True
                        masa_kerja_desc='Lolos'
                    else :
                        masa_kerja_desc='Masa Kerja Anda Baru Mencapai '+str(employee.masa_kerja)+' Tahun, Kurang Dari '+str(persyaratan_obj.int_value)+' Tahun. Yang Menjadi Persyaratan Formasi Ini.'
        return masa_kerja_desc,masa_kerja
    def validate_pangkat_minimum(self,cr,uid,employee,formasi,context=None):
        
        status_pangkat_minimum=False
        status_pangkat_minimum_desc='Tidak LolosX'
        if formasi and formasi.level_pendidikan_id:
            if employee and employee.golongan_id:
                golongan_id_minimum = formasi.level_pendidikan_id.pangkat_minimum
                if not  golongan_id_minimum:
                    raise osv.except_osv(_('Kelengkapan Data Formasi Tidak Lengkap'),
                                _('Proses Tidak Dapat Dilanjutkan Karena Persyaratan Pangkat Minimum Pada Jenjang Pendidikan Belum Diatur Oleh Admin'))
                if golongan_id_minimum:
                    if golongan_id_minimum.id == employee.golongan_id.id:
                        if employee.masa_kerja > formasi.level_pendidikan_id.masa_kerja_pangkat :
                            status_pangkat_minimum =True;
                            status_pangkat_minimum_desc='Lolos'
                        else :
                            status_pangkat_minimum_desc =formasi.level_pendidikan_id.izin_belajar_desc
                    if employee.golongan_id.level > golongan_id_minimum.level:
                        if employee.masa_kerja > formasi.level_pendidikan_id.masa_kerja_pangkat :
                            status_pangkat_minimum =True;
                            status_pangkat_minimum_desc='Lolos'
                        else :
                            status_pangkat_minimum_desc =formasi.level_pendidikan_id.izin_belajar_desc
                    else:
                        golongan_history_pool = self.pool.get('partner.employee.golongan.history')
                        golongan_emp_ids=golongan_history_pool.search(cr,uid,[('golongan_id_history','=',golongan_id_minimum.id),('partner_id','=',employee.id)])
                        status_pangkat_minimum_desc = formasi.level_pendidikan_id.izin_belajar_desc
                        if golongan_emp_ids:
                            eq_golongan_minimum_emp=golongan_history_pool.browse(cr,uid,golongan_emp_ids[0])
                            if golongan_id_minimum.id == eq_golongan_minimum_emp.golongan_id_history.id:
                                if eq_golongan_minimum_emp.date :
                                    today = date.today()
                                    awal_masa_kerja = eq_golongan_minimum_emp.date and time.strftime('%Y', time.strptime(eq_golongan_minimum_emp.date,'%Y-%m-%d'))
                                    minimum_pangkat_masa_kerja = today.year - int(awal_masa_kerja)
                                    if minimum_pangkat_masa_kerja > formasi.level_pendidikan_id.masa_kerja_pangkat :
                                        status_pangkat_minimum=True
                                        status_pangkat_minimum_desc='Lolos'
                                    else :
                                        status_pangkat_minimum_desc = formasi.level_pendidikan_id.izin_belajar_desc
                        
                           
                        
                            
        return status_pangkat_minimum_desc,status_pangkat_minimum
formasi_pendidikan_lanjutan()
