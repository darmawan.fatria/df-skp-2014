<!DOCTYPE html>
<html>
<style type="text/css">
body{
    font-family: Tahoma;
	font-size:14px;
	background-color:white;
}
.kop_tanggal{
    padding: 0 0 0 42.5em;
}

table.header_table {
	border-width: 0px;
	border-collapse: collapse;
}
table.header_table td {
	border-top:0px;
	border-bottom:0px;
	border-right:0px;
	border-left:0px;
    border-style: solid;
	padding: 5px;
}
.kanans {
	padding-left: 5em;
}
.kiri {
	margin-right: 15em;
    padding-right: 18em;
}
table.content_table {
	border-width: 0px;
	border-collapse: collapse;
}
table.content_table td {
	border-width: 12px;
    border-top:0px;
	border-bottom:0px;
	border-right:0px;
	border-left:0px;
    border-style: solid;
    padding: 5px;
    vertical-align: top
}
table.sub_content_table {
	border-width: 0px;
	border-collapse: collapse;
}
table.sub_content_table td {
	border-width: 12px;
    border-top:0px;
	border-bottom:0px;
	border-right:0px;
	border-left:0px;
    border-style: solid;
    padding: 5px;
}
.content_table_padding{
    padding-left:4em
}
.to_ttd{
    padding-top: 100px;
}
.ttd{
    padding-left: 500px;
    text-align: center;
    padding-bottom: 10px;
}    
.underline{
    padding-left: 500px;
    text-align: center;
    padding-bottom: 5px;
}  
</style>
<head></head>
    <body>
    <% 
     pergub = get_config_label_pergub()
    %>
    %for o in get_data_izin_belajar(data) :
        <div class="kop_tanggal">
                        Bandung, &nbsp;${get_format_date(o.tanggal_surat_permohonan)} 
                        <br/>
        </div>
        <table class="header_table">
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td >
                    <div class="kanan"> Kepada </div>
                </td>
            </tr>
            <tr>
                <td>Nomor</td>
                <td>:</td>
                <td> ${o.no_sp_izin_belajar}  </td>
                <td>
                    <div class="kanan"> Yth. Bapak Gubernur </div>
                </td>
            </tr>
            <tr>
                <td>Sifat</td>
                <td>:</td>
                <td>Biasa</td>
                <td>
                    <div class="kanan"> melalui </div>
                </td>
            </tr>
            <tr>
                <td>Lampiran</td>
                <td>:</td>
                <td>1 (satu) berkas</td>
                <td>
                    <div class="kanan">Kepala Badan Kepegawaian Daerah</div>
                </td>
            </tr>
            <tr>
                <td>Hal</td>
                <td>:</td>
                <td>Permohonan Izin Belajar an ${o.employee_id.name}  <div class="kiri"></div></td>
                <td>
                    <div class="kanan">Provinsi Jawa Barat</div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <div class="kanan">Di</div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <div class="kanan">Bandung</div>
                </td>
            </tr>
        </table>
        <br/>
        <div class="content_table_padding">
        <table class="content_table">
            <tr>
                <td colspan="2">
                    <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        Dipermaklumkan dengan hormat, berdasarkan Peraturan Gubernur Jawa Barat Nomor 62 Tahun 2012 tentang Pendidikan Lanjutan dan Riset 
                        Pegawai Negeri Sipil di Lingkungan Pemerintah Provinsi Jawa Barat                       
                        dan ${pergub} tentang Formasi Pendidikan Lanjutan Tahun ${o.tahun_pengajuan or '-'}, 
                        Kami sampaikan   permohonan izin belajar:
                    </p>
                        <div class="content_table_padding">
                            <table class="sub_content_table">
                                <tr>
                                    <td>Nama</td>
                                    <td>:</td>
                                    <td>  ${o.employee_id.name}  </td>
                                </tr>
                                 <tr>
                                    <td>NIP</td>
                                    <td>:</td>
                                    <td>  ${o.nip}  </td>
                                </tr>
                                <tr>
                                    <td>Jabatan</td>
                                    <td>:</td>
                                    <td> ${o.job_id.name} </td>
                                </tr>
                            </table>
                        </div>
                    <p>
                        Untuk mengikuti pendidikan lanjutan pada:
                    </p>
                    <p>
                        <div class="content_table_padding">
                            <table class="sub_content_table">
                                <tr>
                                    <td>Program Studi</td>
                                    <td>:</td>
                                    <td>  ${o.program_studi_id.name} </td>
                                </tr>
                                 <tr>
                                    <td>Jenjang Pendidikan</td>
                                    <td>:</td>
                                    <td> ${o.level_pendidikan_id.name}  </td>
                                </tr>
                                <tr>
                                    <td>Perguruan Tinggi</td>
                                    <td>:</td>
                                    <td> ${o.nama_sekolah_id.name}  </td>
                                </tr>
                                <tr>
                                    <td>Masa Pendidikan</td>
                                    <td>:</td>
                                    <td> ${get_format_date(o.start_date)}  s/d ${get_format_date(o.end_date)}  </td>
                                </tr>
                            </table>
                        </div>
                    </p>
                    <p>Permohonan izin belajar yang bersangkutan telah diperiksa dan validasi sesuai ketentuan pendidikan lanjutan, kami nyatakan bahwa:
                    </p>
                </td>
            </tr>
            <tr>
                <td>a.</td>
                <td>Program studi yang dimohon terdapat dalam formasi pendidikan lanjutan yaitu program studi ${o.program_studi_id.name}; </td>
            </tr>
            <tr>
                <td>b.</td>
                <td>Program studi yang dimohon terakreditasi ${o.akreditasi} berlaku sampai dengan Tahun ${o.tahun_kadaluarsa_sk_banpt or '-'} sesuai dengan SK Ban PT Nomor ${o.no_sk_banpt} Tahun ${o.tahun_sk_banpt} ;
                </td>
            </tr>
            <tr>
                <td>c.</td>
                <td>Pangkat yang bersangkutan memenuhi ketentuan pangkat minimal untuk program (paket B,C,S1,S2 dan S3) yaitu Pangkat/Golongan Ruang  ${o.golongan_id.name}  sesuai dengan nomor SK Kenaikan Pangkat PNS Nomor ${o.employee_id.current_sk_golongan_id} tahun ${o.employee_id.current_tahun_golongan_id};</td>
            </tr>
            <tr>
                <td>d.</td>
                <td>Status PNS yang bersangkutan memenuhi ketentuan minimal 2  tahun sebagai PNS sesuai dengan SK Pengangkatan PNS Nomor ${o.employee_id.sk_pengangkatan_pns}  Tahun ${o.employee_id.tahun_pengangkatan_pns} ;</td>
            </tr>
            <tr>
                <td>e.</td>
                <td>Perilaku PNS yang bersangkutan sangat baik/baik sesuai dengan nilai SKP Tahun ${o.employee_id.last_year_tahun_skp or '-'}  dan tidak sedang menjalani hukuman disiplin ringan/sedang/berat serta tidak sedang menjalani pemberhentian sementara sebagai PNS;</td>
            </tr>
            <tr>
                <td>f.</td>
                <td>Perkuliahan PNS yang bersangkutan diluar jam/hari kerja sesuai dengan surat keterangan dari Perguruan Tinggi Nomor  ${o.no_sk_jam_kuliah or '-'}  tanggal ${get_format_date(o.tanggal_sk_jam_kuliah)};</td>
            </tr>
            <tr>
                <td>g.</td>
                <td>PNS dinyatakan lulus seleksi masuk pendidikan perguruan tinggi sesuai dengan keterangan  Nomor ${o.no_sk_lulus} Tanggal ${get_format_date(o.tanggal_sk_lulus)}</td>
            </tr>
            <tr>
                <td></td>
                <td >
                    Demikian permohonan ini kami sampaikan, sebagai bahan pertimbangan, atas perhatian kami ucapkan terima kasih.
                    
                </td>
            </tr>
        </table>
    </div>
    <div class="to_ttd"> 
<div class="ttd"> KEPALA ${o.company_id.name} </div>    
<br /><br /><br /><br /><br />
<div class="underline"> ${o.company_id.head_of_comp_employee_id.name} </div>  
<div class="underline">     ____________________________________ </div>  
<div class="underline"> NIP. ${o.company_id.head_of_comp_employee_id.nip}  </div>    

    
</div>    
    %endfor
    </body>
</html>