<!DOCTYPE html SYSTEM "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<style>
body{
    font-family: Tahoma;
	font-size:14px;
	background-color:white;
}

table.header_table {
	border-width: 0px;
	border-collapse: collapse;
    font-size:16px;
    width: 100%;
    
}
table.header_table td {
	border-top:0px;
	border-bottom:0px;
	border-right:0px;
	border-left:0px;
    border-style: solid;
	padding: 5px;
    text-align: center;
}
table.content_table {
	border-width: 0px;
	border-collapse: collapse;
}
table.content_table td {
	border-width: 12px;
    border-top:0px;
	border-bottom:0px;
	border-right:0px;
	border-left:0px;
    border-style: solid;
    padding: 5px;
    vertical-align: top;
    text-align: left;
}
.text_left{
	text-align: left;
}
.text_center{
	text-align: center;
}
.to_ttd{
    padding-top: 100px;
}
.ttd{
    padding-left: 500px;
    text-align: center;
    padding-bottom: 10px;
}
</style>
</head>
<body>
%for o in get_data_izin_belajar(data) :
    <table class="header_table">
            <tr>
                <td colspan="3" >S U R A T   I Z I N</td>
            </tr>
            <tr>
                <td colspan="3" > Nomor : ${o.no_sk_izin_belajar}</td>
            </tr>
            <tr>
                <td colspan="3">TENTANG </td>
                
            </tr>
            <tr >
                <td colspan="3">IZIN BELAJAR PEGAWAI NEGERI SIPIL </td>
                
            </tr>
            <tr>
                <td colspan="3">DI LINGKUNGAN PEMERINTAH PROVINSI JAWA BARAT</td>
            </tr>
            <tr>
                <td colspan="3">a.n. ${o.employee_id.name}</td>
            </tr>

    </table>
    <br /><br />
    <table class="content_table">
        <tr>
            <td  >Dasar </td>
            <td> : </td>
            <td> a. </td>
            <td> Peraturan Gubernur Jawa Barat Nomor 62 Tahun 2012 tentang Pendidikan Lanjutan dan Riset Pegawai Negeri Sipil di Lingkungan Pemerintah Provinsi Jawa Barat. </td>
        </tr>
        <tr>
            <td  > </td>
            <td>  </td>
            <td> b. </td>
            <td> Keputusan Gubernur Jawa Barat Nomor 890/ Kep-1808-BKD/2013 tentang Formasi Pendidikan Lanjutan dan Riset Pegawai Negeri Sipil di Lingkungan Pemerintah Provinsi Jawa Barat Tahun ${o.tahun_pengajuan or '-'}. </td>
        </tr>
        <tr>
            <td>  </td>
            <td>  </td>
            <td> c. </td>
            <td> Surat Kepala ${o.company_id.name} Provinsi Jawa Barat Nomor: ${o.no_sp_izin_belajar or '-'} tanggal ${get_format_date(o.tanggal_surat_permohonan)} Perihal Permohonan Izin Belajar a.n. ${o.employee_id.name}. </td>
        </tr>
        <tr>
            <td>  </td>
            <td>  </td>
            <td> d. </td>
            <td> Surat Keputusan Badan Akreditasi Nasional Perguruan Tinggi Nomor ${o.no_sk_banpt} tanggal ${get_format_date(o.tanggal_sk_banpt)} tentang Status, Peringkat, dan Hasil Akreditasi Program  ${o.level_pendidikan_id.name}   di Perguruan Tinggi, dengan Terakreditasi ${o.akreditasi}. </td>
        </tr>
        <tr>
            <td>  </td>
            <td>  </td>
            <td> e. </td>
            <td> Surat keterangan sebagai mahasiswa dari ${o.nama_sekolah_id.name}  Nomor:${o.no_sk_lulus or '-'} tanggal ${get_format_date(o.tanggal_sk_lulus)}. </td>
        </tr>
        
        <tr>
            <td>  </td>
            <td>  </td>
            <td>  </td>
            <td > <div class="text_center"> MEMBERIKAN IZN </div></td>
        </tr>
        <tr>
            <td> Kepada </td>
            <td>  </td>
            <td>  </td>
            <td > </td>
        </tr>
        
        <tr>
            <td> Nama </td>
            <td> : </td>
            <td>  </td>
            <td > ${o.employee_id.name} </td>
        </tr>
        
        <tr>
            <td> N I P </td>
            <td> : </td>
            <td>  </td>
            <td > ${o.nip}  </td>
        </tr>
        
        <tr>
            <td> Pangkat/Golongan </td>
            <td> : </td>
            <td>  </td>
            <td >  ${o.golongan_id.name}  </td>
        </tr>
        
        <tr>
            <td> Jabatan </td>
            <td> : </td>
            <td>  </td>
            <td >  ${o.job_id.name} </td>
        </tr>
        
        <tr>
            <td> Instansi </td>
            <td> : </td>
            <td>  </td>
            <td > ${o.company_id.name} </td>
        </tr>
        <tr>
            <td> Untuk </td>
            <td> : </td>
            <td>  </td>
            <td > Mengikuti Pendidikan Program ${o.level_pendidikan_id.name}  Program Studi ${o.program_studi_id.name} pada ${o.nama_sekolah_id.name} ${o.lokasi_sekolah.name} Tahun ${o.tahun_akademik} selama ${o.masa_perkuliahan}  Tahun terhitung mulai tanggal ditetapkan, dengan ketentuan : <br/>
1. Izin Belajar diberikan diluar jam kerja; <br/>
2. Tidak mengganggu tugas-tugas kedinasan; <br/>
3. Biaya ditanggung oleh yang bersangkutan; <br/>
4. Melaporkan hasil kelulusan setelah menyelesaikan Pendidikan; <br/>
5. Dapat mengajukan bantuan biaya riset edukasi.  <br/>
            </td>
        </tr>
    </table>
    
<div class="to_ttd"> 
<div class="ttd"> Ditetapkan di  B a n d u n g </div>    
<div class="ttd"> Pada tanggal ${get_format_date(o.tanggal_surat_keputusan)}</div>    
<div class="ttd"> ${o.level_pendidikan_id and o.level_pendidikan_id.employee_id and o.level_pendidikan_id.employee_id.job_id and o.level_pendidikan_id.employee_id.job_id.name or '-'  }</div>    
<br /><br /><br /><br /><br />
<div class="ttd"> ${o.level_pendidikan_id and o.level_pendidikan_id.employee_id and o.level_pendidikan_id.employee_id.name or '-'  }</div>  
<div class="ttd">      ${o.level_pendidikan_id and o.level_pendidikan_id.employee_id and o.level_pendidikan_id.employee_id.golongan_id and o.level_pendidikan_id.employee_id.golongan_id.description  or '-'  } </div>  
<div class="ttd"> NIP. ${o.level_pendidikan_id and o.level_pendidikan_id.employee_id and o.level_pendidikan_id.employee_id.nip or '-'  } </div>    
<div class="ttd"> </div>    
    
</div>    

  








%endfor
</body>
</html>
