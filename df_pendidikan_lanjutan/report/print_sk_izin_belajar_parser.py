from report import report_sxw
from report_webkit import webkit_report
import time
from osv import fields, osv
from tools.translate import _
import locale


class print_sk_izin_belajar_parser(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context=None):
        locale.setlocale(locale.LC_ALL, 'id_ID.utf8')
        super(print_sk_izin_belajar_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'get_data_izin_belajar' : self._get_data_izin_belajar,
            'get_format_date' : self._get_format_date,
           
        })
        
    def _get_data_izin_belajar(self,data):
        
        obj_data=self.pool.get(data['src_model']).browse(self.cr,self.uid,data['izin_belajar_ids'])
        return obj_data
    def _get_format_date(self,a_date):
        formatted_print_date=''
        try :
            formatted_print_date = time.strftime('%d %B %Y', time.strptime(a_date,'%Y-%m-%d'))
        except :
            formatted_print_date='-'
            
        return formatted_print_date
    
report_sxw.report_sxw('report.sk.izin.belajar.report.form' ,
                        'sk.izin.belajar.report',
                        'addons/df_pendidikan_lanjutan/report/sk_izin_belajar.mako', parser=print_sk_izin_belajar_parser)
