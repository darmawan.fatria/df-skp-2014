from report import report_sxw
from report_webkit import webkit_report
import time
from osv import fields, osv
from tools.translate import _
import locale


class print_sp_izin_belajar_parser(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context=None):
        locale.setlocale(locale.LC_ALL, 'id_ID.utf8')
        super(print_sp_izin_belajar_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'get_data_izin_belajar' : self._get_data_izin_belajar,
            'get_config_label_pergub' : self._get_config_label_pergub,
            'get_format_date' : self._get_format_date,
            
           
        })
        
    def _get_data_izin_belajar(self,data):
        
        obj_data=self.pool.get(data['src_model']).browse(self.cr,self.uid,data['izin_belajar_ids'])
        return obj_data
    def _get_config_label_pergub(self):
        config_pool = self.pool.get('pendidikan.lanjutan.config')
        config_ids = config_pool.search(self.cr,self.uid,[('code','=','rep.pergub.ib'),('active','=','True')],context=None)
        o = config_pool.browse(self.cr,self.uid,config_ids,context=None)[0]
        
        return o and o.description or '-'
    def _get_format_date(self,a_date):
        formatted_print_date=''
        try :
            formatted_print_date = time.strftime('%d %B %Y', time.strptime(a_date,'%Y-%m-%d'))
        except :
            formatted_print_date='-'
            
        return formatted_print_date
    
report_sxw.report_sxw('report.sp.izin.belajar.report.form' ,
                        'sp.izin.belajar.report',
                        'addons/df_pendidikan_lanjutan/report/surat_permohonan_izin_belajar.mako', parser=print_sp_izin_belajar_parser)
