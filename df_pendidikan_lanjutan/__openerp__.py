##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

{
    "name": "Formasi Pendidikan Lanjutan",
    "version": "1.0",
    "author": "darmawan fatriananda",
    "category": "Pelayanan BKD/Pendidikan Lanjutan",
    "description": """
    Pelayanan Kepegawaian untuk program pendidikan lanjutan
    """,
    "website" : "",
    "license" : "GPL-3",
    "depends": [
                "df_lp_partner_employee",
                "df_skp_employee"
                ],
    "init_xml": [],
    'update_xml': ["security/pendidikan_lanjutan_security.xml",
                   "formasi_pendidikan_lanjutan_view.xml",
                   "popup_view/pendidikan_lanjutan_popup_view.xml",
                   "izin_belajar_view.xml",
                   "pendidikan_lanjutan_config_view.xml",
                   "company_view.xml",
                   "izin_belajar_report_view.xml"
                   ],
    'demo_xml': [],
    'installable': True,
    'active': False,
}
