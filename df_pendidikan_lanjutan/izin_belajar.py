# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from datetime import datetime,timedelta
import time
#from mx import DateTime
from openerp.tools.translate import _

# ====================== res partner ================================= #

class izin_belajar(osv.osv):
    _name = 'izin.belajar'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description='Izin Belajar'
    
    def _get_masa_perkuliahan(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        masa_perkuliahan=0
        for o in self.browse(cr, uid, ids, context=context):
            
            if o.start_date and o.end_date:
                awal_masa_kuliah = time.strftime('%Y', time.strptime(o.start_date,'%Y-%m-%d'))
                akhir_masa_kuliah = time.strftime('%Y', time.strptime(o.end_date,'%Y-%m-%d'))
                masa_perkuliahan = int(akhir_masa_kuliah) - int(awal_masa_kuliah)
            res[o.id]=masa_perkuliahan
        return res
    _columns = {
        'name': fields.char('Nama Pengajuan',size=456),
        'formasi_id': fields.many2one('formasi.pendidikan.lanjutan', 'Formasi Izin Belajar',required=True,readonly="True"),
        'level_pendidikan_id': fields.related('formasi_id', 'level_pendidikan_id',   type="many2one", relation='partner.employee.study.degree', string='Jenjang Pendidikan', store=True),
        'program_studi_id': fields.related('formasi_id', 'program_studi_id',   type="many2one", relation='hr.employee.study', string='Program Studi', store=True),
        
        'user_id': fields.many2one('res.users', 'User',required=True, readonly=True,),
        'employee_id': fields.many2one('res.partner', 'Pegawai',required=True, readonly=True,),
        'company_id': fields.many2one('res.company', 'OPD',required=True, readonly=True,),
        'department_id': fields.many2one('hr.department', 'Unit Kerja', readonly=True,),
        'golongan_id': fields.many2one('hr.employee.golongan', 'Pangkat/Golongan', readonly=True,), 
        'job_id': fields.many2one('hr.job', 'Jabatan', readonly=True,),
        'image': fields.related('employee_id', 'image',   type="binary",  string='Image', readonly=True,store=True),
        'nip': fields.related('employee_id', 'nip',   type="char",  string='NIP',  readonly=True,store=True),
        
        'nama_sekolah_id': fields.many2one('hr.employee.school', 'Nama Perguruan Tinggi',required=True),
        'lokasi_sekolah': fields.many2one('partner.employee.school.location', 'Lokasi Perguruan Tinggi',required=True),
        'tahun_pengajuan' : fields.char('Tahun Pembuatan Izin Belajar',size=4,required=True),
        'tanggal_pengajuan' : fields.date('Tanggal Pengajuan Izin Belajar',required=True),
        'tanggal_surat_permohonan' : fields.date('Tanggal Penetapan Surat Permohonan Izin Belajar'),
        'tanggal_surat_keputusan' : fields.date('Tanggal Penetapan Surat Keputusan Izin Belajar'),
        'tahun_akademik' : fields.char('Tahun Akademik',size=9,required=True,placeholder="2014/2015"),
        'start_date' : fields.date('Masa Pendidikan Awal'),
        'end_date' : fields.date('Masa Pendidikan Akhir'),
        'masa_perkuliahan': fields.function(_get_masa_perkuliahan, method=True,readonly=True , store=False,
                                         type="integer",string='Masa Perkuliahan (Tahun)'),
        
        'state'     : fields.selection([('draft', 'Draft'), 
                                             ('verifikasi_opd', 'Verifikasi Kepala OPD'),
                                             ('verifikasi_bkd', 'Verifikasi BKD'),
                                             ('confirm', 'Pengajuan Izin Belajar Diterima'),
                                             ('done', 'Izin Belajar Selesai'),
                                            ], 'Status',required=True),
        #syarat
        'akreditasi'     : fields.selection([('A', 'A'), 
                                             ('B', 'B'),
                                             ('C', 'C'),
                                            ], 'Akreditasi',required=True),
        'no_sk_banpt' : fields.char('No SK Ban PT',size=100,placeholder="014/SK/BAN-PT/AK-XII/Dpl-III/I/2013",required=True),
        'tahun_sk_banpt' : fields.char('Tahun SK Ban PT',size=4,required=True,placeholder="2014"),
        'tahun_kadaluarsa_sk_banpt' : fields.char('Tahun Kadaluarsa SK Ban PT',size=4,placeholder="2014"),
        'tanggal_sk_banpt' : fields.date('Tanggal SK Ban PT',required=True),
        'no_sk_jam_kuliah' : fields.char('No SK Jam Perkuliahan',size=100),
        'tanggal_sk_jam_kuliah' : fields.date('Tanggal SK Jam Kuliah',),
        'no_sk_jam_kuliah_attach':fields.binary('Lampiran SK Jam Perkuliahan'),
        'no_sk_lulus' : fields.char('No SK Lulus Seleksi',size=100,placeholder="049/PPs-MM/UNWIM/2014",required=True),
        'tanggal_sk_lulus' : fields.date('Tanggal SK Lulus',required=True),
        'no_sk_lulus_attach':fields.binary('Lampiran SK Lulus Seleksi'),
        'active'     : fields.boolean('Aktif'),
        'notes'     : fields.text('Catatan'),
        'status_masa_kerja'     : fields.boolean('Status Masa Kerja Kerja',readonly=True),
        'status_pangkat_minimum'     : fields.boolean('Status Pangkat Minimum',readonly=True),
        'status_hukuman_disiplin'     : fields.boolean('Status Tidak Hukuman Disiplin',readonly=True),
        'nilai_skp'     : fields.float('Nilai SKP Terakhir',readonly=True),
        
        'user_id_opd': fields.many2one('res.users', 'Verifikatur OPD',required=True, readonly=True,),
        'user_id_bkd': fields.many2one('res.users', 'Verifikatur BKD',required=True, readonly=True,),
        
        'no_sp_izin_belajar': fields.char('No Surat Permohonan',size=35,placeholder="800/1033/Kepeg Dispenda"),
        'no_sk_izin_belajar': fields.char('No Surat Keputusan',size=35,placeholder="826.5/0120/110/Bangrir"),
        'attach_sp_izin_belajar':fields.binary('Lampiran Surat Permohonan'),
        'attach_sk_izin_belajar':fields.binary('Lampiran Surat Keputusan'),
    }
   
    def validate_data_kepegawaian(self,cr,uid,ids,context=None):
        
        for pengajuan_obj in self.browse(cr, uid, ids, context=context) :
            company_id = pengajuan_obj.user_id.company_id
            if pengajuan_obj and pengajuan_obj.formasi_id :
                name = pengajuan_obj.formasi_id.name
                if pengajuan_obj.user_id and pengajuan_obj.user_id.partner_id:
                    name=name+' '+pengajuan_obj.user_id.partner_id.name
            if not pengajuan_obj.status_masa_kerja :
                raise osv.except_osv(_('Maaf Tidak Bisa Mendaftar Izin Belajar'),
                                _('Persyaratan Masa Kerja Tidak Lolos, Silahkan Cek Data Kepegawaian Anda'))
            if not pengajuan_obj.status_pangkat_minimum :
                raise osv.except_osv(_('Maaf Tidak Bisa Mendaftar Izin Belajar'),
                                _('Persyaratan Pangkan Minimum Tidak Lolos, Silahkan Cek Data Riwayat Kepangkatan Kepegawaian Anda'))
            if company_id and not company_id.user_id_pl_opd :
                raise osv.except_osv(_('Maaf Konfigurasi OPD Tidak Lengkat'),
                                _('Konfigurasi Petugas Verifikasi Permohonan Untuk OPD Anda Belum Di Atur, Silahkan Hubungi Admin'))
            if company_id and not company_id.user_id_pl_bkd :
                raise osv.except_osv(_('Maaf Konfigurasi OPD Tidak Lengkap'),
                                _('Konfigurasi Petugas Verifikasi Dari BKD Untuk OPD Anda Belum Di Atur, Silahkan Hubungi Admin'))
            izin_belajar_ids = self.search(cr,uid,[('formasi_id','=',pengajuan_obj.formasi_id.id),('user_id','=',pengajuan_obj.user_id.id),('state','!=','draft'),('active','=',True)])
            if izin_belajar_ids and len(izin_belajar_ids) >0 :
                raise osv.except_osv(_('Maaf Tidak Bisa Mendaftar Izin Belajar'),
                                _('Anda Sudah Mendaftar Untuk Formasi Ini,Pendaftaran Tidak Boleh Lebih Dari 1 Kali'))
        return True
    def action_verifikasi_opd(self, cr, uid, ids, context=None):
        if not self.validate_data_kepegawaian(cr, uid, ids, context):
            raise osv.except_osv(_('Izin Belajar Tidak Bisa Di Proses!'),
                                        _('Persyaratan Tidak Mencukupi Untuk Formasi Izin Belajar Ini.'))
        return self.write(cr, uid, ids, {'state':'verifikasi_opd'}, context=context) 
    def action_verifikasi_bkd(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'verifikasi_bkd'}, context=context) 
    def action_confirm(self, cr, uid, ids, context):
        
        if self.validate_required_surat_keputusan(cr,uid,ids,context):
            return self.write(cr, uid, ids, {'state':'confirm'}, context=context)
        return False
    def action_draft(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'draft'}, context=context)
    
    def validate_required_surat_keputusan(self,cr,uid,ids,context=None):
        for o in self.browse(cr, uid, ids, context=context) :
            print "- ",o.no_sk_izin_belajar
            print "- 1 ",o.tanggal_surat_keputusan
            if not o.no_sk_izin_belajar and not  o.tanggal_surat_keputusan :
                raise osv.except_osv(_('Izin Belajar Tidak Bisa Di Proses!'),
                                        _('No SK Dan Tanggal SK Harus Di Isi Terlebih Dahulu'))
            if not o.no_sk_izin_belajar and o.tanggal_surat_keputusan :
                raise osv.except_osv(_('Izin Belajar Tidak Bisa Di Proses!'),
                                        _('Silahkan isi No SK Terlebih Dahulu'))
            if o.no_sk_izin_belajar and not  o.tanggal_surat_keputusan :
                raise osv.except_osv(_('Izin Belajar Tidak Bisa Di Proses!'),
                                        _('Silahkan isi Tanggal SK Terlebih Dahulu'))    
        
        return True
            
    def download_sk_izin_belajar(self, cr, uid, ids, context={}):
        
        datas = {
             'ids': ids,
             'izin_belajar_ids': ids,
             'src_model': 'izin.belajar',
                 }
        
        return {
            'type': 'ir.actions.report.xml',
             'report_name': 'sk.izin.belajar.report.form',
             'report_type': 'webkit',
             'datas': datas,
            }
        
    def download_sp_izin_belajar(self, cr, uid, ids, context={}):
        
        datas = {
             'ids': ids,
             'izin_belajar_ids': ids,
             'src_model': 'izin.belajar',
                 }
        
        return {
            'type': 'ir.actions.report.xml',
             'report_name': 'sp.izin.belajar.report.form',
             'report_type': 'webkit',
             'datas': datas,
            }
    
    
izin_belajar()
