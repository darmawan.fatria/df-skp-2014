import time
import netsvc
from tools.translate import _
import tools
from datetime import datetime, timedelta
from osv import fields, osv
from dateutil.relativedelta import relativedelta

class sk_izin_belajar_report(osv.osv_memory):
    _name = "sk.izin.belajar.report"

    #_columns = {
        #'name_pic' : fields.many2one('penandatangan.faktur', 'Penandatangan', required='True'),
    #}
    def print_sk_izin_belajar(self, cr, uid, ids, context={}):
        value = self.read(cr, uid, ids)[0]
        
        datas = {
             'ids': context.get('active_ids',[]),
             'izin_belajar_ids': context.get('active_ids',[]),
             'model': 'sk.izin.belajar.report',
             'src_model': 'izin.belajar',
             'form': value
                 }
        
        return {
            'type': 'ir.actions.report.xml',
             'report_name': 'sk.izin.belajar.report.form',
             'report_type': 'webkit',
             'datas': datas,
            }
sk_izin_belajar_report