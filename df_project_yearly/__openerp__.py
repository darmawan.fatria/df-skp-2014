##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

{
    "name": "Penilaian Pelaporan Target Tahunan",
    "version": "1.0",
    "author": "Darmawan Fatriananda",
    "category": "Target SKP",
    "description": """
    Penilaian Pelaporan Target Tahunan
    """,
    "website" : "http://mediasee.net",
    "license" : "GPL-3",
    "depends": [
                "df_project",
                ],
    'update_xml': [
                   "project_view.xml",
                   "project_tambahan_kreatifitas_yearly_view.xml",
                   "popup_view/project_skp_yearly_popup_view.xml",
                   "popup_view/project_tambahan_kreatifitas_popup_view.xml",
                   ],
    'installable': True,
    'active': False,
}
