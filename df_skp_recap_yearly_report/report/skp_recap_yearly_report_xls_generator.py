import time
import xlwt
import cStringIO
from xlwt import Workbook, Formula
from report_engine_xls import report_xls
import skp_recap_yearly_report_xls_generator
import skp_recap_yearly_report_parser
from skp_recap_yearly_report_parser import skp_recap_yearly_report_parser



class skp_recap_yearly_report_xls_generator(report_xls):
	
    def generate_xls_report(self, parser, filters, obj, workbook):
	#ws_summary = workbook.add_sheet(('Laporan Tahunan'))
	worksheet = workbook.add_sheet(('Lampiran Rekap SKP'))
	ws_target = workbook.add_sheet(('Lampiran Target SKP'))
	
	
        worksheet.panes_frozen = True
        worksheet.remove_splits = True
        worksheet.portrait = False # Landscape
        worksheet.fit_wiresult_datah_to_pages = 1
        worksheet.col(1).wiresult_datah = len("ABCDEFG")*1024
        worksheet.protect = True  # defaults to False
        worksheet.password = "how_do_you_turn_this_on"
        
       
        
        # Styles (It's used for writing rows / headers)
        int_number_style=  xlwt.easyxf("borders: top thin, bottom thin, left thin, right thin;",num_format_str='#,##0;(#,##0)')
        ak_number_style=  xlwt.easyxf("borders: top thin, bottom thin, left thin, right thin;",num_format_str='#,##0.000000;(#,##0.000000)')
        row_normal_style_align_right=  xlwt.easyxf("borders: top thin, bottom thin, left thin, right thin;align: wrap 1, vert centre,horiz right;",num_format_str='#,##0.00;(#,##0.00)')
        big_point_with_border_style = xlwt.easyxf('font: height 250, name Arial,  bold on, italic off; align: wrap on, vert centre, horiz center;pattern: pattern solid, fore_color white;borders: top thin, bottom thin, left thin, right thin;', num_format_str='#,##0.00;(#,##0.00)')
        invisible_style=  xlwt.easyxf("align: wrap 1, vert centre;",num_format_str='#,##0.00;(#,##0.00)')
        row_normal_style=  xlwt.easyxf("borders: top thin, bottom thin, left thin, right thin;align: wrap 1, vert centre;",num_format_str='#,##0.00;(#,##0.00)')
        row_normal_without_border_style=  xlwt.easyxf("align: wrap 1, vert centre;",num_format_str='#,##0.00;(#,##0.00)')
        row_normal_without_border_center_style=  xlwt.easyxf("align: wrap 1, vert centre,horiz centre;",num_format_str='#,##0.00;(#,##0.00)')
        header_normal_style=  xlwt.easyxf('align: wrap on,  vert centre, horiz left;borders: top thin, bottom thin, left thin, right thin;',num_format_str='#,##0.00;(#,##0.00)')
        bold_normal_style=  xlwt.easyxf('font: bold on;',num_format_str='#,##0;(#,##0)')
        bold_normal_with_border_style=  xlwt.easyxf('font: bold on;borders: top thin, bottom thin, left thin, right thin;align: wrap 1, vert centre;',num_format_str='#,##0;(#,##0)')
        title_with_border_style = xlwt.easyxf('font: height 220, name Arial,  colour_index black, bold on, italic off; align: wrap on, vert centre,horiz centre;pattern: pattern solid, fore_color white;borders: top thin, bottom thin, left thin, right thin;', num_format_str='#,##0.00;(#,##0.00)')
        title_style = xlwt.easyxf('font: height 220, name Arial,  colour_index black, bold on, italic off; align: wrap on, vert centre,horiz centre;pattern: pattern solid, fore_color white ;', num_format_str='#,##0.00;(#,##0.00)')
        header_with_border_style = xlwt.easyxf('font: height 200, name Arial, colour_index black, bold on, italic off; align: wrap on, vert centre, horiz center;pattern: pattern solid, fore_color white ;borders: top thin, bottom thin, left thin, right thin;', num_format_str='#,##0.00;(#,##0.00)')
        header_style = xlwt.easyxf('font: height 180, name Arial, colour_index black, bold on, italic off; align: wrap 1, vert centre, horiz center;pattern: pattern solid, fore_color white ;', num_format_str='#,##0.00;(#,##0.00)')
        sub_header_style = xlwt.easyxf('font: height 180, name Arial,  colour_index black, bold on, italic off; align: wrap on, vert centre, horiz center;pattern: pattern solid, fore_color white ;borders: top thin, bottom thin, left thin, right thin;', num_format_str='#,##0.00;(#,##0.00)')
        sum_style = xlwt.easyxf('font: height 180, name Arial, colour_index black, bold on, italic off; align: wrap on, vert centre;pattern: pattern solid,fore_color white ;', num_format_str='#,##0.00;(#,##0.00)')
		# Specifying columns, the order doesn't matter
		# lamda d,f,p: is a function who has filter,data,parser as the parameters it is expected to the value of the column
        target_specs = [
	    # Infos
	    ('Title_2',  14, 200, 'text', lambda x, d, p: 'PEGAWAI NEGERI SIPIL'),
	    ('Title_target_1', 14, 200, 'text', lambda x, d, p: 'FORMULIR SASARAN KERJA'),
	    ('fix_period', 8, 200, 'text', lambda x, d, p:  'Jangka Waktu Penilaian 1 Januari sd 31 Desember '+filters['form']['period_year']),
	    #Atribut Kepegawaian
	    ('no_', 1, 30, 'text', lambda x, d, p:  'NO'),
	    ('no_1', 1, 10, 'number', lambda x, d, p: 1,xlwt.Row.set_cell_number,int_number_style),
	    ('no_2', 1, 10, 'number', lambda x, d, p: 2,xlwt.Row.set_cell_number,int_number_style),
	    ('no_3', 1, 10, 'number', lambda x, d, p: 3,xlwt.Row.set_cell_number,int_number_style),
	    ('no_4', 1, 10, 'number', lambda x, d, p: 4,xlwt.Row.set_cell_number,int_number_style),
	    ('no_5', 1, 10, 'number', lambda x, d, p: 5,xlwt.Row.set_cell_number,int_number_style),
	    ('no_6', 1, 10, 'number', lambda x, d, p: 6,xlwt.Row.set_cell_number,int_number_style),
	    ('PEJABAT PENILAI', 6, 400, 'text', lambda x, d, p:  'I. PEJABAT PENILAI'),
	    ('PEGAWAI NEGERI SIPIL YANG DINILAI', 6, 400, 'text', lambda x, d, p:  'II. PEGAWAI NEGERI SIPIL YANG DINILAI'),
	    ('nama', 2, 80, 'text', lambda x, d, p:  'Nama'),
	    ('nip', 2, 80, 'text', lambda x, d, p:  'NIP'),
	    ('pangkat_gol', 2, 80, 'text', lambda x, d, p:  'Pangkat/Gol.Ruang'),
	    ('jabatan', 2, 80, 'text', lambda x, d, p:  'Jabatan'),
	    ('unit_kerja', 2, 80, 'text', lambda x, d, p:  'Unit Kerja'),
	    
	    ('nama_atasan', 4, 200, 'text', lambda x, d, p:  d and d.user_id_atasan and d.user_id_atasan.name or '' ),
	    ('nama_pegawai', 4, 200, 'text', lambda x, d, p:  d and d.name or '' ),
	    ('nip_atasan', 4, 200, 'text', lambda x, d, p:  d and d.user_id_atasan and d.user_id_atasan.nip or '' ),
	    ('nip_pegawai', 4, 200, 'text', lambda x, d, p:  d and d.nip or '' ),
	    ('golongan_atasan', 4, 200, 'text', lambda x, d, p:  d and d.user_id_atasan and d.user_id_atasan.golongan_id and d.user_id_atasan.golongan_id.name or '' ),
	    ('golongan_pegawai', 4, 200, 'text', lambda x, d, p:  d and d.golongan_id and d.golongan_id.name or '' ),
	    ('jabatan_atasan', 4, 200, 'text', lambda x, d, p:  d and d.user_id_atasan and d.user_id_atasan.job_id and d.user_id_atasan.job_id.name or '' ),
	    ('jabatan_pegawai', 4, 200, 'text', lambda x, d, p:  d and d.job_id and d.job_id.name or '' ),
	    ('unit_kerja_atasan', 4, 200, 'text', lambda x, d, p:  d and d.user_id_atasan and d.user_id_atasan.department_id and d.user_id_atasan.department_id.name or '' ),
	    ('unit_kerja_pegawai', 4, 200, 'text', lambda x, d, p:  d and d.department_id and d.department_id.name or '' ),
	    ('company_pegawai', 1, 200, 'text', lambda x, d, p:  d and d.company_id and d.company_id.name or '' ),
	    ('nama_banding', 1, 200, 'text', lambda x, d, p:  d and d.user_id_banding and d.user_id_banding.name or '' ),
	    ('nip_banding', 1, 200, 'text', lambda x, d, p:  d and d.user_id_banding and d.user_id_banding.nip or '' ),
	    ('golongan_banding', 1, 200, 'text', lambda x, d, p:  d and d.user_id_banding and d.user_id_banding.golongan_id and d.user_id_banding.golongan_id.name or '' ),
	    ('jabatan_banding', 1, 200, 'text', lambda x, d, p:  d and d.user_id_banding and d.user_id_banding.job_id and d.user_id_banding.job_id.name or '' ),
	    ('unit_kerja_banding', 1, 200, 'text', lambda x, d, p:  d and d.user_id_banding and d.user_id_banding.department_id and d.user_id_banding.department_id.name or '' ),
	    
	    
	    # Header Atribut Kegiatan
	    ('NO', 1, 30, 'text', lambda x, d, p:  'NO'),
	    ('III.KEGIATAN TUGAS POKOK JABATAN', 3, 400, 'text', lambda x, d, p:  'III.KEGIATAN TUGAS POKOK JABATAN'),
	    ('I.KEGIATAN TUGAS POKOK JABATAN', 3, 400, 'text', lambda x, d, p:  'III.KEGIATAN TUGAS POKOK JABATAN'),
	    ('III.KEGIATAN TUGAS POKOK JABATAN', 3, 400, 'text', lambda x, d, p:  'III.KEGIATAN TUGAS POKOK JABATAN'),
	    ('II.TUGAS TAMBAHAN DAN KREATIFITAS / UNSUR PENUNJANG', 3, 400, 'text', lambda x, d, p:  'II.TUGAS TAMBAHAN DAN KREATIFITAS / UNSUR PENUNJANG'),
	    ('a.Tugas Tambahan', 3, 200, 'text', lambda x, d, p:  'a.Tugas Tambahan'),
	    ('b.Kreatifitas', 3, 200, 'text', lambda x, d, p:  'b.Kreatifitas'),
	    ('TARGET', 6, 100, 'text', lambda x, d, p:  'TARGET'),
	    ('REALISASI', 6, 100, 'text', lambda x, d, p:  'REALISASI'),
	    ('ANGKA KREDIT', 1, 40, 'text', lambda x, d, p:  'AK'),
	    ('KUANT / OUTPUT', 2, 100, 'text', lambda x, d, p:  'KUANT / OUTPUT'),
	    ('KUAL / MUTU', 1, 100, 'text', lambda x, d, p:  'KUAL / MUTU'),
	    ('WAKTU', 1, 100, 'text', lambda x, d, p:  'WAKTU'),
	    ('BIAYA', 1, 100, 'text', lambda x, d, p:  'BIAYA'),
	    ('PERHITUNGAN', 1, 100, 'text', lambda x, d, p:  'PERHITUNGAN'),
	    ('NILAI CAPAIAN SKP', 1, 100, 'text', lambda x, d, p:  'NILAI CAPAIAN SKP'),
	    ('TOTAL NILAI CAPAIAN SKP', 17, 200, 'text', lambda x, d, p:  'TOTAL NILAI CAPAIAN SKP'),
	    ('1', 1, 15, 'text', lambda x, d, p:  'NO'),
	    ('2', 3, 70, 'text', lambda x, d, p:  'III.KEGIATAN TUGAS POKOK JABATAN'),
	    ('3', 1, 25, 'text', lambda x, d, p:  'ANGKA KREDIT'),
	    ('4', 2, 20, 'text', lambda x, d, p:  'KUANT / OUTPUT'),
	    ('5', 1, 25, 'text', lambda x, d, p:  'KUAL / MUTU'),
	    ('6', 1, 25, 'text', lambda x, d, p:  'WAKTU'),
	    ('7', 1, 70, 'text', lambda x, d, p:  'BIAYA'),
	    ('8', 1, 25, 'text', lambda x, d, p:  'ANGKA KREDIT'),
	    ('9', 2, 30, 'text', lambda x, d, p:  'KUANT / OUTPUT'),
	    ('10', 1, 25, 'text', lambda x, d, p:  'KUAL / MUTU'),
	    ('11', 1, 25, 'text', lambda x, d, p:  'WAKTU'),
	    ('12', 1, 50, 'text', lambda x, d, p:  'BIAYA'),
	    ('13', 1, 25, 'text', lambda x, d, p:  'PERHITUNGAN'),
	    ('14', 1, 25, 'text', lambda x, d, p:  'NILAI CAPAIAN SKP'),
		]
        #Spek Header data
        target_header_info_top_template = self.xls_row_template(target_specs, ['quadruple_empty_column','TARGET'])
        target_header_info_template = self.xls_row_template(target_specs,['NO','III.KEGIATAN TUGAS POKOK JABATAN','ANGKA KREDIT','KUANT / OUTPUT','KUAL / MUTU','WAKTU','BIAYA'])
        target_realisasi_header_info_top_template = self.xls_row_template(target_specs, ['quadruple_empty_column','TARGET',])
        target_realisasi_header_info_template = self.xls_row_template(target_specs,['NO','III.KEGIATAN TUGAS POKOK JABATAN','ANGKA KREDIT','KUANT / OUTPUT','KUAL / MUTU','WAKTU','BIAYA'])
        sub_target_realisasi_header_info_template = self.xls_row_template(target_specs, ['1','2','3','4','5','6','7',])
        
        row_spec_value = ['seq','nama_kegiatan','real_target_ak','real_target_kuantitas','real_target_satuan_kuantitas','real_target_kualitas','real_target_waktu','real_target_biaya',
						  ]
        row_template = self.xls_row_template(target_specs,row_spec_value)
        cols_specs = [
	    # ('header', column_span, column_type, lamda function)
	    # Infos
	    ('Title_1', 18, 200, 'text', lambda x, d, p: 'PENILAIAN SASARAN KERJA'),
	    ('Title_2',  18, 200, 'text', lambda x, d, p: 'PEGAWAI NEGERI SIPIL'),
	    ('Title_target_1', 18, 200, 'text', lambda x, d, p: 'FORMULIR SASARAN KERJA'),
	    ('fix_period', 8, 200, 'text', lambda x, d, p:  'Jangka Waktu Penilaian 1 Januari sd 31 Desember '+filters['form']['period_year']),
	    
	    #Data Atribut Kegiatan
	    ('seq', 1, 20, 'number', lambda x, d, p: d.sequence,xlwt.Row.set_cell_number,int_number_style),
	    ('nama_kegiatan', 3, 200, 'text', lambda x, d, p:  d.name),
	    ('target_ak', 1, 100, 'number', lambda x, d, p:  d.target_angka_kredit,xlwt.Row.set_cell_number,ak_number_style),
	    ('target_kuantitas', 1, 100, 'number', lambda x, d, p: d.target_jumlah_kuantitas_output,xlwt.Row.set_cell_number,int_number_style),
	    ('target_satuan_kuantitas', 1, 100, 'text', lambda x, d, p:  d.target_satuan_kuantitas_output and d.target_satuan_kuantitas_output.name or ''),
	    ('target_kualitas', 1, 100, 'number', lambda x, d, p:  d.target_kualitas,xlwt.Row.set_cell_number,int_number_style),
	    ('target_waktu', 1, 100, 'number', lambda x, d, p:  d.target_waktu,xlwt.Row.set_cell_number,int_number_style),
	    ('target_biaya', 1, 200, 'number', lambda x, d, p:  d.target_biaya),
	    ('real_target_ak', 1, 100, 'number', lambda x, d, p:  d.target_angka_kredit,xlwt.Row.set_cell_number,ak_number_style),
	    ('real_target_kuantitas', 1, 100, 'number', lambda x, d, p: d.target_jumlah_kuantitas_output,xlwt.Row.set_cell_number,int_number_style),
	    ('real_target_satuan_kuantitas', 1, 100, 'text', lambda x, d, p:  d.target_satuan_kuantitas_output and d.target_satuan_kuantitas_output.name or ''),
	    ('real_target_kualitas', 1, 100, 'number', lambda x, d, p:  d.target_kualitas,xlwt.Row.set_cell_number,int_number_style),
	    ('real_target_waktu', 1, 100, 'number', lambda x, d, p:  d.target_waktu,xlwt.Row.set_cell_number,int_number_style),
	    ('real_target_biaya', 1, 200, 'number', lambda x, d, p:  d.target_biaya),
	    ('realisasi_ak', 1, 100, 'number', lambda x, d, p:  d.realisasi_angka_kredit,xlwt.Row.set_cell_number,ak_number_style),
	    ('realisasi_kuantitas', 1, 100, 'number', lambda x, d, p:  d.realisasi_jumlah_kuantitas_output,xlwt.Row.set_cell_number,int_number_style),
	    ('realisasi_satuan_kuantitas', 1, 100, 'text', lambda x, d, p:  d.realisasi_satuan_kuantitas_output and d.realisasi_satuan_kuantitas_output.name or ''),
	    ('realisasi_kualitas', 1, 100, 'number', lambda x, d, p:  d.realisasi_kualitas,xlwt.Row.set_cell_number,int_number_style),
	    ('realisasi_waktu', 1, 100, 'number', lambda x, d, p:  d.realisasi_waktu,xlwt.Row.set_cell_number,int_number_style),
	    ('realisasi_biaya', 1, 200, 'number', lambda x, d, p:  d.realisasi_biaya),
	    ('total_nilai_target_skp', 1, 200, 'number', lambda x, d, p:  d.jumlah_perhitungan),
	    ('total_capaian_target_skp', 1, 200, 'number', lambda x, d, p:  d.nilai_akhir),
	    ('nilai_tugas_tambahan', 1, 200, 'number', lambda x, d, p:  0), #tambahan dan kreatifitas
	    ('nilai_kreatifitas', 1, 200, 'number', lambda x, d, p:  0),#tambahan dan kreatifitas
	    ('sum_nilai_capaian_skp', 1, 100, 'number', lambda x, d, p: d['sum_nilai_capaian_skp']),
	    
	    #Footer
	    ('footer_tanggal', 5, 100, 'text', lambda x, d, p:  p.get_concat_with_format_date('Bandung, ',filters)),
	    ('footer_pejabat_penilai', 5, 300, 'text', lambda x, d, p:  'Pejabat Penilai'),
	    ('footer_nama_atasan', 5, 200, 'text', lambda x, d, p:  d and d.user_id_atasan and d.user_id_atasan.name or '' ),
	    ('footer_nip_atasan', 5, 200, 'text', lambda x, d, p:  d and d.user_id_atasan and d.user_id_atasan.nip or '' ),
	    ('footer_pns_yg_dinilai', 5, 300, 'text', lambda x, d, p:  'Pegawai Negeri Sipil Yang Dinilai'),
	    ('footer_nama_pns', 5, 200, 'text', lambda x, d, p:  d and  d.name or '' ),
	    ('footer_nip_pns', 5, 200, 'text', lambda x, d, p:  d and d.nip or '' ),
	    # Misc
	    ('single_empty_column', 1, 0, 'text', lambda x, d, p: ''),
	    ('double_empty_column', 2, 0, 'text', lambda x, d, p: ''),
	    ('triple_empty_column', 3, 0, 'text', lambda x, d, p: ''),
	    ('quadruple_empty_column', 4, 0, 'text', lambda x, d, p: ''),
	    ('12_empty_column', 12, 0, 'text', lambda x, d, p: ''),
	    
	    # Header Atribut Perilaku
	    # No
	    ('no_1', 1, 20, 'number', lambda x, d, p: 1,xlwt.Row.set_cell_number,int_number_style),
	    ('no_2', 1, 20, 'number', lambda x, d, p: 2,xlwt.Row.set_cell_number,int_number_style),
	    ('no_3', 1, 20, 'number', lambda x, d, p: 3,xlwt.Row.set_cell_number,int_number_style),
	    ('no_4', 1, 20, 'number', lambda x, d, p: 4,xlwt.Row.set_cell_number,int_number_style),
	    ('no_5', 1, 20, 'number', lambda x, d, p: 5,xlwt.Row.set_cell_number,int_number_style),
	    ('no_6', 1, 20, 'number', lambda x, d, p: 6,xlwt.Row.set_cell_number,int_number_style),
	    ('UNSUR PENILAIAN', 2, 200, 'text', lambda x, d, p:  'UNSUR PENILAIAN'),
	    ('NILAI', 2, 200, 'text', lambda x, d, p:  'UNSUR NILAI'),
	    ('KETERANGAN', 3, 300, 'text', lambda x, d, p:  'KETERANGAN'),
	    ('Orientasi Pelayanan', 2, 200, 'text', lambda x, d, p:  'Orientasi Pelayanan'),
	    ('Integritas', 2, 200, 'text', lambda x, d, p:  'Integritas'),
	    ('Komitmen', 2, 200, 'text', lambda x, d, p:  'Komitmen'),
	    ('Disiplin', 2, 200, 'text', lambda x, d, p:  'Disiplin'),
	    ('Kerjasama', 2, 200, 'text', lambda x, d, p:  'Kerjasama'),
	    ('Kepemimpinan',2, 200, 'text', lambda x, d, p:  'Kepemimpinan'),
	    
	    ('nilai_orientasi_pelayanan', 2, 200, 'number', lambda x, d, p:  d.nilai_pelayanan),
	    ('nilai_integritas', 2, 200, 'number', lambda x, d, p:  d.nilai_integritas),
	    ('nilai_komitmen', 2, 200, 'number', lambda x, d, p:  d.nilai_komitmen),
	    ('nilai_disiplin', 2, 200, 'number', lambda x, d, p:  d.nilai_disiplin),
	    ('nilai_kerjasama', 2, 200, 'number', lambda x, d, p:  d.nilai_kerjasama),
	    ('nilai_kepemimpinan', 2, 200, 'number', lambda x, d, p:  d.nilai_kepemimpinan),
	    ('indeks_nilai_perilaku', 3, 300, 'text', lambda x, d, p:  ''),
	    ('b. Perilaku Kerja', 1, 100, 'text', lambda x, d, p:  'b. Perilaku Kerja'),
	    
		]
        
        #HEADER
        title_1_template = self.xls_row_template(target_specs, ['Title_target_1'])
        title_2_template = self.xls_row_template(target_specs, ['Title_2'])
        #KEPEGAWAIAN
        header_pegawai_template = self.xls_row_template(target_specs, ['NO','PEJABAT PENILAI','NO','PEGAWAI NEGERI SIPIL YANG DINILAI'])
        header_nama_pegawai_template = self.xls_row_template(target_specs, ['no_1','nama','nama_atasan','no_1','nama','nama_pegawai'])
        header_nip_pegawai_template = self.xls_row_template(target_specs, ['no_2','nip','nip_atasan','no_2','nip','nip_pegawai'])
        header_golongan_pegawai_template = self.xls_row_template(target_specs, ['no_3','pangkat_gol','golongan_atasan','no_3','pangkat_gol','golongan_pegawai'])
        header_jabatan_pegawai_template = self.xls_row_template(target_specs, ['no_4','jabatan','jabatan_atasan','no_4','jabatan','jabatan_pegawai'])
        header_unit_kerja_pegawai_template = self.xls_row_template(target_specs, ['no_5','unit_kerja','unit_kerja_atasan','no_5','unit_kerja','unit_kerja_pegawai'])
        
 
 #Get Data
        data_rekap_tahunan_pegawai = parser.get_recap_yearly_report_raw(filters);
        data_pegawai = parser.get_atribut_kepegawaian(filters);
               
#SKP ===========================================================================================================
        row_count = 0
        #   Target Tahunan
#WRITE HEADER
        self.xls_write_row(worksheet, filters, None, parser, row_count, title_1_template, title_style)
        row_count+=1
        self.xls_write_row(worksheet, filters, None, parser, row_count, title_2_template, title_style)
        row_count+=1
 # Write DATA KEPEGAWAIAN
        row_count+=1
        self.xls_write_row(worksheet, filters, None, parser, row_count, header_pegawai_template, header_with_border_style)
        #row_count+=1
        #self.xls_write_row_header(worksheet, row_count, header_pegawai_template,header_style, set_column_size=False)
        row_count+=1
        self.xls_write_row(worksheet, filters, data_pegawai, parser, row_count, header_nama_pegawai_template, header_normal_style)
        row_count+=1
        self.xls_write_row(worksheet, filters, data_pegawai, parser, row_count, header_nip_pegawai_template, header_normal_style)
        row_count+=1
        self.xls_write_row(worksheet, filters, data_pegawai, parser, row_count, header_golongan_pegawai_template, header_normal_style)
        row_count+=1
        self.xls_write_row(worksheet, filters, data_pegawai, parser, row_count, header_jabatan_pegawai_template, header_normal_style)
        row_count+=1
        self.xls_write_row(worksheet, filters, data_pegawai, parser, row_count, header_unit_kerja_pegawai_template, header_normal_style)
        row_count+=1
#HEADER TARGET REALISASI
        
        row_count+=1
        self.xls_write_row(worksheet,filters,None,parser, row_count, target_header_info_top_template, header_with_border_style,)
        row_count+=1
        self.xls_write_row_header(worksheet, row_count, target_realisasi_header_info_template, header_with_border_style, set_column_size=False)
        row_count+=1
        self.xls_write_row_header(worksheet, row_count, sub_target_realisasi_header_info_template, sub_header_style, set_column_size=True)
        
		#REALISASI
        row_count+=1
        row_data=row_count
        idx=1
        total_capaian_skp=jumlah_capaian_skp=0
        result = parser.get_skp_recap_yearly_report_raw(filters);
        for employee_data_summary_data in result:
            # Write Rows
                #employee_data_summary_data.sequence=1
                self.xls_write_row_with_indeks(worksheet, filters, employee_data_summary_data, parser, row_data, row_template, row_normal_style,idx)
                row_data+=1
                total_capaian_skp+=employee_data_summary_data.total_capaian_target_skp
                jumlah_capaian_skp+=1
                idx+=1
                
 

    # Override from report_engine_xls.py	
    def create_source_xls(self, cr, uid, ids, filters, report_xml, context=None): 
        if not context: context = {}
	
	# Avoiding context's values change
        context_clone = context.copy()
        
        rml_parser = self.parser(cr, uid, filters['report_file_name'] , context=context_clone)
        objects = self.getObjects(cr, uid, ids, context=context_clone)
        rml_parser.set_context(objects, filters, ids, 'xls')
        io = cStringIO.StringIO()
        workbook = xlwt.Workbook(encoding='utf-8')
        self.generate_xls_report(rml_parser, filters, rml_parser.localcontext['objects'], workbook)
        workbook.save(io)
        io.seek(0)
        return (io.read(), 'xls')

#Start the reporting service
skp_recap_yearly_report_xls_generator(
    #name (will be referred from skp_recap_yearly_report.py, must add "report." as prefix)
    'report.skp.recap.yearly.xls',
    #model
    'skp.recap.yearly.report',
    #file
    'addons/df_skp_recap_yearly_report/report/skp_recap_yearly_report.xls',
    #parser
    parser=skp_recap_yearly_report_parser,
    #header
    header=True
)