import time
import xlwt
import cStringIO
from xlwt import Workbook, Formula
from report_engine_xls import report_xls
import izin_belajar_recap_report_xls_generator
import izin_belajar_recap_report_parser
from izin_belajar_recap_report_parser import izin_belajar_recap_report_parser

class izin_belajar_recap_report_xls_generator(report_xls):
	
    def generate_xls_report(self, parser, filters, obj, workbook):
	worksheet = workbook.add_sheet(('Rekapitulasi Izin Belajar'))
        worksheet.panes_frozen = True
        worksheet.remove_splits = True
        worksheet.portrait = True # Landscape
        worksheet.fit_wiresult_datah_to_pages = 1
        worksheet.col(1).wiresult_datah = len("ABCDEFG")*1024
        
        int_number_style=  xlwt.easyxf("borders: top thin, bottom thin, left thin, right thin;",num_format_str='#,##0;(#,##0)')

	# Specifying columns, the order doesn't matter
	# lamda d,f,p: is a function who has filter,data,parser as the parameters it is expected to the value of the column
        cols_specs = [
	    # ('header', column_span, column_type, lamda function)
	    
	    # Infos
	    ('Company', 2, 200, 'text', lambda x, d, p: 'Badan Kepegawaian Daerah Provinsi Jawa Barat'),
	    ('Title',  2, 0, 'text', lambda x, d, p: p.get_title('Rekapitulasi Izin Belajar',filters)),
	    
	   	    # Main Headers / Rows
	    
	    ('NO', 1, 30, 'number', lambda x, d, p:  0,xlwt.Row.set_cell_number,int_number_style),
	    ('TANGGAL SURAT', 1, 100, 'text', lambda x, d, p:  p.get_format_date(d.tanggal_surat_keputusan)),
	    ('OPD', 1, 150, 'text', lambda x, d, p:  d.company_id.name),
	    ('NAMA', 1, 150, 'text', lambda x, d, p:  d.employee_id.name),
	    ('NIP', 1, 100, 'text', lambda x, d, p:  d.employee_id.nip),
	    ('PANGKAT/GOLONGAN', 1, 100, 'text', lambda x, d, p:  d.golongan_id and d.golongan_id.name),
	    ('PROGRAM STUDI', 1, 150, 'text', lambda x, d, p:  d.program_studi_id and d.program_studi_id.name),
	    ('THN AKADEMIK', 1, 80, 'text', lambda x, d, p:  d.tahun_akademik),
	    ('NO SURAT IZIN', 1, 100, 'text', lambda x, d, p:  d.no_sk_izin_belajar),
	    
	    #Other
	    ('header_jenjang_pendidikan', 9, 100, 'text', lambda x, d, p: d['level_pendidikan_name']),
	    
        # Misc
	    ('single_empty_column', 1, 0, 'text', lambda x, d, p: ''),
	    ('triple_empty_column', 3, 0, 'text', lambda x, d, p: ''),
	    ('quadruple_empty_column', 4, 0, 'text', lambda x, d, p: ''),
	]
    

        row_spec_value = ['NO','TANGGAL SURAT','OPD','NAMA','NIP','PANGKAT/GOLONGAN','PROGRAM STUDI','THN AKADEMIK','NO SURAT IZIN']
        # Row templates (Order Matters, this joins the columns that are specified in the second parameter)
        company_template = self.xls_row_template(cols_specs, ['single_empty_column','Company'])
        title_template = self.xls_row_template(cols_specs, ['single_empty_column','Title'])
        
        row_template = self.xls_row_template(cols_specs,row_spec_value)
        header_level_pendidikan_template = self.xls_row_template(cols_specs, ['header_jenjang_pendidikan',])
        empty_row_template = self.xls_row_template(cols_specs, ['single_empty_column'])

        # Styles (It's used for writing rows / headers)
        row_normal_style=  xlwt.easyxf("borders: top thin, bottom thin, left thin, right thin;align: wrap 1, vert centre;",num_format_str='#,##0.00;(#,##0.00)')
        row_normal_odd_style=  xlwt.easyxf("borders: top thin, bottom thin, left thin, right thin;align: wrap 1, vert centre;",num_format_str='#,##0.00;(#,##0.00)')
        info_style = xlwt.easyxf('font: height 200, name Arial, colour_index white, bold on, italic off; align: wrap on, vert centre;pattern: pattern solid, fore_color gray50;', num_format_str='#,##0.00;(#,##0.00)')
        top_style = xlwt.easyxf('font: height 200, name Arial, colour_index white, bold on, italic off; align: wrap on, vert centre, horiz center;pattern: pattern solid, fore_color orange;', num_format_str='#,##0.00;(#,##0.00)')
        header_style = xlwt.easyxf('font: height 200, name Arial, colour_index white, bold on, italic off; align: wrap 1, vert centre, horiz center;pattern: pattern solid, fore_color green;borders: top thin, bottom thin, left thin, right thin;', num_format_str='#,##0.00;(#,##0.00)')
        sum_style = xlwt.easyxf('font: height 200, name Arial, colour_index white, bold on, italic off; align: wrap on, vert centre;pattern: pattern solid, fore_color gray50;', num_format_str='#,##0.00;(#,##0.00)')
        sub_header_style = xlwt.easyxf('font: height 230, name Arial, bold on, italic off; align: wrap on, vert centre, horiz center;pattern: pattern solid, fore_color green;borders: top thin, bottom thin, left thin, right thin;', num_format_str='#,##0.00;(#,##0.00)')
		 
        # Write infos
        # xls_write_row(worksheet, filters, data parser, row_number, template, style)
        row_count=0
        self.xls_write_row(worksheet, filters, None, parser, row_count, company_template, info_style)
        row_count+=1
        self.xls_write_row(worksheet, filters, None, parser, row_count, title_template, info_style)
        
        row_count+=2
            # Write headers (It uses the first parameter of cols_specs)
        sub_header={}
        level_pendidikan_ids = parser.get_all_level_pendidikan()
        for level_pendidikan_id in level_pendidikan_ids :
	        result = parser.get_izin_belajar_recap_report_raw(filters,level_pendidikan_id.id);
	        if not result : continue;
	        if result and len(result)==0:continue;
	        
	        sub_header['level_pendidikan_name'] = level_pendidikan_id.name
	        row_count+=1
        	self.xls_write_row(worksheet, filters, sub_header, parser, row_count, header_level_pendidikan_template, sub_header_style)
	        row_count+=1
	        self.xls_write_row_header(worksheet, row_count, row_template, header_style, set_column_size=True)
	        row_count+=1
	        idx=1
	        
	        for izin_belajar_recap_data in result:
	            # Write Rows
	                self.xls_write_row_with_indeks(worksheet, filters, izin_belajar_recap_data, parser, row_count, row_template, row_normal_style,idx)
	                row_count+=1
	                idx+=1
        	row_count+=2
		        

        # Write Totals

    # Override from report_engine_xls.py	
    def create_source_xls(self, cr, uid, ids, filters, report_xml, context=None): 
        if not context: context = {}
	
	# Avoiding context's values change
        context_clone = context.copy()
	
        rml_parser = self.parser(cr, uid, self.name2, context=context_clone)
        objects = self.getObjects(cr, uid, ids, context=context_clone)
        rml_parser.set_context(objects, filters, ids, 'xls')
        io = cStringIO.StringIO()
        workbook = xlwt.Workbook(encoding='utf-8')
        self.generate_xls_report(rml_parser, filters, rml_parser.localcontext['objects'], workbook)
        workbook.save(io)
        io.seek(0)
        return (io.read(), 'xls')

#Start the reporting service
izin_belajar_recap_report_xls_generator(
    #name (will be referred from izin_belajar_recap_report.py, must add "report." as prefix)
    'report.izin.belajar.recap.xls',
    #model
    'izin.belajar.recap.report',
    #file
    'addons/df_izin_belajar_recap_report/report/izin_belajar_recap_report.xls',
    #parser
    parser=izin_belajar_recap_report_parser,
    #header
    header=True
)
