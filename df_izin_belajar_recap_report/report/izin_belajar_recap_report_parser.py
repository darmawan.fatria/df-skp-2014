import pooler
import time
from datetime import datetime
import tools
import logging
from report import report_sxw
from datetime import datetime
from report_webkit import webkit_report
import locale

class izin_belajar_recap_report_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context=None):
        locale.setlocale(locale.LC_ALL, 'id_ID.utf8')
        super(izin_belajar_recap_report_parser, self).__init__(cr, uid, name, context=context)
    
    
    def get_all_level_pendidikan(self,context=None):
        level_pool = self.pool.get('partner.employee.study.degree')
        level_ids= level_pool.search(self.cr,self.uid,[],order="degree_level desc")
        results = level_pool.browse(self.cr, self.uid, level_ids)
        return results;
    
    def get_izin_belajar_recap_report_raw(self,filters,level_pendidikan_id,context=None):
        izin_belajar_pool = self.pool.get('izin.belajar')
        p_tahun_pengajuan=filters['form']['tahun_pengajuan']
        izin_belajar_ids= izin_belajar_pool.search(self.cr,self.uid,[('state','=','confirm'),
                                                                     ('tahun_pengajuan','=',p_tahun_pengajuan),
                                                                     ('level_pendidikan_id','=',level_pendidikan_id)
                                                                     
                                                                     ],order="company_id,tanggal_surat_keputusan asc")
        results = izin_belajar_pool.browse(self.cr, self.uid, izin_belajar_ids)
        return results;
    def get_format_date(self,a_date):
        formatted_print_date=''
        try :
            formatted_print_date = time.strftime('%d %B %Y', time.strptime(a_date,'%Y-%m-%d'))
        except :
            formatted_print_date='-'
            
        return formatted_print_date
    
    def get_title(self,prefiks,filters):
        p_tahun_pengajuan=filters['form']['tahun_pengajuan']
      
        return prefiks+' '+p_tahun_pengajuan
   
    
