from osv import fields, osv


class izin_belajar_recap_report(osv.osv_memory):
    
    _name = "izin.belajar.recap.report"
    _columns = {
        'tahun_pengajuan' :fields.char('Tahun Pengajuan',size=4,required=True),
        'company_id': fields.many2one('res.company', 'OPD',),
        'level_pendidikan_id': fields.many2one('partner.employee.study.degree', 'Jenjang Pendidikan',),
    }
    
    def get_izin_belajar_recap_report(self, cr, uid, ids, context={}):
        value = self.read(cr, uid, ids)[0]
        datas = {
            'ids': context.get('active_ids',[]),
            'model': 'izin.belajar.recap.report',
            'form': value
        }
	
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'izin.belajar.recap.xls',
            'report_type': 'webkit',
            'datas': datas,
        }
    
izin_belajar_recap_report()
