##############################################################################
#
#    Alam Dewata Utama, PT
#    Copyright (c) 2011 - 2012 ADSOFT <http://www.adsoft.co.id>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

{
    "name": "Laporan Rekap SKP Bulanan Pegawai",
    "version": "1.1",
    "author": "Darmawan Fatriananda",
    "category": "SKP/Laporan/Rekap Bulanan",
    "description": """
Laporan Rekap SKP Bulanan Pegawai
    """,
    "website" : "http://www.mediasee.net",
    "license" : "GPL-3",
    "depends": ['df_skp_employee' 
                ],
    "init_xml": [ ],
    'update_xml': [
                   "skp_employee_recap_monthly_report_view.xml",
                   ],
    'demo_xml': [],
    'installable': True,
    'active': False,
#    'certificate': 'certificate',
}
