<!DOCTYPE html SYSTEM "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<style>
body{
    font-family: verdana,arial,sans-serif;
	font-size:12px;
	background-color:white;
}

table.header_table {
	border-width: 0px;
	border-collapse: collapse;
}
table.header_table td {
	border-top:0.5px;
	border-bottom:0.5px;
	border-right:0px;
	border-left:0px;
    border-style: solid;
	padding: 5px;
}
table.content_table {
	border-width: 0px;
	border-collapse: collapse;
}
table.content_table th {
	border-width: 12px;
    border-top:0.5px;
	border-bottom:0.5px;
	border-right:0px;
	border-left:0px;
    border-style: solid;
    padding: 5px;
    vertical-align: top;
    background-color: #1a9e79;
    font-size:12px;
    text-align: center;
    
}
table.content_table td {
	border-width: 12px;
    border-top:0.5px;
	border-bottom:0.5px;
	border-right:0px;
	border-left:0px;
    border-style: solid;
    padding: 5px;
    vertical-align: top;
    text-align: right;
}
.text_left{
	text-align: left;
}
.text_center{
	text-align: center;
}
</style>
</head>
<body>
	<%
		e = get_data_pegawai(data)
		
	%>
    <table class="header_table">
            <tr>
                <td>Nama </td>
                <td>:</td>
                <td>${ e and e.name}  </td>
            </tr>
            <tr>
                <td>NIP </td>
                <td>:</td>
                <td>${ e and e.nip} </td>
            </tr>
            <tr>
                <td>Jabatan </td>
                <td>:</td>
                <td>${ e and e.job_id and e.job_id.name} </td>
            </tr>
            <tr>
                <td>Unit Kerja </td>
                <td>:</td>
                <td>${ e and e.department_id and e.department_id.name}</td>
            </tr>
            <tr>
                <td>OPD</td>
                <td>:</td>
                <td>${ e and e.company_id and e.company_id.name}</td>
            </tr>
    </table>
    <br />
    <table class="content_table">
            <tr>
                <TH>BULAN </TH>
                <TH>TAHUN</TH>
                <TH>JUMLAH SKP</TH>
                <TH> SKP</TH>
                <TH> TAMBAHAN</TH>
                <TH>  KREATIFITAS</TH>
                <TH>  PELAYANAN</TH>
                <TH>  INTEGRITAS</TH>
                <TH>  KOMITMEN</TH>
                <TH>  DISIPLIN</TH>
                <TH>  KERJASAMA</TH>
                <TH>  KEPEMIMPINAN</TH>
                <TH>  PERILAKU</TH>
                <TH> SKP (%)</TH>
                <TH> SKP (%) + TAMBAHAN</TH>
                <TH> PERILAKU (%)</TH>
                <TH> TOTAL (%)</TH>
                <TH>INDEKS NILAI</TH>
            </tr>
      		%for o in get_data(data) :
            <tr>
                <td> <div class="text_left" > ${ get_period(o.target_period_month) }  </div> </td>
                <td> <div class="text_center" > ${o.target_period_year} </div> </td>
                <td> <div class="text_left" > ${o.skp_state_count} </div> </td>
                <td> ${formatLang (o.nilai_skp) }  </td>
                <td> ${formatLang (o.fn_nilai_tambahan) }  </td>
                <td> ${formatLang (o.fn_nilai_kreatifitas) }  </td>
                <td> ${formatLang (o.nilai_pelayanan) }  </td>
                <td> ${formatLang (o.nilai_integritas) }   </td>
                <td> ${formatLang (o.nilai_komitmen) }    </td>
                <td> ${formatLang (o.nilai_disiplin) }   </td>
                <td> ${formatLang (o.nilai_kerjasama) }   </td>
                <td> ${formatLang (o.nilai_kepemimpinan) }  </td>
                <td> ${formatLang (o.nilai_perilaku) }  </td>
                <td> ${formatLang (o.nilai_skp_percent) } </td>
                <td> ${formatLang (o.nilai_skp_tambahan_percent) }  </td>
                <td> ${formatLang (o.nilai_perilaku_percent) }  </td>
                <td> ${formatLang (o.nilai_total) } </td>
                <td> <div class="text_center" > ${o.nilai_indeks} </div>   </td>
            </tr>
        	%endfor
    </table>

</body>
</html>
