from report import report_sxw
from report_webkit import webkit_report

from osv import fields, osv
from tools.translate import _
#import tools

class skp_employee_recap_monthly_report_parser(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context=None):

        super(skp_employee_recap_monthly_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'get_data' : self._get_data,
             'get_period' : self.get_period,
             'get_data_pegawai' : self._get_data_pegawai,

        })
    def _get_data(self,data):

        obj_data=self.pool.get(data['src_model']).browse(self.cr,self.uid,data['data_ids'])

        return obj_data
    def _get_data_pegawai(self,data):
        employee_obj=None
        obj_data=self.pool.get(data['src_model']).browse(self.cr,self.uid,data['data_ids'])
        if obj_data and obj_data[0]:
            employee_obj = obj_data[0].employee_id
        return employee_obj
    def get_period(self,month):
        bulan ='';
        if month:
            if month == '10':
                bulan = "Oktober"
            elif month == '11':
                bulan = "November"
            elif month == '12':
                bulan = "Desember"
            elif month == '01':
                bulan = "Januari"
            elif month == '02':
                bulan = "Februari"
            elif month == '03':
                bulan = "Maret"
            elif month == '04':
                bulan = "April"
            elif month == '05':
                bulan = "Mei"
            elif month == '06':
                bulan = "Juni"
            elif month == '07':
                bulan = "Juli"
            elif month == '08':
                bulan = "Agustus"
            elif month == '09':
                bulan = "September"
       
        return bulan
    
report_sxw.report_sxw('report.skp.employee.recap.monthly.report.form' ,
                        'skp.employee.recap.monthly.report',
                        'addons/df_skp_employee_recap_monthly_report/report/skp_employee_recap_monthly_report.mako', parser=skp_employee_recap_monthly_report_parser)
