import time
import netsvc

from tools.translate import _
import tools
from datetime import datetime, timedelta
from osv import fields, osv

from dateutil.relativedelta import relativedelta



class skp_employee_recap_monthly_report(osv.osv_memory):
    _name = "skp.employee.recap.monthly.report"

    #_columns = {
        #'name_pic' : fields.many2one('penandatangan.faktur', 'Penandatangan', required='True'),
    #}
    def download_laporan(self, cr, uid, ids, context={}):
        value = self.read(cr, uid, ids)[0]
        
        datas = {
             'ids': context.get('active_ids',[]),
             'data_ids': context.get('active_ids',[]),
             'model': 'skp.employee.recap.monthly.report',
             'src_model': 'skp.employee',
             'form': value
                 }
        return {
            'type': 'ir.actions.report.xml',
             'report_name': 'skp.employee.recap.monthly.report.form',
             'report_type': 'webkit',
             'datas': datas,
            }
skp_employee_recap_monthly_report