##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

{
    "name": "Laporan Lampiran Formulir SKP TAHUNAN",
    "version": "1.0",
    "author": "darmawan fatriananda",
    "category": "Penilaian Prestasi Kerja /Reporting / Laporan Summary Tahunan SKP",
    "description": """
   Hasil Lampiran REkapitulasi Tahunan Pegawai Versi BKN
   Konsep penilaian versi 2, dengan cara input ulang skp tahunan
    """,
    "website" : "http://www.mediasee.net",
    "license" : "GPL-3",
    "depends": [
                "df_skp_employee",
                "df_project_yearly",
                ],
    'update_xml': [
                   "skp_yearly_recap_summary_report_view.xml"
                   ],
    'installable': True,
    'active': False,

}