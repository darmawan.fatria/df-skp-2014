from openerp.report import report_sxw
import pooler
from datetime import datetime
from dateutil.relativedelta import relativedelta
import time
from operator import itemgetter
from itertools import groupby
from report_webkit import webkit_report
from osv import fields, osv
from tools.translate import _
import netsvc
import tools
import decimal_precision as dp
import math
import locale
class skp_yearly_recap_summary_pdf_report(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context=None):
        super(skp_yearly_recap_summary_pdf_report, self).__init__(cr, uid, name, context=context)
        locale.setlocale(locale.LC_ALL, 'id_ID.utf8')
        self.localcontext.update({
            'get_atribut_kepegawaian' : self.get_atribut_kepegawaian,
            'get_skp_yearly_report_raw' : self.get_skp_yearly_report_raw,
            'get_summary_yearly_report_raw' : self.get_summary_yearly_report_raw,
            
            'format_formula_skp':self.format_formula_skp,
            'format_integer' :self.format_integer,
            
        })
        
    def get_atribut_kepegawaian(self,filters,context=None):
        period_year=filters['form']['period_year']
        user_id = False
        data_pegawai=None
        if filters['form']['user_id']:
            user_id=filters['form']['user_id'][0]
        user_pool = self.pool.get('res.users')
        if user_id:
            result = user_pool.browse(self.cr, self.uid, user_id)
            if  result and result.partner_id:
                data_pegawai = result.partner_id
        return data_pegawai;
    
    def get_skp_yearly_report_raw(self,filters,context=None):
        period_year=filters['form']['period_year']
        user_id = False
        if filters['form']['user_id']:
            user_id=filters['form']['user_id'][0]
        target_pool = self.pool.get('project.project')
        target_ids=target_pool.search(self.cr, self.uid, [('state','in',('confirm','closed')),('target_period_year','=',period_year),('user_id','=',user_id)]
                                      ,order='target_type_id,name', context=None)
        results = target_pool.browse(self.cr, self.uid, target_ids)
        return results;
    def get_summary_yearly_report_raw(self,filters,context=None):
        period_year=filters['form']['period_year']
        user_id = False
        if filters['form']['user_id']:
            user_id=filters['form']['user_id'][0]
        skp_yearly_pool = self.pool.get('skp.employee.yearly.recapitulation')
        skp_yearly_ids=skp_yearly_pool.search(self.cr, self.uid, [('target_period_year','=',period_year),('user_id','=',user_id)], context=None)
        results = skp_yearly_pool.browse(self.cr, self.uid, skp_yearly_ids)
        if results:
            return results[0];
        return None
    def format_integer(self,val,context=None):
        try :
            return int(val)
        except:
            return None
    def format_formula_skp(self,recap_yearly):
        if not recap_yearly : return '-'
        try :
            str_jml =str(int( ( recap_yearly.jumlah_perhitungan_skp or 0) + ( recap_yearly.fn_nilai_tambahan or 0) + ( recap_yearly.fn_nilai_kreatifitas or 0) ))
            st_total_skp = str(recap_yearly.jml_skp)
            return "( %s : %s ) = "%(str_jml,st_total_skp)
        except:
            return '-'
        
       
report_sxw.report_sxw('report.skp.yearly.recap.summary.report.form',
                        'skp.yearly.recap.summary.report', 
                        'addons/df_skp_recap_yearly_pdf_report/report/skp_yearly_recap_summary_pdf_report.mako', parser=skp_yearly_recap_summary_pdf_report)
# report_sxw.report_sxw('report.penjualan.form', 'report.keuangan', 'addons/ad_laporan_keuangan/report/salesreport.mako', parser=ReportKeu)
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: