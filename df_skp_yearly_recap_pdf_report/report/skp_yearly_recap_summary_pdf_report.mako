<!DOCTYPE html SYSTEM "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<style>

body{
    font-family: verdana,arial,sans-serif;
	font-size:12px;
    word-wrap: break-word;
}

table.header_table {
	width: 100%;
    border-width: 0px;
	border-collapse: collapse;
    font-size:15px;
    
    
}
table.header_table td {
	padding: 5px;
    text-align: center;
}
table.sub_header_table {
    width: 100%;
	border-collapse: collapse;
    font-size:13px;
}
table.sub_header_table td {
	padding: 5px;
    text-align: left;
}
table.content_summary {
	width: 100%;
    border-collapse: collapse !important;    
    
}
table.content_summary td {
    border: 1px solid #000 !important;
    padding: 3px;
    vertical-align: top;
    text-align: left;
}
table.content_target {
	width: 100%;
    border-collapse: collapse !important;
    page-break-inside:auto; 
    
}
table.content_target thead {
	display:table-header-group;
	
    
}
table.content_target thead th{
	border: 1px solid #000 !important;
    padding: 3px;
    vertical-align: top;
    text-align: center;
}
    
table.content_target tr {
    page-break-inside:avoid; page-break-after:auto
}
table.content_target td {
    border: 1px solid #000 !important;
    padding: 3px;
    vertical-align: top;
    
}
.atribut_peg_no{
    width:6px;
    text-align=center;
}
.atribut_pegX{
     width:100px;
}
.atribut_peg_data{
     width:38%;
}
.nilai_summary_no{
    width:6px;
}    
.text_left{
	text-align: left;
}
.text_right{
	text-align: right;
}
.text_center{
	text-align: center;
}
.to_ttd_tgl{
    padding-top: 100px;
    padding-left:500px;
    padding-bottom:10px;
}
.ttd_right{
    padding-left: 500px;
    text-align: center;
    padding-bottom: 10px;
    
}
.ttd_left{
 padding-right: 600px;   
    text-align: center;
    padding-bottom: 10px;
    
}
.ttd_tgl{
     padding-top: 100px; 
     padding-left:30px;
     padding-bottom:10px;
}

.atribut_ak{
    width:18px;
    text-align:right;
}
.atribut_ko{
    width:100px;
    text-align:center;
}
.atribut_kw{
    width:15px;
    text-align:center;
}
.atribut_wkt{
    width:12px;
    text-align:center;
}
.atribut_biaya{
    width:150px;
    text-align:right;
}


p { 
    page-break-before: always;
  }
  h3, h4 {
    page-break-after: avoid;
  }
  pre, blockquote {
    page-break-inside: avoid;
  }

</style>
</head>
<body>
<%
		e = get_atribut_kepegawaian(data)
		recap = get_summary_yearly_report_raw(data)
		
		
%>
    <table class="header_table">
            <tr>
                <td>FORMULIR SASARAN KERJA</td>
            </tr>
            <tr>
                <td  > PEGAWAI NEGERI SIPIL</td>
            </tr>
    </table>
    <br /><br />
    
    
    <table class="content_summary" >
        <tr>
            <td  class="atribut_peg_no" >NO</td>
            <td  colspan="2" class="atribut_peg_data" > I. PEJABAT PENILAI </td>
            <td  class="atribut_peg_no" >NO</td>
            <td  colspan="2" class="atribut_peg_data" > II. PEGAWAI NEGERI SIPIL YANG DINILAI </td>
        </tr>
        <tr>
        	<td class="text_center"  >1</td>
            <td  class="atribut_peg">Nama</td>
            <td class="atribut_peg_data" >  ${e.user_id_atasan and e.user_id_atasan.fullname or ''} </td>
        	<td class="text_center"  >1</td>
            <td  class="atribut_peg">Nama</td>
            <td class="atribut_peg_data" >  ${e.fullname}</td>
        </tr>
        <tr>
        	<td  >2</td>
            <td  >N I P</td>
            <td class="atribut_peg_data" >  ${e.user_id_atasan and e.user_id_atasan.nip or ''}   </td>
        	<td  >2</td>
            <td  >N I P</td>
            <td class="atribut_peg_data" >  ${e.nip}</td>
        </tr>
        <tr>
        	<td  >3</td>
            <td  > Pangkat/Gol. Ruang</td>
            <td class="atribut_peg_data" >  ${e.user_id_atasan and e.user_id_atasan.golongan_id and e.user_id_atasan.golongan_id.name  or ''}  </td>
        	<td  >3</td>
            <td  > Pangkat/Gol. Ruang</td>
            <td class="atribut_peg_data" >  ${e.golongan_id and e.golongan_id.name or ''}</td>
        </tr>
        <tr>
        	<td  >4</td>
            <td  >Jabatan</td>
            <td class="atribut_peg_data" >  ${e.user_id_atasan and e.user_id_atasan.job_id and e.user_id_atasan.job_id.name  or ''} </td>
        	<td  >4</td>
            <td  >Jabatan</td>
            <td class="atribut_peg_data" >  ${e.job_id and e.job_id.name or ''}</td>
        </tr>
        <tr>
        	<td  >5</td>
            <td  >Unit Kerja</td>
            <td class="atribut_peg_data" >  ${e.user_id_atasan and e.user_id_atasan.department_id and e.user_id_atasan.department_id.name  or ''} </td>
        	<td  >5</td>
            <td  >Unit Kerja</td>
            <td class="atribut_peg_data" >  ${e.department_id and e.department_id.name or ''}</td>
        </tr>
    </table>
    
    <br/>
    <table class="content_target" >
    	<thead>
        <tr>
            <th rowspan="2" >NO</th>
            <th rowspan="2" > III. KEGIATAN TUGAS POKOK JABATAN </th>
            <th rowspan="2" >ANGKA KREDIT</th>
            <th colspan="4" > TARGET </th>
        </tr>
        <tr>
            <th  >KUANT/ OUTPUT</th>
            <th  >KUAL/ MUTU</th>
            <th  >WAKTU</th>
            <th  >BIAYA</th>
        </tr>
        </thead>
        <%
           targat_idx=1;
        %>
        %for target in get_skp_yearly_report_raw(data) :
        <tr>
        	<td  class="text_center" > ${targat_idx}</td>
            <td  > ${target.name}</td>
            <td  class="atribut_ak"> ${ target.target_angka_kredit and formatLang(target.target_angka_kredit) or '-'}</td>
            <td  class="atribut_ko"> ${format_integer(target.target_jumlah_kuantitas_output) or '-'} ${target.target_satuan_kuantitas_output and target.target_satuan_kuantitas_output.name or ''}</td>
            <td  class="atribut_kw"> ${format_integer(target.target_kualitas)  or '-'}</td>
            <td  class="atribut_wkt"> ${target.target_waktu or '-'}</td>
            <td  class="atribut_biaya"> ${ target.target_biaya and formatLang(target.target_biaya) or '-'}</td>
        </tr>
        <%
           targat_idx+=1;
        %>
        %endfor
	</table>
	%for a_break in range(1,data['form']['break_target']) :
	        <br />
	%endfor
	<br/>
	<table class="header_table">
	<tr>
			<td width="50%">	</td>
			<td width="50%"> ............... , ...........................</td>
	</tr>
	<tr>
			<td width="50%"> Pejabat Penilai	</td>
			<td width="50%"> Pegawai Yang Dinilai </td>
	</tr>
	<tr>
			<td width="50%" height="100px"> 	</td>
			<td width="50%"> </td>
	</tr>
	<tr>
			<td width="50%"> ( ${e.user_id_atasan and e.user_id_atasan.fullname or ''} )	</td>
			<td width="50%"> ( ${e.fullname or ''}  )</td>
	</tr>
	<tr>
			<td width="50%"> NIP. ${e.user_id_atasan and e.user_id_atasan.nip or ''}	</td>
			<td width="50%"> NIP. ${e.nip or ''} </td>
	</tr>
	</table>
	
    <p></p>
	<table class="header_table">
            <tr>
                <td>PENILAIAN SASARAN KERJA</td>
            </tr>
            <tr>
                <td  > PEGAWAI NEGERI SIPIL</td>
            </tr>
    </table>
    <br /><br />
	
	Jangka waktu penilaian 1 Januari s/d 31 Desember ${data and data['form']['period_year'] or ''}
	<table class="content_target" >
    	<thead>
        <tr>
            <th rowspan="2" >NO</th>
            <th rowspan="2" > I. Kegiatan Tugas Pokok Jabatan </th>
            <th rowspan="2" > AK </th>
            <th colspan="4" > TARGET </th>
            <th rowspan="2" > AK </th>
            <th colspan="4" > REALISASI </th>
            <th rowspan="2" > PERHITUNGAN </th>
            <th rowspan="2" > NILAI CAPAIAN SKP </th>
        </tr>
        <tr>
            <th  >KUANT/ OUTPUT</th>
            <th  >KUAL/ MUTU</th>
            <th  >WAKTU</th>
            <th  >BIAYA</th>
            <th  >KUANT/ OUTPUT</th>
            <th  >KUAL/ MUTU</th>
            <th  >WAKTU</th>
            <th  >BIAYA</th>
        </tr>
        <tr>
        	<th>1</th>
        	<th>2</th>
        	<th>3</th>
        	<th>4</th>
        	<th>5</th>
        	<th>6</th>
        	<th>7</th>
        	<th>8</th>
        	<th>9</th>
        	<th>10</th>
        	<th>11</th>
        	<th>12</th>
        	<th>13</th>
        	<th>14</th>
        </tr>
        </thead>
        <%
           targat_idx=1;
        %>
        %for target in get_skp_yearly_report_raw(data) :
        <tr>
        	<td  class="text_center" > ${targat_idx}</td>
            <td  > ${target.name}</td>
            <td  class="atribut_ak"> ${ target.target_angka_kredit and formatLang(target.target_angka_kredit) or '-'}</td>
            <td  class="atribut_ko"> ${format_integer(target.target_jumlah_kuantitas_output) or '-'} ${target.target_satuan_kuantitas_output and target.target_satuan_kuantitas_output.name or ''}</td>
            <td  class="atribut_kw"> ${format_integer(target.target_kualitas)  or '-'}</td>
            <td  class="atribut_wkt"> ${target.target_waktu or '-'}</td>
            <td  class="atribut_biaya"> ${ target.target_biaya and formatLang(target.target_biaya) or '-'}</td>
            <td  class="atribut_ak"> ${ target.realisasi_angka_kredit and formatLang(target.realisasi_angka_kredit) or '-'}</td>
            <td  class="atribut_ko"> ${format_integer(target.realisasi_jumlah_kuantitas_output) or '-'} ${target.realisasi_satuan_kuantitas_output and target.realisasi_satuan_kuantitas_output.name or ''}</td>
            <td  class="atribut_kw"> ${format_integer(target.realisasi_kualitas)  or '-'}</td>
            <td  class="atribut_wkt"> ${target.realisasi_waktu or '-'}</td>
            <td  class="atribut_biaya"> ${ target.realisasi_biaya and formatLang(target.realisasi_biaya) or '-'}</td>
            <td  class="text_right"> ${ target.jumlah_perhitungan and formatLang(target.jumlah_perhitungan) or '-'}</td>
            <td  class="text_right"> ${ target.nilai_akhir and formatLang(target.nilai_akhir) or '-'}</td>
        </tr>
	       <%
	           targat_idx+=1;
	        %>
        %endfor   
        </table>     
        %for a_break in range(1,data['form']['break_realisasi']) :
                        <br />
        %endfor
        <table  class="content_target">
		<tr>
			
			<td colspan="14"><b>II. Tugas Tambahan dan Kreativitas/ Unsur Penunjang :</b></td>
		</tr>
		<tr>
			<td ></td>
			<td colspan="12">a. Tugas Tambahan:</td>
			<td class="text_right"> ${recap and format_integer(recap.fn_nilai_tambahan) or '-'}</td>
		</tr>
		<tr>
			<td></td>
			<td colspan="12">b. Kreatifitas:</td>
			<td class="text_right">${recap and format_integer(recap.fn_nilai_kreatifitas) or '-'}</td>
		</tr>
		<tr>
			<td></td>
			<td colspan="12">JUMLAH:</td>
			<td class="text_right"> ${recap and formatLang(recap.jumlah_perhitungan_skp) or '-'} </td>
		</tr>
		<tr>
			<td rowspan="2" colspan="13" class="text_center">NILAI CAPAIAN SKP</td>
			<!-- <td rowspan="2" colspan="3" class="text_right">  </td>-->
			<td class="text_right" >  ${recap and formatLang(recap.nilai_skp_tambahan) or '-'}  </td>
		</tr>
		<tr>
			
			<td class="text_center"> ${recap and recap.indeks_nilai_skp or '-'} </td>
		</tr>
		</table>
		<br/>
		<table class="header_table">
	<tr>
			<td width="50%">	</td>
			<td width="50%"> ............... , ...........................</td>
	</tr>
	<tr>
			<td></td>
			<td width="50%"> Pejabat Penilai	</td>
			
	</tr>
	<tr>
			<td></td>
			<td width="50%" height="100px"> 	</td>
			
	</tr>
	<tr>
			<td></td>
			<td width="50%"> ( ${e.user_id_atasan and e.user_id_atasan.fullname or ''} )	</td>
			
	</tr>
	<tr>
			<td></td>
			<td width="50%"> NIP. ${e.user_id_atasan and e.user_id_atasan.nip or ''}	</td>
			
	</tr>
	</table>
		
</body>
</html>
