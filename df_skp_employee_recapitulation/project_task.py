# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from osv import fields, osv
from datetime import datetime,timedelta
import time
from mx import DateTime


class project_task(osv.osv):
    _inherit = 'project.task'
  
    def do_skp_summary_calculation(self, cr,uid, user_id,employee_id, target_period_year,  context=None):
        sey_pool = self.pool.get('skp.employee.yearly.recapitulation')
        data_skp_summary  = sey_pool._get_akumulasi_nilai_all_tahunan(cr, uid,user_id,employee_id, target_period_year, context=context)
        skp_yearly_ids = sey_pool.search(cr, uid, [('employee_id', '=', employee_id),
                                                     ('target_period_year', '=', target_period_year),
                                                   ], context=None)
        if not skp_yearly_ids:
                        values = {
                            'employee_id': employee_id,
                            'user_id': user_id,
                            'target_period_year': target_period_year,
                        }
                        new_skp_yearly_id=sey_pool.create(cr , uid, values, context=None)
                        skp_yearly_ids.append(new_skp_yearly_id)
                        
        if data_skp_summary and skp_yearly_ids :
            for skp_yearly_id in  skp_yearly_ids:
                        count_of_month = data_skp_summary['count_of_month']
                        jml_skp = data_skp_summary['jml_skp']
                        if count_of_month > 0 or jml_skp > 0:
                            update_values = {
                                'jml_skp': data_skp_summary['jml_skp'],
                                'jml_skp_done': data_skp_summary['jml_skp_done'],
                                'jml_realisasi_skp':data_skp_summary['jml_realisasi_skp'],
                                'nilai_skp': data_skp_summary['nilai_skp']   ,
                                'nilai_skp_tambahan': data_skp_summary['nilai_skp_tambahan']   ,
                                'nilai_skp_percent': data_skp_summary['nilai_skp_percent'],
                                'nilai_skp_tambahan_percent': data_skp_summary['nilai_skp_tambahan_percent'],
                                'jml_perilaku': data_skp_summary['jml_perilaku'],
                                'nilai_perilaku': data_skp_summary['nilai_perilaku'],
                                'nilai_perilaku_percent': data_skp_summary['nilai_perilaku_percent'],
                                'nilai_pelayanan': data_skp_summary['nilai_pelayanan'],
                                'nilai_integritas': data_skp_summary['nilai_integritas'],
                                'nilai_komitmen': data_skp_summary['nilai_komitmen'],
                                'nilai_disiplin': data_skp_summary['nilai_disiplin'],
                                'nilai_kerjasama': data_skp_summary['nilai_kerjasama'],
                                'nilai_kepemimpinan': data_skp_summary['nilai_kepemimpinan'],
                                'fn_nilai_tambahan': data_skp_summary['fn_nilai_tambahan'],
                                'fn_nilai_kreatifitas': data_skp_summary['fn_nilai_kreatifitas'],
                                'nilai_total': data_skp_summary['nilai_total'],
                                'indeks_nilai_pelayanan': data_skp_summary['indeks_nilai_pelayanan'],
                                'indeks_nilai_integritas': data_skp_summary['indeks_nilai_integritas'],
                                'indeks_nilai_komitmen': data_skp_summary['indeks_nilai_komitmen'],
                                'indeks_nilai_disiplin': data_skp_summary['indeks_nilai_disiplin'],
                                'indeks_nilai_kerjasama': data_skp_summary['indeks_nilai_kerjasama'],
                                'indeks_nilai_kepemimpinan': data_skp_summary['indeks_nilai_kepemimpinan'],
                                'indeks_nilai_total': data_skp_summary['indeks_nilai_total'],
                                'indeks_nilai_skp': data_skp_summary['indeks_nilai_skp'],
                                'jumlah_perhitungan_skp': data_skp_summary['jumlah_perhitungan_skp'],
                                
                            }
                            sey_pool.write(cr , uid,[skp_yearly_id,], update_values, context=None)
        return True;
project_task()
