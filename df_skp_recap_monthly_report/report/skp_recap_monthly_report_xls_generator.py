import time
import xlwt
import cStringIO
from xlwt import Workbook, Formula
from report_engine_xls import report_xls
import skp_recap_monthly_report_xls_generator
import skp_recap_monthly_report_parser
from skp_recap_monthly_report_parser import skp_recap_monthly_report_parser



class skp_recap_monthly_report_xls_generator(report_xls):
	
    def generate_xls_report(self, parser, filters, obj, workbook):
	worksheet = workbook.add_sheet(('Laporan SKP Bulanan'))
	worksheet_2 = workbook.add_sheet(('Laporan Perilaku Bulanan')) 
	
        worksheet.panes_frozen = True
        worksheet.remove_splits = True
        worksheet.portrait = False # Landscape
        worksheet.fit_wiresult_datah_to_pages = 1
        worksheet.col(1).wiresult_datah = len("ABCDEFG")*1024
    	
    	worksheet_2.panes_frozen = True
        worksheet_2.remove_splits = True
        worksheet_2.portrait = False # Landscape
        worksheet_2.fit_wiresult_datah_to_pages = 1
        worksheet_2.col(1).wiresult_datah = len("ABCDEFG")*1024
        
        int_number_style=  xlwt.easyxf("borders: top thin, bottom thin, left thin, right thin;",num_format_str='#,##0;(#,##0)')
        row_normal_style_align_right=  xlwt.easyxf("borders: top thin, bottom thin, left thin, right thin;align: wrap 1, vert centre,horiz right;",num_format_str='#,##0.00;(#,##0.00)')
        big_point_with_border_style = xlwt.easyxf('font: height 220, name Arial, colour_index black, bold on, italic off; align: wrap on, vert centre, horiz center;pattern: pattern solid, fore_color white;borders: top thin, bottom thin, left thin, right thin;', num_format_str='#,##0.00;(#,##0.00)')
		# Specifying columns, the order doesn't matter
		# lamda d,f,p: is a function who has filter,data,parser as the parameters it is expected to the value of the column
        cols_specs = [
	    # ('header', column_span, column_type, lamda function)
	    
	    # Infos
	    
	    ('Title_1', 18, 200, 'text', lambda x, d, p: 'PENILAIAN SASARAN KERJA'),
	    ('Title_2',  18, 200, 'text', lambda x, d, p: 'PEGAWAI NEGERI SIPIL'),
	    ('Title_3', 18, 200, 'text', lambda x, d, p: 'PENILAIAN PERILAKU, TUGAS TAMBAHAN DAN KREATIFITAS'),
	    
	    ('fix_period', 3, 200, 'text', lambda x, d, p:  d['period_month']),
	    #Atribut Kepegawaian
	    ('no_', 1, 30, 'text', lambda x, d, p:  'NO'),
	    ('no_1', 1, 10, 'number', lambda x, d, p: 1,xlwt.Row.set_cell_number,int_number_style),
	    ('no_2', 1, 10, 'number', lambda x, d, p: 2,xlwt.Row.set_cell_number,int_number_style),
	    ('no_3', 1, 10, 'number', lambda x, d, p: 3,xlwt.Row.set_cell_number,int_number_style),
	    ('no_4', 1, 10, 'number', lambda x, d, p: 4,xlwt.Row.set_cell_number,int_number_style),
	    ('no_5', 1, 10, 'number', lambda x, d, p: 5,xlwt.Row.set_cell_number,int_number_style),
	    ('no_6', 1, 10, 'number', lambda x, d, p: 6,xlwt.Row.set_cell_number,int_number_style),
	    ('PEJABAT PENILAI', 7, 400, 'text', lambda x, d, p:  'I. PEJABAT PENILAI'),
	    ('PEGAWAI NEGERI SIPIL YANG DINILAI', 7, 400, 'text', lambda x, d, p:  'II. PEGAWAI NEGERI SIPIL YANG DINILAI'),
	    ('nama', 2, 80, 'text', lambda x, d, p:  'Nama'),
	    ('nip', 2, 80, 'text', lambda x, d, p:  'NIP'),
	    ('pangkat_gol', 2, 80, 'text', lambda x, d, p:  'Pangkat/Gol.Ruang'),
	    ('jabatan', 2, 80, 'text', lambda x, d, p:  'Jabatan'),
	    ('unit_kerja', 2, 80, 'text', lambda x, d, p:  'Unit Kerja'),
	    
	    
	    ('nama_atasan', 5, 200, 'text', lambda x, d, p:  d and d.user_id_atasan and d.user_id_atasan.name or '' ),
	    ('nama_pegawai', 5, 200, 'text', lambda x, d, p:  d and d.name or '' ),
	    ('nip_atasan', 5, 200, 'text', lambda x, d, p:  d and d.user_id_atasan and d.user_id_atasan.nip or '' ),
	    ('nip_pegawai', 5, 200, 'text', lambda x, d, p:  d and d.nip or '' ),
	    ('golongan_atasan', 5, 200, 'text', lambda x, d, p:  d and d.user_id_atasan and d.user_id_atasan.golongan_id and d.user_id_atasan.golongan_id.name or '' ),
	    ('golongan_pegawai', 5, 200, 'text', lambda x, d, p:  d and d.golongan_id and d.golongan_id.name or '' ),
	    ('jabatan_atasan', 5, 200, 'text', lambda x, d, p:  d and d.user_id_atasan and d.user_id_atasan.job_id and d.user_id_atasan.job_id.name or '' ),
	    ('jabatan_pegawai', 5, 200, 'text', lambda x, d, p:  d and d.job_id and d.job_id.name or '' ),
	    ('unit_kerja_atasan', 5, 200, 'text', lambda x, d, p:  d and d.user_id_atasan and d.user_id_atasan.department_id and d.user_id_atasan.department_id.name or '' ),
	    ('unit_kerja_pegawai', 5, 200, 'text', lambda x, d, p:  d and d.department_id and d.department_id.name or '' ),
	    ('company_pegawai', 5, 200, 'text', lambda x, d, p:  d and d.company_id and d.company_id.name or '' ),
	    ('nama_banding', 5, 200, 'text', lambda x, d, p:  d and d.user_id_banding and d.user_id_banding.name or '' ),
	    ('nip_banding', 5, 200, 'text', lambda x, d, p:  d and d.user_id_banding and d.user_id_banding.nip or '' ),
	    ('golongan_banding', 5, 200, 'text', lambda x, d, p:  d and d.user_id_banding and d.user_id_banding.golongan_id and d.user_id_banding.golongan_id.name or '' ),
	    ('jabatan_banding', 5, 200, 'text', lambda x, d, p:  d and d.user_id_banding and d.user_id_banding.job_id and d.user_id_banding.job_id.name or '' ),
	    ('unit_kerja_banding', 5, 200, 'text', lambda x, d, p:  d and d.user_id_banding and d.user_id_banding.department_id and d.user_id_banding.department_id.name or '' ),
	    
	    
	    # Header Atribut Kegiatan
	    ('NO', 1, 30, 'text', lambda x, d, p:  'NO'),
	    ('III.KEGIATAN TUGAS POKOK JABATAN', 3, 400, 'text', lambda x, d, p:  'III.KEGIATAN TUGAS POKOK JABATAN'),
	    ('I.KEGIATAN TUGAS POKOK JABATAN', 3, 400, 'text', lambda x, d, p:  'III.KEGIATAN TUGAS POKOK JABATAN'),
	    ('II.TUGAS TAMBAHAN DAN KREATIFITAS / UNSUR PENUNJANG', 3, 400, 'text', lambda x, d, p:  'II.TUGAS TAMBAHAN DAN KREATIFITAS / UNSUR PENUNJANG'),
	    ('a.Tugas Tambahan', 3, 200, 'text', lambda x, d, p:  'a.Tugas Tambahan'),
	    ('b.Kreatifitas', 3, 200, 'text', lambda x, d, p:  'b.Kreatifitas'),
	    ('TARGET', 6, 100, 'text', lambda x, d, p:  'TARGET'),
	    ('REALISASI', 6, 100, 'text', lambda x, d, p:  'REALISASI'),
	    ('ANGKA KREDIT', 1, 40, 'text', lambda x, d, p:  'AK'),
	    ('KUANT / OUTPUT', 2, 100, 'text', lambda x, d, p:  'KUANT / OUTPUT'),
	    ('KUAL / MUTU', 1, 100, 'text', lambda x, d, p:  'KUAL / MUTU'),
	    ('WAKTU', 1, 100, 'text', lambda x, d, p:  'WAKTU'),
	    ('BIAYA', 1, 100, 'text', lambda x, d, p:  'BIAYA'),
	    ('PERHITUNGAN', 1, 100, 'text', lambda x, d, p:  'PERHITUNGAN'),
	    ('NILAI CAPAIAN SKP', 1, 100, 'text', lambda x, d, p:  'NILAI CAPAIAN SKP'),
	    ('TOTAL NILAI CAPAIAN SKP', 17, 200, 'text', lambda x, d, p:  'TOTAL NILAI CAPAIAN SKP'),
	    ('1', 1, 15, 'text', lambda x, d, p:  'NO'),
	    ('2', 3, 70, 'text', lambda x, d, p:  'III.KEGIATAN TUGAS POKOK JABATAN'),
	    ('3', 1, 25, 'text', lambda x, d, p:  'ANGKA KREDIT'),
	    ('4', 2, 30, 'text', lambda x, d, p:  'KUANT / OUTPUT'),
	    ('5', 1, 25, 'text', lambda x, d, p:  'KUAL / MUTU'),
	    ('6', 1, 25, 'text', lambda x, d, p:  'WAKTU'),
	    ('7', 1, 70, 'text', lambda x, d, p:  'BIAYA'),
	    ('8', 1, 25, 'text', lambda x, d, p:  'ANGKA KREDIT'),
	    ('9', 2, 30, 'text', lambda x, d, p:  'KUANT / OUTPUT'),
	    ('10', 1, 25, 'text', lambda x, d, p:  'KUAL / MUTU'),
	    ('11', 1, 25, 'text', lambda x, d, p:  'WAKTU'),
	    ('12', 1, 70, 'text', lambda x, d, p:  'BIAYA'),
	    ('13', 1, 30, 'text', lambda x, d, p:  'PERHITUNGAN'),
	    ('14', 1, 30, 'text', lambda x, d, p:  'NILAI CAPAIAN SKP'),
	    #Data Atribut Kegiatan
	    ('seq', 1, 20, 'number', lambda x, d, p: 0,xlwt.Row.set_cell_number,int_number_style),
	    ('nama_kegiatan', 3, 200, 'text', lambda x, d, p:  d.name),
	    ('target_ak', 1, 100, 'number', lambda x, d, p:  d.target_angka_kredit),
	    ('target_kuantitas', 1, 100, 'number', lambda x, d, p: d.target_jumlah_kuantitas_output,xlwt.Row.set_cell_number,int_number_style),
	    ('target_satuan_kuantitas', 1, 100, 'text', lambda x, d, p:  d.target_satuan_kuantitas_output and d.target_satuan_kuantitas_output.name or ''),
	    ('target_kualitas', 1, 100, 'number', lambda x, d, p:  d.target_kualitas,xlwt.Row.set_cell_number,int_number_style),
	    ('target_waktu', 1, 100, 'number', lambda x, d, p:  d.target_waktu,xlwt.Row.set_cell_number,int_number_style),
	    ('target_biaya', 1, 200, 'number', lambda x, d, p:  d.target_biaya),
	    ('realisasi_ak', 1, 100, 'number', lambda x, d, p:  d.realisasi_angka_kredit),
	    ('realisasi_kuantitas', 1, 100, 'number', lambda x, d, p:  d.realisasi_jumlah_kuantitas_output,xlwt.Row.set_cell_number,int_number_style),
	    ('realisasi_satuan_kuantitas', 1, 100, 'text', lambda x, d, p:  d.realisasi_satuan_kuantitas_output and d.realisasi_satuan_kuantitas_output.name or ''),
	    ('realisasi_kualitas', 1, 100, 'number', lambda x, d, p:  d.realisasi_kualitas,xlwt.Row.set_cell_number,int_number_style),
	    ('realisasi_waktu', 1, 100, 'number', lambda x, d, p:  d.realisasi_waktu,xlwt.Row.set_cell_number,int_number_style),
	    ('realisasi_biaya', 1, 200, 'number', lambda x, d, p:  d.realisasi_biaya),
	    ('total_nilai_target_skp', 1, 200, 'number', lambda x, d, p:  d.jumlah_perhitungan),
	    ('total_capaian_target_skp', 1, 200, 'number', lambda x, d, p:  d.nilai_akhir),
	    ('nilai_tugas_tambahan', 1, 200, 'number', lambda x, d, p:  0),
	    ('nilai_kreatifitas', 1, 200, 'number', lambda x, d, p:  0),
	    ('sum_nilai_capaian_skp', 1, 100, 'number', lambda x, d, p: 0),
	    
	    #Footer
	    ('footer_tanggal', 5, 100, 'text', lambda x, d, p:  p.get_concat_with_format_date('Bandung, ',filters)),
	    ('footer_pejabat_penilai', 5, 300, 'text', lambda x, d, p:  'Pejabat Penilai'),
	    ('footer_nama_atasan', 5, 200, 'text', lambda x, d, p:  d and d.user_id_atasan and d.user_id_atasan.name or '' ),
	    ('footer_nip_atasan', 5, 200, 'text', lambda x, d, p:  d and d.user_id_atasan and d.user_id_atasan.nip or '' ),
	    # Misc
	    ('single_empty_column', 1, 0, 'text', lambda x, d, p: ''),
	    ('double_empty_column', 2, 0, 'text', lambda x, d, p: ''),
	    ('triple_empty_column', 3, 0, 'text', lambda x, d, p: ''),
	    ('quadruple_empty_column', 4, 0, 'text', lambda x, d, p: ''),
	    ('12_empty_column', 12, 0, 'text', lambda x, d, p: ''),
	    
	    
		]
        
        cols_specs_2 = [
				#content perilaku
	    ('1', 1, 15, 'text', lambda x, d, p:  'NO'),
	    ('2', 2, 70, 'text', lambda x, d, p:  'Periode'),
	    ('3', 2, 25, 'text', lambda x, d, p:  'Orientasi Pelayanan'),
	    ('4', 2, 25, 'text', lambda x, d, p:  'Integritas'),
	    ('5', 2, 25, 'text', lambda x, d, p:  'Disiplin'),
	    ('6', 2, 25, 'text', lambda x, d, p:  'Komitmen'),
	    ('7', 2, 25, 'text', lambda x, d, p:  'Kerjasama'),
	    ('8', 2, 25, 'text', lambda x, d, p:  'Kepemimpinan'),
	    ('9', 2, 25, 'text', lambda x, d, p:  'Tugas Tambahan'),
	    ('10', 2, 25, 'text', lambda x, d, p:  'Kreatifitas'),
	    ('11', 2, 25, 'text', lambda x, d, p:  'Nilai Perilaku'),
	    ('12', 2, 25, 'text', lambda x, d, p:  'Nilai Perilaku Percent'),
	    ('NO', 1, 20, 'number', lambda x, d, p: 0,xlwt.Row.set_cell_number,int_number_style),
	    ('Periode Bulan', 2, 200, 'number', lambda x, d, p:  parser.get_month_to_char(d.target_period_month)),
	    ('Pelayanan', 2, 200, 'number', lambda x, d, p:  d.nilai_pelayanan),
	    ('Integritas', 2, 200, 'number', lambda x, d, p:  d.nilai_integritas),
	    ('Komitmen', 2, 200, 'number', lambda x, d, p:  d.nilai_komitmen),
	    ('Disiplin', 2, 200, 'number', lambda x, d, p:  d.nilai_disiplin),
	    ('Kerjasama', 2, 200, 'number', lambda x, d, p:  d.nilai_kerjasama),
	    ('Kepemimpinan', 2, 200, 'number', lambda x, d, p:  d.nilai_kepemimpinan),	
	    ('Tugas Tambahan', 2, 200, 'number', lambda x, d, p:  d.fn_nilai_tambahan),
	    ('Kreatifitas', 2, 200, 'number', lambda x, d, p:  d.fn_nilai_kreatifitas),
	    ('Nilai Perilaku', 2, 200, 'number', lambda x, d, p:  d.nilai_perilaku),
	    ('Perilaku (%)', 2, 200, 'number', lambda x, d, p:  d.nilai_perilaku_percent),
		]
        row_spec_value = ['seq','nama_kegiatan','target_ak','target_kuantitas','target_satuan_kuantitas','target_kualitas','target_waktu','target_biaya',
						  'realisasi_ak','realisasi_kuantitas','realisasi_satuan_kuantitas','realisasi_kualitas','realisasi_waktu','realisasi_biaya',
						  'total_nilai_target_skp','total_capaian_target_skp']
        target_spec_value = ['seq','nama_kegiatan','target_ak','target_kuantitas','target_satuan_kuantitas','target_kualitas','target_waktu','target_biaya']
        row_spec_perilaku_value = ['NO','Periode Bulan','Pelayanan','Integritas','Komitmen','Disiplin','Kerjasama','Kepemimpinan',
						  'Tugas Tambahan','Kreatifitas','Nilai Perilaku']				
        
        # Row templates (Order Matters, this joins the columns that are specified in the second parameter)
        
        title_1_template = self.xls_row_template(cols_specs, ['Title_1'])
        title_2_template = self.xls_row_template(cols_specs, ['Title_2',])
        jangka_waktu_template = self.xls_row_template(cols_specs, ['single_empty_column','fix_period',])
        #KEPEGAWAIAN
        header_pegawai_template = self.xls_row_template(cols_specs, ['NO','PEJABAT PENILAI','NO','PEGAWAI NEGERI SIPIL YANG DINILAI'])
        header_nama_pegawai_template = self.xls_row_template(cols_specs, ['no_1','nama','nama_atasan','no_1','nama','nama_pegawai'])
        header_nip_pegawai_template = self.xls_row_template(cols_specs, ['no_2','nip','nip_atasan','no_2','nip','nip_pegawai'])
        header_golongan_pegawai_template = self.xls_row_template(cols_specs, ['no_3','pangkat_gol','golongan_atasan','no_3','pangkat_gol','golongan_pegawai'])
        header_jabatan_pegawai_template = self.xls_row_template(cols_specs, ['no_4','jabatan','jabatan_atasan','no_4','jabatan','jabatan_pegawai'])
        header_unit_kerja_pegawai_template = self.xls_row_template(cols_specs, ['no_5','unit_kerja','unit_kerja_atasan','no_5','unit_kerja','unit_kerja_pegawai'])
        #KTARGET REALISASI
        target_header_info_top_template = self.xls_row_template(cols_specs, ['quadruple_empty_column','TARGET',])
        target_header_info_template = self.xls_row_template(cols_specs,['NO','III.KEGIATAN TUGAS POKOK JABATAN','ANGKA KREDIT','KUANT / OUTPUT','KUAL / MUTU','WAKTU','BIAYA'])
        target_realisasi_header_info_top_template = self.xls_row_template(cols_specs, ['quadruple_empty_column','TARGET','REALISASI','double_empty_column'])
        target_realisasi_header_info_template = self.xls_row_template(cols_specs,['NO','I.KEGIATAN TUGAS POKOK JABATAN','ANGKA KREDIT','KUANT / OUTPUT','KUAL / MUTU','WAKTU','BIAYA','ANGKA KREDIT','KUANT / OUTPUT','KUAL / MUTU','WAKTU','BIAYA','PERHITUNGAN','NILAI CAPAIAN SKP'])
        sub_target_realisasi_header_info_template = self.xls_row_template(cols_specs, ['1','2','3','4','5','6','7','8','9','10','11','12','13','14'])
        tambahan_kreatifitas_header_info_template = self.xls_row_template(cols_specs,['single_empty_column','II.TUGAS TAMBAHAN DAN KREATIFITAS / UNSUR PENUNJANG','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column'])
        tugas_tambahan_template = self.xls_row_template(cols_specs,['single_empty_column','a.Tugas Tambahan','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','nilai_tugas_tambahan'])
        kreatifitas_template = self.xls_row_template(cols_specs,['single_empty_column','b.Kreatifitas','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','single_empty_column','nilai_kreatifitas'])
        nilai_capaian_skp_template = self.xls_row_template(cols_specs,['TOTAL NILAI CAPAIAN SKP','sum_nilai_capaian_skp'])
        row_template = self.xls_row_template(cols_specs,row_spec_value)
        target_template = self.xls_row_template(cols_specs,target_spec_value)
        # Footer
        footer_tanggal_template = self.xls_row_template(cols_specs, ['12_empty_column','footer_tanggal',])
        footer_pejabat_penilai_template = self.xls_row_template(cols_specs, ['12_empty_column','footer_pejabat_penilai',])
        footer_nama_atasan_template = self.xls_row_template(cols_specs, ['12_empty_column','footer_nama_atasan',])
        footer_nip_atasan_template = self.xls_row_template(cols_specs, ['12_empty_column','footer_nip_atasan',])
        #sums_template = self.xls_row_template(cols_specs, ['triple_empty_column','saldo_awal_sum','penjualan_sum','penjualan_jm_sum','pelunasan_sum','pelunasan_jm_sum','saldo_akhir_sum'])
        empty_row_template = self.xls_row_template(cols_specs, ['single_empty_column'])
        
		
        # Styles (It's used for writing rows / headers)
        invisible_style=  xlwt.easyxf("align: wrap 1, vert centre;",num_format_str='#,##0.00;(#,##0.00)')
        row_normal_style=  xlwt.easyxf("borders: top thin, bottom thin, left thin, right thin;align: wrap 1, vert centre;",num_format_str='#,##0.00;(#,##0.00)')
        row_normal_without_border_style=  xlwt.easyxf("align: wrap 1, vert centre;",num_format_str='#,##0.00;(#,##0.00)')
        row_normal_without_border_center_style=  xlwt.easyxf("align: wrap 1, vert centre,horiz centre;",num_format_str='#,##0.00;(#,##0.00)')
        header_normal_style=  xlwt.easyxf('align: wrap on,  vert centre, horiz left;borders: top thin, bottom thin, left thin, right thin;',num_format_str='#,##0.00;(#,##0.00)')
        bold_normal_style=  xlwt.easyxf('font: bold on;',num_format_str='#,##0;(#,##0)')
        bold_normal_with_border_style=  xlwt.easyxf('font: bold on;borders: top thin, bottom thin, left thin, right thin;align: wrap 1, vert centre;',num_format_str='#,##0;(#,##0)')
        title_with_border_style = xlwt.easyxf('font: height 220, name Arial,  bold on, italic off; align: wrap on, vert centre,horiz centre;pattern: pattern solid, fore_color white;borders: top thin, bottom thin, left thin, right thin;', num_format_str='#,##0.00;(#,##0.00)')
        title_style = xlwt.easyxf('font: height 220, name Arial,  bold on, italic off; align: wrap on, vert centre,horiz centre;pattern: pattern solid, fore_color white;', num_format_str='#,##0.00;(#,##0.00)')
        header_with_border_style = xlwt.easyxf('font: height 200, name Arial, colour_index black, bold on, italic off; align: wrap on, vert centre, horiz center;pattern: pattern solid, fore_color White;borders: top thin, bottom thin, left thin, right thin;', num_format_str='#,##0.00;(#,##0.00)')
        header_style = xlwt.easyxf('font: height 200, name Arial, colour_index black, bold on, italic off; align: wrap 1, vert centre, horiz center;pattern: pattern solid, fore_color White;', num_format_str='#,##0.00;(#,##0.00)')
        sub_header_style = xlwt.easyxf('font: height 200, name Arial, colour_index black, bold on, italic off; align: wrap on, vert centre, horiz center;pattern: pattern solid, fore_color White;borders: top thin, bottom thin, left thin, right thin;', num_format_str='#,##0.00;(#,##0.00)')
        sum_style = xlwt.easyxf('font: height 200, name Arial, colour_index black, bold on, italic off; align: wrap on, vert centre;pattern: pattern solid, fore_color gray50;', num_format_str='#,##0.00;(#,##0.00)')

 
 #Get Data
        #data_rekap_tahunan_pegawai = parser.get_recap_monthly_report_raw(filters);
        data_pegawai = parser.get_atribut_kepegawaian(filters);
               
 #SKP ===========================================================================================================
        row_count = 0
        self.xls_write_row(worksheet, filters, None, parser, row_count, title_1_template, title_style)
        row_count+=1
        self.xls_write_row(worksheet, filters, None, parser, row_count, title_2_template, title_style)
        
        row_count+=1
        #self.xls_write_row(worksheet, filters, data_rekap_tahunan_pegawai, parser, row_count, jangka_waktu_template, bold_normal_style)
#START HEADER PEGAWAI--------------------------------------------------        
 # Write DATA KEPEGAWAIAN
        row_count+=1
        self.xls_write_row(worksheet, filters, None, parser, row_count, header_pegawai_template, header_with_border_style)
        #row_count+=1
        #self.xls_write_row_header(worksheet, row_count, header_pegawai_template,header_style, set_column_size=False)
        row_count+=1
        self.xls_write_row(worksheet, filters, data_pegawai, parser, row_count, header_nama_pegawai_template, header_normal_style)
        row_count+=1
        self.xls_write_row(worksheet, filters, data_pegawai, parser, row_count, header_nip_pegawai_template, header_normal_style)
        row_count+=1
        self.xls_write_row(worksheet, filters, data_pegawai, parser, row_count, header_golongan_pegawai_template, header_normal_style)
        row_count+=1
        self.xls_write_row(worksheet, filters, data_pegawai, parser, row_count, header_jabatan_pegawai_template, header_normal_style)
        row_count+=1
        self.xls_write_row(worksheet, filters, data_pegawai, parser, row_count, header_unit_kerja_pegawai_template, header_normal_style)
        period_skp={}
#END HEADER PEGAWAI--------------------------------------------------
#START SKP DAN TAMBAHAN--------------------------------------------------
#HEADER TARGET REALISASI
		
        for int_month in range(1,13) :
         	#logic loop month
	        month_filter = format(int_month, '02')
	        result = parser.get_skp_recap_monthly_report_raw(filters,month_filter);
	        if not result :
	        	continue;
	        period_skp['period_month']=parser.get_period_month_year(month_filter,filters)
	        row_count+=2
        	self.xls_write_row(worksheet, filters, period_skp, parser, row_count, jangka_waktu_template, bold_normal_style)
	    	row_count+=1
	        self.xls_write_row(worksheet,filters,None,parser, row_count, target_realisasi_header_info_top_template, header_with_border_style,)
	        row_count+=1
	        self.xls_write_row_header(worksheet, row_count, target_realisasi_header_info_template, header_with_border_style, set_column_size=False)
	        row_count+=1
	        self.xls_write_row_header(worksheet, row_count, sub_target_realisasi_header_info_template, sub_header_style, set_column_size=True)
	
	#DATA SKP             
			#REALISASI
	        row_count+=1
	        row_data=row_count
	        total_capaian_skp=jumlah_capaian_skp=0
	        idx=1	                
	        for employee_data_summary_data in result:
	            # Write Rows
	                #employee_data_summary_data.sequence=1
	                self.xls_write_row_with_indeks(worksheet, filters, employee_data_summary_data, parser, row_data, row_template, row_normal_style,idx)
	                row_data+=1
	                total_capaian_skp+=employee_data_summary_data.nilai_akhir
	                jumlah_capaian_skp+=1
	                idx+=1
                
			row_count=row_data
        
#START FOOTER--------------------------------------------------        
        #write footer
        #row_count+=3
        #self.xls_write_row(worksheet, filters, data_pegawai, parser, row_count, footer_tanggal_template, row_normal_without_border_center_style)
        #row_count+=1
        #self.xls_write_row(worksheet, filters, data_pegawai, parser, row_count, footer_pejabat_penilai_template, row_normal_without_border_center_style)
        #row_count+=5
        #self.xls_write_row(worksheet, filters, data_pegawai, parser, row_count, footer_nama_atasan_template, row_normal_without_border_center_style)
        #row_count+=1
        #self.xls_write_row(worksheet, filters, data_pegawai, parser, row_count, footer_nip_atasan_template, row_normal_without_border_center_style)
#END FOOTER--------------------------------------------------           

#end OF LAporan SKP ===========================================================================================================

#DATA TAMBAHAN DAN KREATIFITAS		 DAN PERILAKU
        title_3_template = self.xls_row_template(cols_specs, ['Title_3',])
        title_2_template = self.xls_row_template(cols_specs, ['Title_2',])
        
        sub_target_realisasi_header_info_template = self.xls_row_template(cols_specs_2, ['1','2','3','4','5','6','7','8','9','10','11'])
        row_template = self.xls_row_template(cols_specs_2,row_spec_perilaku_value)
        
        
        row_count = 0
        self.xls_write_row(worksheet_2, filters, None, parser, row_count, title_3_template, title_style)
        row_count+=1
        self.xls_write_row(worksheet_2, filters, None, parser, row_count, title_2_template, title_style)
        
        row_count+=1
        # Write DATA KEPEGAWAIAN
        row_count+=1
        self.xls_write_row(worksheet_2, filters, None, parser, row_count, header_pegawai_template, header_with_border_style)
        #row_count+=1
        #self.xls_write_row_header(worksheet, row_count, header_pegawai_template,header_style, set_column_size=False)
        row_count+=1
        self.xls_write_row(worksheet_2, filters, data_pegawai, parser, row_count, header_nama_pegawai_template, header_normal_style)
        row_count+=1
        self.xls_write_row(worksheet_2, filters, data_pegawai, parser, row_count, header_nip_pegawai_template, header_normal_style)
        row_count+=1
        self.xls_write_row(worksheet_2, filters, data_pegawai, parser, row_count, header_golongan_pegawai_template, header_normal_style)
        row_count+=1
        self.xls_write_row(worksheet_2, filters, data_pegawai, parser, row_count, header_jabatan_pegawai_template, header_normal_style)
        row_count+=1
        self.xls_write_row(worksheet_2, filters, data_pegawai, parser, row_count, header_unit_kerja_pegawai_template, header_normal_style)
        row_count+=1
 #END HEADER PEGAWAI--------------------------------------------------        
        
        
        row_count+=1
        self.xls_write_row_header(worksheet_2, row_count, row_template, header_with_border_style, set_column_size=False)
        row_count+=1
        self.xls_write_row_header(worksheet_2, row_count, sub_target_realisasi_header_info_template, sub_header_style, set_column_size=True)
        row_count+=1
        result_skp_employee = parser.get_recap_monthly_report_raw(filters);
        row_data=row_count
        idx=1	                
        
	    
        for skp_emp_data in result_skp_employee:
	            # Write Rows
	                #employee_data_summary_data.sequence=1
	                self.xls_write_row_with_indeks(worksheet_2, filters, skp_emp_data, parser, row_data, row_template, row_normal_style,idx)
	                row_data+=1
	                idx+=1
        row_count=row_data
        		
 
        
       
    # Override from report_engine_xls.py	
    def create_source_xls(self, cr, uid, ids, filters, report_xml, context=None): 
        if not context: context = {}
	
	# Avoiding context's values change
        context_clone = context.copy()
	
        rml_parser = self.parser(cr, uid, self.name2, context=context_clone)
        objects = self.getObjects(cr, uid, ids, context=context_clone)
        rml_parser.set_context(objects, filters, ids, 'xls')
        io = cStringIO.StringIO()
        workbook = xlwt.Workbook(encoding='utf-8')
        self.generate_xls_report(rml_parser, filters, rml_parser.localcontext['objects'], workbook)
        workbook.save(io)
        io.seek(0)
        return (io.read(), 'xls')

#Start the reporting service
skp_recap_monthly_report_xls_generator(
    #name (will be referred from skp_recap_monthly_report.py, must add "report." as prefix)
    'report.skp.recap.monthly.xls',
    #model
    'skp.recap.monthly.report',
    #file
    'addons/df_skp_recap_monthly_report/report/skp_recap_monthly_report.xls',
    #parser
    parser=skp_recap_monthly_report_parser,
    #header
    header=True
)