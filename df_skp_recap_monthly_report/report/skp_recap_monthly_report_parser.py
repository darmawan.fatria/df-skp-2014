import openerp.pooler
import time
from datetime import datetime
import openerp.tools
import logging
from openerp.report import report_sxw
from datetime import datetime
from openerp.addons.report_webkit import webkit_report
from openerp.tools.translate import _
from openerp.osv import osv
import math
import locale

class skp_recap_monthly_report_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context=None):
        locale.setlocale(locale.LC_ALL, 'id_ID.utf8')
        super(skp_recap_monthly_report_parser, self).__init__(cr, uid, name, context=context)

    def get_skp_recap_monthly_report_raw(self,filters,month_filter,context=None):
        period_year=filters['form']['period_year']
        company_id=filters['form']['company_id']
        user_id = False
        if filters['form']['user_id']:
            user_id=filters['form']['user_id'][0]
        #print "Period : ",month_filter+" "+period_year 
        target_pool = self.pool.get('project.task')
        target_ids=target_pool.search(self.cr, self.uid, [('work_state','in',('done','closed')),('task_category','=','skp'),
                                                          ('target_period_month','=',month_filter),('target_period_year','=',period_year),('user_id','=',user_id)], context=None)
        results = target_pool.browse(self.cr, self.uid, target_ids)
        return results;
    
    def get_atribut_kepegawaian(self,filters,context=None):
        period_year=filters['form']['period_year']
        user_id = False
        data_pegawai=None
        if filters['form']['user_id']:
            user_id=filters['form']['user_id'][0]
        user_pool = self.pool.get('res.users')
        if user_id:
            result = user_pool.browse(self.cr, self.uid, user_id)
            if  result and result.partner_id:
                data_pegawai = result.partner_id
        return data_pegawai;
    
    
    def get_recap_monthly_report_raw(self,filters,context=None):
        period_year=filters['form']['period_year']
        company_id=filters['form']['company_id']
        user_id = False
        if filters['form']['user_id']:
            user_id=filters['form']['user_id'][0]
        skp_monthly_pool = self.pool.get('skp.employee')
        skp_monthly_ids=skp_monthly_pool.search(self.cr, self.uid, [('target_period_year','=',period_year),('user_id','=',user_id)],order='target_period_month', context=None)
        results = skp_monthly_pool.browse(self.cr, self.uid, skp_monthly_ids)
        
        
        return results
        
        
    def get_title(self,prefix,filters,context=None):
        period_year=filters['form']['period_year'];
        return prefix+' '+period_year
    def get_concat_with_format_date(self,str_prefix,filters,context=None):
        print_date=filters['form']['print_date'];
        formatted_print_date = time.strftime('%d %B %Y', time.strptime(print_date,'%Y-%m-%d'))
        return str_prefix+' '+formatted_print_date
    def get_concat_kuantitas_output(self,kuantitas_output,satuan,context=None):
        ret_qty=''
        ret_satuan =''
        if kuantitas_output:
            ret_qty = str(kuantitas_output)
        if satuan and satuan.name :
            ret_satuan = satuan.name
        return ret_qty+' '+ret_satuan
    def get_concat_nilai_perilaku(self,nilai_perilaku,pengali,context=None):
        str_nilai_perilaku=''
        
        if nilai_perilaku:
            str_nilai_perilaku = str(round(nilai_perilaku))
        ret_skp = "%s x %s" % (str_nilai_perilaku, pengali)
        return ret_skp
    def get_concat_nilai_skp(self,nilai_skp,pengali,context=None):
        str_nilai_skp=''
        #str_tugas_tambahan='0'
        #str_kreatifitas='0'
        if nilai_skp:
            str_nilai_skp = str(round(nilai_skp))
        #if tugas_tambahan:
            #str_tugas_tambahan = str(int(tugas_tambahan))
        #if kreatifitas:
            #str_kreatifitas = str(int(kreatifitas))
        ret_skp = "(%s x %s )  " % (str_nilai_skp, pengali,)
        return ret_skp
    def get_period_month_year(self,month,filters):
        period_year=filters['form']['period_year']
        bulan ='';
        
        if month:
            if month == '10':
                bulan = "Oktober"
            elif month == '11':
                bulan = "November"
            elif month == '12':
                bulan = "Desember"
            elif month == '01':
                bulan = "Januari"
            elif month == '02':
                bulan = "Februari"
            elif month == '03':
                bulan = "Maret"
            elif month == '04':
                bulan = "April"
            elif month == '05':
                bulan = "Mei"
            elif month == '06':
                bulan = "Juni"
            elif month == '07':
                bulan = "Juli"
            elif month == '08':
                bulan = "Agustus"
            elif month == '09':
                bulan = "September"
       
        return bulan+' '+period_year
    def get_month_to_char(self,month):
        
        bulan ='';
        
        if month:
            if month == '10':
                bulan = "Oktober"
            elif month == '11':
                bulan = "November"
            elif month == '12':
                bulan = "Desember"
            elif month == '01':
                bulan = "Januari"
            elif month == '02':
                bulan = "Februari"
            elif month == '03':
                bulan = "Maret"
            elif month == '04':
                bulan = "April"
            elif month == '05':
                bulan = "Mei"
            elif month == '06':
                bulan = "Juni"
            elif month == '07':
                bulan = "Juli"
            elif month == '08':
                bulan = "Agustus"
            elif month == '09':
                bulan = "September"
       
        return bulan

