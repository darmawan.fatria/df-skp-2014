from openerp.osv import fields, osv
import time
from mx import DateTime
from tools.translate import _
import tools

class skp_recap_monthly_report(osv.osv_memory):
    
    _name = "skp.recap.monthly.report"
    _columns = {
        'period_year'       : fields.char('Periode Tahun', size=4, required=True),
        'print_date'       : fields.date('Tanggal Pengesahan',  required=True),
        'company_id'        : fields.many2one('res.company', 'OPD',),
        'user_id'        : fields.many2one('res.users', 'Pegawai Yang Dinilai'),
        
    }
    _defaults = {
        'period_year':lambda *args: time.strftime('%Y'),
        'print_date': lambda *args: time.strftime('%Y-%m-%d'),
        'user_id': lambda self, cr, uid, ctx: uid,
    }
    def get_skp_recap_monthly_report(self, cr, uid, ids, context={}):
        value = self.read(cr, uid, ids)[0]
        if value:
            period_year = value['period_year']
            user_id = value['user_id'][0]
            skp_yearly_pool = self.pool.get('skp.employee.yearly.recapitulation')
            skp_yearly_ids=skp_yearly_pool.search(cr, uid, [('target_period_year','=',period_year),('user_id','=',user_id)], context=None)
            if not skp_yearly_ids :
                raise osv.except_osv(_('Laporan Tidak Bisa Di Download'),
                                        _('Belum ada rekapitulasi Tahunan pegawai.'))
        datas = {
            'ids': context.get('active_ids',[]),
            'model': 'skp.recap.monthly.report',
            'form': value
        }
	
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'skp.recap.monthly.xls',
            'report_type': 'webkit',
            'datas': datas,
        }
    
skp_recap_monthly_report()
