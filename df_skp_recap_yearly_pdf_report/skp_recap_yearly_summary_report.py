import time
import netsvc
from tools.translate import _
import tools
from datetime import datetime, timedelta
from osv import fields, osv
from dateutil.relativedelta import relativedelta


import decimal_precision as dp

class skp_recap_yearly_summary_report(osv.osv_memory):
    _name = "skp.recap.yearly.summary.report"
    
    _columns = {
        'period_year'       : fields.char('Periode Tahun', size=4, required=True),
        'print_date'       : fields.date('Tanggal Pengesahan',  required=True),
        'user_id'        : fields.many2one('res.users', 'Pejabat Yang DInilai'),
    } 
    _defaults = {
        'period_year':lambda *args: time.strftime('%Y'),
        'print_date': lambda *args: time.strftime('%Y-%m-%d'),
        'user_id': lambda self, cr, uid, ctx: uid,
    }
    def compute_rep(self, cr, uid, ids, context={}):
        value = self.read(cr, uid, ids)[0]
        
        datas = {
             'ids': context.get('active_ids',[]),
             'model': 'skp.recap.yearly.summary.report',
             'form': value
               }
        
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'skp.recap.yearly.summary.report.form',
            #'report_type': 'webkit',
            'datas': datas,
            }
skp_recap_yearly_summary_report()