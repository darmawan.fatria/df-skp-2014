from openerp.report import report_sxw
import pooler
from datetime import datetime
from dateutil.relativedelta import relativedelta
import time
from operator import itemgetter
from itertools import groupby
from report_webkit import webkit_report
from osv import fields, osv
from tools.translate import _
import netsvc
import tools
import decimal_precision as dp
import math

class skp_recap_yearly_summary_pdf_report(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context=None):
        super(skp_recap_yearly_summary_pdf_report, self).__init__(cr, uid, name, context=context)
        
        self.localcontext.update({
            'get_atribut_kepegawaian' : self.get_atribut_kepegawaian,
            'get_concat_nilai_skp' : self.get_concat_nilai_skp,
            'get_recap_yearly_report_raw' : self.get_recap_yearly_report_raw,
            'parsing_nilai_kepemimpinan':self.parsing_nilai_kepemimpinan,
            'get_rata_rata_nilai_perilaku':self.get_rata_rata_nilai_perilaku,
             'get_concat_nilai_perilaku':self.get_concat_nilai_perilaku,
            'get_concat_with_format_date':self.get_concat_with_format_date, 
            
            
            
        })
        
    def get_atribut_kepegawaian(self,filters,context=None):
        period_year=filters['form']['period_year']
        user_id = False
        data_pegawai=None
        if filters['form']['user_id']:
            user_id=filters['form']['user_id'][0]
        target_pool = self.pool.get('project.project')
        target_ids=target_pool.search(self.cr, self.uid, [('state','in',('confirm','closed')),('target_period_year','=',period_year),('user_id','=',user_id),], context=None)
        if target_ids:
            result = target_pool.browse(self.cr, self.uid, target_ids)[0]
            if  result.user_id and result.employee_id:

                data_pegawai = result.employee_id
        return data_pegawai;
    def get_recap_yearly_report_raw(self,filters,context=None):
        period_year=filters['form']['period_year']
        user_id = False
        if filters['form']['user_id']:
            user_id=filters['form']['user_id'][0]
        skp_yearly_pool = self.pool.get('skp.employee.yearly')
        skp_yearly_ids=skp_yearly_pool.search(self.cr, self.uid, [('target_period_year','=',period_year),('user_id','=',user_id)], context=None)
        results = skp_yearly_pool.browse(self.cr, self.uid, skp_yearly_ids)
        if results:
            return results[0];
        if not results :
            raise osv.except_osv(_('Laporan Tidak Bisa Di Download'),
                                    _('Belum ada rekapitulasi tahunan pegawai.'))
    
    def get_concat_nilai_perilaku(self,nilai_perilaku,pengali,context=None):
        str_nilai_perilaku=''
        
        if nilai_perilaku:
            str_nilai_perilaku = nilai_perilaku
        ret_skp = "%s x %s" % (str_nilai_perilaku, pengali)
        return ret_skp
    def get_concat_nilai_skp(self,nilai_skp,pengali,tugas_tambahan,kreatifitas,context=None):
        str_nilai_skp='-'
        str_tugas_tambahan='0'
        str_kreatifitas='0'
        if nilai_skp:
            str_nilai_skp = nilai_skp
        if tugas_tambahan:
            str_tugas_tambahan = str(int(tugas_tambahan))
        if kreatifitas:
            str_kreatifitas = str(int(kreatifitas))
        ret_skp = "(%s x %s ) + %s + %s " % (str_nilai_skp, pengali,str_tugas_tambahan,str_kreatifitas)
        return ret_skp
    
    def parsing_nilai_kepemimpinan(self,skp_yearly_obj,type):
       val =None
       if skp_yearly_obj and skp_yearly_obj.employee_id and skp_yearly_obj.employee_id.job_type == 'struktural' :
            if type==1 :
                val = skp_yearly_obj.nilai_kepemimpinan
            if type==2 :
                val = skp_yearly_obj.indeks_nilai_kepemimpinan
       return val
    def get_rata_rata_nilai_perilaku(self,skp_yearly_obj):
       val =0
       if not skp_yearly_obj : return 0;
       if skp_yearly_obj and skp_yearly_obj.total_perilaku == 0 : return 0;
       
       if skp_yearly_obj and skp_yearly_obj.employee_id and skp_yearly_obj.employee_id.job_type == 'struktural' :
           val = skp_yearly_obj.total_perilaku/6
       else :
            val = skp_yearly_obj.total_perilaku/5
            
       return val
       
    def get_concat_with_format_date(self,str_prefix,filters,context=None):
        print_date=filters['form']['print_date'];
        try:
            formatted_print_date = time.strftime('%d %B %Y', time.strptime(print_date,'%Y-%m-%d'))
        except:
            formatted_print_date=''
        return str_prefix+' '+formatted_print_date
   
    
report_sxw.report_sxw('report.skp.recap.yearly.summary.report.form',
                        'skp.recap.yearly.summary.report', 
                        'addons/df_skp_recap_yearly_pdf_report/report/skp_recap_yearly_summary_pdf_report.mako', parser=skp_recap_yearly_summary_pdf_report)
# report_sxw.report_sxw('report.penjualan.form', 'report.keuangan', 'addons/ad_laporan_keuangan/report/salesreport.mako', parser=ReportKeu)
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: