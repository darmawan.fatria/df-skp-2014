# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from datetime import date,datetime,timedelta
import time
from mx import DateTime

# ====================== res partner ================================= #

class res_partner(osv.osv):
    _inherit = 'res.partner'
    
    def _current_masa_kerja(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        masa_kerja=0
        for id in ids:
            golongan_ids=self.pool.get('partner.employee.golongan.history').search(cr,uid,[('partner_id','=',id),('jenis','=','pns')],order="date asc,id asc")
            if golongan_ids:
                golongan_hist=self.pool.get('partner.employee.golongan.history').browse(cr,uid,golongan_ids[0])
                if golongan_hist.date :
                    today = date.today()
                    awal_masa_kerja = golongan_hist.date and time.strftime('%Y', time.strptime(golongan_hist.date,'%Y-%m-%d'))
                    masa_kerja = today.year - int(awal_masa_kerja)
            res[id]=masa_kerja
        return res
    def _tahun_pengangkatan_pns(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        tahun_awal_masa_kerja=1900
        for id in ids:
            golongan_ids=self.pool.get('partner.employee.golongan.history').search(cr,uid,[('partner_id','=',id),('jenis','=','pns')],order="date asc,id asc")
            if golongan_ids:
                golongan_hist=self.pool.get('partner.employee.golongan.history').browse(cr,uid,golongan_ids[0])
                if golongan_hist.date :
                    today = date.today()
                    tahun_awal_masa_kerja = golongan_hist.date and time.strftime('%Y', time.strptime(golongan_hist.date,'%Y-%m-%d'))
            res[id]=tahun_awal_masa_kerja
        return res
    def _sk_pengangkatan_pns(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        no_sk='-'
        for id in ids:
            golongan_ids=self.pool.get('partner.employee.golongan.history').search(cr,uid,[('partner_id','=',id),('jenis','=','pns')],order="date asc,id asc")
            if golongan_ids:
                golongan_hist=self.pool.get('partner.employee.golongan.history').browse(cr,uid,golongan_ids[0])
                if golongan_hist.name :
                    no_sk = golongan_hist.name
            res[id]=no_sk
        return res
    def _current_golongan_id(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        golongan_id=False
        for id in ids:
            golongan_ids=self.pool.get('partner.employee.golongan.history').search(cr,uid,[('partner_id','=',id)],order="date desc,id desc")
            if golongan_ids:
                golongan_hist=self.pool.get('partner.employee.golongan.history').browse(cr,uid,golongan_ids[0])
                if golongan_hist.golongan_id_history :
                    golongan_id= golongan_hist.golongan_id_history.id
            res[id]=golongan_id
        return res
    def _current_tahun_golongan_id(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        current_tahun=1900
        for id in ids:
            golongan_ids=self.pool.get('partner.employee.golongan.history').search(cr,uid,[('partner_id','=',id)],order="date desc,id desc")
            if golongan_ids:
                golongan_hist=self.pool.get('partner.employee.golongan.history').browse(cr,uid,golongan_ids[0])
                if golongan_hist.date :
                    current_tahun = golongan_hist.date and time.strftime('%Y', time.strptime(golongan_hist.date,'%Y-%m-%d'))
            res[id]=current_tahun
        return res
    def _current_sk_golongan_id(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        no_sk='-'
        for id in ids:
            golongan_ids=self.pool.get('partner.employee.golongan.history').search(cr,uid,[('partner_id','=',id)],order="date desc,id desc")
            if golongan_ids:
                golongan_hist=self.pool.get('partner.employee.golongan.history').browse(cr,uid,golongan_ids[0])
                if golongan_hist.name :
                    no_sk = golongan_hist.name
            res[id]=no_sk
        return res
    def _current_jenjang_pendidikan_id(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        lookup_pool=self.pool.get('partner.employee.study.history')
        pendidikan_id=False
        for id in ids:
            pendidikan_ids=lookup_pool.search(cr,uid,[('partner_id','=',id),],order="tahun_lulus desc,id desc")
            if pendidikan_ids:
                pendidikan_hist=lookup_pool.browse(cr,uid,pendidikan_ids[0])
                if  pendidikan_hist.jenjang_pendidikan_id_history :
                    pendidikan_id=  pendidikan_hist.jenjang_pendidikan_id_history.id
            res[id]=pendidikan_id
        return res
    def _current_nama_sekolah_id(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        lookup_pool=self.pool.get('partner.employee.study.history')
        pendidikan_id=False
        for id in ids:
            pendidikan_ids=lookup_pool.search(cr,uid,[('partner_id','=',id)],order="tahun_lulus desc,id desc")
            if pendidikan_ids:
                pendidikan_hist=lookup_pool.browse(cr,uid,pendidikan_ids[0])
                if  pendidikan_hist.nama_sekolah_id_history :
                    pendidikan_id=  pendidikan_hist.nama_sekolah_id_history.id
            res[id]=pendidikan_id
        return res
    def _current_tahun_pendidikan(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        lookup_pool=self.pool.get('partner.employee.study.history')
        tahun_lulus=1900
        for id in ids:
            pendidikan_ids=lookup_pool.search(cr,uid,[('partner_id','=',id)],order="tahun_lulus desc,id desc")
            if pendidikan_ids:
                pendidikan_hist=lookup_pool.browse(cr,uid,pendidikan_ids[0])
                if  pendidikan_hist.tahun_lulus :
                    tahun_lulus=  pendidikan_hist.tahun_lulus
            res[id]=tahun_lulus
        return res
    def _current_jurusan_id(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        lookup_pool=self.pool.get('partner.employee.study.history')
        pendidikan_id=False
        for id in ids:
            pendidikan_ids=lookup_pool.search(cr,uid,[('partner_id','=',id)],order="tahun_lulus desc,id desc")
            if pendidikan_ids:
                pendidikan_hist=lookup_pool.browse(cr,uid,pendidikan_ids[0])
                if  pendidikan_hist.jurusan_id_history :
                    pendidikan_id=  pendidikan_hist.jurusan_id_history.id
            res[id]=pendidikan_id
        return res
    def _current_hukuman_disiplin(self, cr, uid, ids, name, arg, context=None):
        #jika True = Tidak Terkena Hukuman Disiplin, False = Terkena Hukuman Disiplin
        if context is None:
            context = {}
        res = {}
        lookup_pool=self.pool.get('partner.employee.hukuman.disiplin.history')
        status_hukuman_disiplin=True
        for id in ids:
            today = date.today()
            hukuman_disiplin_ids=lookup_pool.search(cr,uid,[('partner_id','=',id),('date_start','<=',today),('date_end','>=',today)])
            if hukuman_disiplin_ids:
                hukuman_hist=lookup_pool.browse(cr,uid,hukuman_disiplin_ids[0])
                if  hukuman_hist:
                    status_hukuman_disiplin=  False
            res[id]=status_hukuman_disiplin
        return res
    def _current_nilai_skp(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        lookup_pool=self.pool.get('skp.employee.yearly')
        today = date.today()
        for id in ids:
            nilai_skp=0.0
            skp_yearly_ids=lookup_pool.search(cr,uid,[('employee_id','=',id),('target_period_year','=',today.year)],order="target_period_year desc,id desc")
            if skp_yearly_ids:
                skp_yearly_obj=lookup_pool.browse(cr,uid,skp_yearly_ids[0])
                if  skp_yearly_obj:
                    nilai_skp=  skp_yearly_obj.nilai_total
            res[id]=nilai_skp
        return res
    def _last_year_nilai_skp(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        lookup_pool=self.pool.get('skp.employee.yearly')
        today = date.today()
        for id in ids:
            nilai_skp=0.0
            skp_yearly_ids=lookup_pool.search(cr,uid,[('employee_id','=',id),('target_period_year','=',(today.year-1))],order="target_period_year desc,id desc")
            if skp_yearly_ids:
                skp_yearly_obj=lookup_pool.browse(cr,uid,skp_yearly_ids[0])
                if  skp_yearly_obj:
                    nilai_skp=  skp_yearly_obj.nilai_total
            res[id]=nilai_skp
        return res
    def _last_year_tahun_skp(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        lookup_pool=self.pool.get('skp.employee.yearly')
        today = date.today()
        for id in ids:
            nilai_skp=0.0
            skp_yearly_ids=lookup_pool.search(cr,uid,[('employee_id','=',id),('target_period_year','=',(today.year-1))],order="target_period_year desc,id desc")
            if skp_yearly_ids:
                skp_yearly_obj=lookup_pool.browse(cr,uid,skp_yearly_ids[0])
                if  skp_yearly_obj:
                    nilai_skp=  skp_yearly_obj.target_period_year
            res[id]=nilai_skp
        return resz
    
    #jabatan default
    def _current_job_type(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        
        val=None
        for id in ids:
            hist_ids=self.pool.get('partner.employee.job.history').search(cr,uid,[('partner_id','=',id)],order="date desc,id desc")
            if hist_ids:
                hist_obj=self.pool.get('partner.employee.job.history').browse(cr,uid,hist_ids[0])
                if hist_obj and hist_obj.job_type :
                    val = hist_obj.job_type
            res[id]=val
        return res
    def _current_eselon_id(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        val=None
        for id in ids:
            hist_ids=self.pool.get('partner.employee.job.history').search(cr,uid,[('partner_id','=',id)],order="date desc,id desc")
            if hist_ids:
                hist_obj=self.pool.get('partner.employee.job.history').browse(cr,uid,hist_ids[0])
                if hist_obj and hist_obj.eselon_id :
                    val = hist_obj.eselon_id.id
            res[id]=val
        return res
    def _current_biro_id(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        val=None
        for id in ids:
            hist_ids=self.pool.get('partner.employee.job.history').search(cr,uid,[('partner_id','=',id)],order="date desc,id desc")
            if hist_ids:
                hist_obj=self.pool.get('partner.employee.job.history').browse(cr,uid,hist_ids[0])
                if hist_obj and hist_obj.biro_id :
                    val = hist_obj.biro_id.id
            res[id]=val
        return res
    def _current_job_id(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        val=None
        for id in ids:
            hist_ids=self.pool.get('partner.employee.job.history').search(cr,uid,[('partner_id','=',id)],order="date desc,id desc")
            if hist_ids:
                hist_obj=self.pool.get('partner.employee.job.history').browse(cr,uid,hist_ids[0])
                if hist_obj and hist_obj.job_id_history :
                    val = hist_obj.job_id_history.id
            res[id]=val
        return res
    def _current_department_id(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        val=None
        for id in ids:
            hist_ids=self.pool.get('partner.employee.job.history').search(cr,uid,[('partner_id','=',id)],order="date desc,id desc")
            if hist_ids:
                hist_obj=self.pool.get('partner.employee.job.history').browse(cr,uid,hist_ids[0])
                if hist_obj and hist_obj.department_id :
                    val = hist_obj.department_id.id
            res[id]=val
        return res
    
    job_type_list =[('struktural', 'Jabatan Struktural'), ('jft', 'Jabatan Fungsional Tertentu'), ('jfu', 'Jabatan Fungsional Umum')]
    _columns = {
        'department_id': fields.function(_current_department_id, method=True,readonly=True , store=True,
                                         type="many2one",relation='hr.department', string='Unit Kerja'),
        'job_id': fields.function(_current_job_id, method=True,readonly=True , store=True,
                                         type="many2one",relation='hr.job', string='Jabatan'),
        'biro_id': fields.function(_current_biro_id, method=True,readonly=True , store=True,
                                         type="many2one",relation='hr.employee.biro', string='Biro'),
        'eselon_id': fields.function(_current_eselon_id, method=True,readonly=True , store=True,
                                         type="many2one",relation='hr.employee.eselon', string='Eselon'),
        'job_type': fields.function(_current_job_type, method=True,readonly=True , store=True,
                                         type="selection",selection=job_type_list, string='Jenis Jabatan'),
        'job_id_history': fields.one2many('partner.employee.job.history','partner_id', 'Riwayat Jabatan'),
        
        'golongan_id': fields.function(_current_golongan_id, method=True,readonly=True , store=True,
                                         type="many2one",relation='hr.employee.golongan', string='Pangkat/Golongan'),
        'golongan_id_history': fields.one2many('partner.employee.golongan.history','partner_id', 'Riwayat Kepangkatan'), 
        'jenjang_pendidikan_id': fields.function(_current_jenjang_pendidikan_id, method=True,readonly=True , store=True,
                                         type="many2one",relation='partner.employee.study.type', string='Tingkat Pendidikan'),
        'nama_sekolah': fields.function(_current_nama_sekolah_id, method=True,readonly=True , store=True,
                                         type="many2one",relation='hr.employee.school', string='Sekolah'),
        'jurusan': fields.function(_current_jurusan_id, method=True,readonly=True , store=True,
                                         type="many2one",relation='hr.employee.study', string='Jurusan Pendidikan'),
        'tahun_lulus': fields.function(_current_tahun_pendidikan, method=True,readonly=True , store=True,
                                         type="char", string='Tahun Lulus'),
        'jenjang_pendidikan_id_history': fields.one2many('partner.employee.study.history','partner_id', 'Riwayat Kepangkatan'),
        'masa_kerja': fields.function(_current_masa_kerja, method=True,readonly=True , store=True,
                                         type="integer",string='Masa Kerja'),
        'tahun_pengangkatan_pns': fields.function(_tahun_pengangkatan_pns, method=True,readonly=True , store=True,
                                         type="char",string='Tahun Pengangkatan PNS'),
        'sk_pengangkatan_pns': fields.function(_sk_pengangkatan_pns, method=True,readonly=True , store=True,
                                         type="char",string='NO SK Pengangkatan PNS'),
        'current_tahun_golongan_id': fields.function(_current_tahun_golongan_id, method=True,readonly=True , store=True,
                                         type="char",string='Tahun SK Pangkat Terakhir'),
        'current_sk_golongan_id': fields.function(_current_sk_golongan_id, method=True,readonly=True , store=True,
                                         type="char",string='NO SK Pangkat Terakhir'),
        'status_hukuman_disiplin': fields.function(_current_hukuman_disiplin, method=True,readonly=True , store=True,
                                         type="boolean", string='Status Tidak Ada Hukuman Disiplin'),
        'hukuman_disiplin_id_history': fields.one2many('partner.employee.hukuman.disiplin.history','partner_id', 'Riwayat Hukuman Disiplin'),
        'nilai_skp': fields.function(_current_nilai_skp, method=True,readonly=True , store=False,
                                         type="float", string='Nilai SKP'),
        'last_year_nilai_skp': fields.function(_last_year_nilai_skp, method=True,readonly=True , store=False,
                                         type="float", string='Nilai SKP Tahun Lalu'),
        'last_year_tahun_skp': fields.function(_current_nilai_skp, method=True,readonly=True , store=False,
                                         type="char", string='Tahun Terakhir SKP'),
        'skp_employee_yearly_ids': fields.one2many('skp.employee.yearly','employee_id', 'Nilai SKP Tahunan',readonly=True ), 
        #diklat
        'diklat_kepemimpinan_id_history': fields.one2many('partner.employee.diklat.kepemimpinan.history','partner_id',string='Riwayat Diklat Kepemimpinan'),
        'diklat_fungsional_id_history': fields.one2many('partner.employee.diklat.fungsional.history','partner_id', string='Riwayat Diklat Fungsional'),
        'diklat_teknik_id_history': fields.one2many('partner.employee.diklat.teknik.history','partner_id' ,string='Riwayat Diklat Teknik/Substantif'),
        'bidang_kompetensi_id_history': fields.one2many('partner.employee.bidang.kompetensi.history','partner_id' ,string='Riwayat Bidang Kompetensi'),
    }
     
   
res_partner()
