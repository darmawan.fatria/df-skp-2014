# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from datetime import datetime,timedelta
import time
from mx import DateTime

class partner_employee_study_degree(osv.osv):
    _name = "partner.employee.study.degree"
    _description = "Level Jenjang Pendidikan"
    _columns = {
        'name': fields.char("Level Pendidikan", size=100,required=True ),
        'degree_level': fields.integer("Nilai Level", required=True ),
        'description': fields.text("Deskripsi"),
                
    }
    def _check_unique_name_insesitive(self, cr, uid, ids, context=None):
        sr_ids = self.search(cr, 1 ,[], context=context)
        lst = [
                x.name.lower().strip() for x in self.browse(cr, uid, sr_ids, context=context)
                if x.name and x.id not in ids
              ]
        for self_obj in self.browse(cr, uid, ids, context=context):
            if self_obj.name and self_obj.name.lower() and self_obj.name.strip() and self_obj.name.lower().strip() in  lst:
                return False
        return True

    _constraints = [(_check_unique_name_insesitive, 'Error: Nama Sudah Tersedia ', ['name'])]
partner_employee_study_degree()
class partner_employee_study_type(osv.osv):
    _name = "partner.employee.study.type"
    _description = "Jenjang Pendidikan"
    _columns = {
        'name': fields.char("Jenjang Pendidikan", size=25,required=True ),
        'level_pendidikan_id': fields.many2one('partner.employee.study.degree', 'Jenjang Pendidikan',required=True),
        'description': fields.text("Deskripsi"),
                
    }
    def _check_unique_name_insesitive(self, cr, uid, ids, context=None):
        sr_ids = self.search(cr, 1 ,[], context=context)
        lst = [
                x.name.lower().strip() for x in self.browse(cr, uid, sr_ids, context=context)
                if x.name and x.id not in ids
              ]
        for self_obj in self.browse(cr, uid, ids, context=context):
            if self_obj.name and self_obj.name.lower() and self_obj.name.strip() and self_obj.name.lower().strip() in  lst:
                return False
        return True

    _constraints = [(_check_unique_name_insesitive, 'Error: Nama Sudah Tersedia ', ['name'])]
partner_employee_study_type()

class hr_employee_golongan(osv.osv):
    _inherit = "hr.employee.golongan"
    
    _columns = {
        'name': fields.char("Golongan/Ruang", size=12, required=True),
        'golongan': fields.integer("Golongan", required=True ),
        'ruang': fields.char("Ruang", size=12, ),
        'description': fields.char("Deskripsi", size=256, ),
        'level': fields.integer("Level", required=True ),
    }
    
hr_employee_golongan()