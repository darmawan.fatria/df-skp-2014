# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from datetime import datetime,timedelta
import time
from mx import DateTime
class partner_employee_golongan_history(osv.osv):
    _name = "partner.employee.golongan.history"
    _description = "Riwayat Kepangkatan"
    _columns = {
        'name': fields.char("No SK", size=100,required=True ),
        'partner_id': fields.many2one('res.partner', 'Data Pegawai',required=True),
        'golongan_id_history': fields.many2one('hr.employee.golongan', 'Golongan',required=True),
        'date': fields.date("TMT.Pangkat", required=True ),
        'jenis'     : fields.selection([('cpns', 'CPNS'), 
                                             ('pns', 'PNS'),
                                             ('pangkat_golongan', 'Kenaikan Pangkat/Golongan'),
                                            ], 'Jenis SK', required=True),
                
    }
    _order = "date desc"
partner_employee_golongan_history()
class partner_employee_study_history(osv.osv):
    _name = "partner.employee.study.history"
    _description = "Riwayat Pendidikan"
    _columns = {
        'name': fields.char("No Ijazah", size=100, ),
        'partner_id': fields.many2one('res.partner', 'Data Pegawai',required=True),
        'nama_sekolah_id_history': fields.many2one('hr.employee.school', 'Sekolah',required=True),
        'jurusan_id_history': fields.many2one('hr.employee.study', 'Jurusan',),
        'jenjang_pendidikan_id_history': fields.many2one('partner.employee.study.type', 'Jenjang Pendidikan',required=False,),
        'tahun_lulus': fields.char("Tahun Lulus", size=4 ,required=True),
        'ipk': fields.float("IPK", ),
        'attachment_file': fields.binary("Upload Ijazah", ),
                
    }
    _order = "tahun_lulus desc"
    
partner_employee_study_history()
class partner_employee_hukuman_disiplin_history(osv.osv):
    _name = "partner.employee.hukuman.disiplin.history"
    _description = "Riwayat Hukuman Disiplin"
    _columns = {
        'partner_id': fields.many2one('res.partner', 'Data Pegawai',required=True),
        'date_start': fields.date("TMT. Hukuman", required=True ),
        'date_end': fields.date("Tgl AKhir Hukuman", required=True ),
        'name'     : fields.selection([('ringan', 'Ringan'), 
                                             ('sedang', 'Sedang'),
                                             ('berat', 'Berat'),
                                            ], 'Jenis Hukuman Disiplin', required=True),
        'notes': fields.text("Uraian",required=True ),
    }
    _order = "date_start desc"
partner_employee_hukuman_disiplin_history()
class partner_employee_job_history(osv.Model):
    _name = "partner.employee.job.history"
    _description = "Riwayat Jabatan"
    _columns = {
        'job_type': fields.selection([('struktural', 'Jabatan Struktural'), ('jft', 'Jabatan Fungsional Tertentu'), ('jfu', 'Jabatan Fungsional Umum')], 'Jenis Jabatan',required=True),
        'eselon_id': fields.many2one('hr.employee.eselon', 'Eselon'),
        'job_id_history': fields.many2one('hr.job', 'Jabatan',required=True),
        'department_id': fields.many2one('hr.department', 'Unit Kerja'),
        'biro_id': fields.many2one('hr.employee.biro', 'Biro', ),
        'date': fields.date("TMT. Jabatan", required=True ),
        'name': fields.char("No SK", size=100,required=False ),
        'company_name': fields.char("Nama OPD", size=150,),
        'instansi_name': fields.char("Nama Instansi", size=150, ),
        'partner_id': fields.many2one('res.partner', 'Data Pegawai',required=True),
        
        
                
    }
    _order = "date desc"
partner_employee_job_history()

class partner_employee_diklat_kepemimpinan_history(osv.Model):
    _name = "partner.employee.diklat.kepemimpinan.history"
    _description = "Riwayat Diklat Kepemimpinan"
    _columns = {
        'name': fields.char("Nama", size=500,required=True ),
        'partner_id': fields.many2one('res.partner', 'Data Pegawai',required=True),
        'tahun': fields.char("Tahun Lulus", size=4),
        'tempat': fields.char("Tempat", size=120),
        'penyelenggara': fields.char("Penyelenggara", size=320),
        'type_id': fields.many2one('diklat.type', 'Jenjang',domain="[('type','=','kepemimpinan')]" ),
    }
    _order = "tahun desc"
partner_employee_diklat_kepemimpinan_history()

class partner_employee_diklat_fungsional_history(osv.Model):
    _name = "partner.employee.diklat.fungsional.history"
    _description = "Riwayat Diklat Fungsional"
    _columns = {
        'name': fields.char("No SK", size=50,required=True ),
        'partner_id': fields.many2one('res.partner', 'Data Pegawai',required=True),
        'tahun': fields.char("Tahun", size=4,required=True),
        'tempat': fields.char("Tempat", size=120),
        'penyelenggara': fields.char("Penyelenggara", size=320),
        'type_id': fields.many2one('diklat.type', 'Jenjang',domain="[('type','=','fungsional')]" ),
    }
    _order = "tahun desc"
partner_employee_diklat_fungsional_history()

class partner_employee_diklat_teknik_history(osv.Model):
    _name = "partner.employee.diklat.teknik.history"
    _description = "Riwayat Diklat Teknik"
    _columns = {
        'name': fields.char("No SK", size=50,required=True ),
        'partner_id': fields.many2one('res.partner', 'Data Pegawai',required=True),
        'tahun': fields.char("Tahun", size=4),
        'tempat': fields.char("Tempat", size=120),
        'penyelenggara': fields.char("Penyelenggara", size=320),
        'type_id': fields.many2one('diklat.type', 'Jenjang',domain="[('type','=','teknik')]" ),
    }
    _order = "tahun desc"
partner_employee_diklat_teknik_history()


class partner_employee_bidang_kompetensi_history(osv.Model):
    _name = "partner.employee.bidang.kompetensi.history"
    _description = "Riwayat Bidang Kompetensi"
    _columns = {
        'name': fields.char("No SK", size=50,required=True ),
        'partner_id': fields.many2one('res.partner', 'Data Pegawai',required=True),
        'date': fields.date("TMT.Kompetensi", ),
        'bidang_kompetensi_type_id': fields.many2one('bidang.kompetensi.type', 'Bidang Kompetensi'),
        'type_id': fields.many2one('diklat.type', 'Jenjang',domain="[('type','=','kompetensi')]" ),
    }
    _order = "date desc"
partner_employee_bidang_kompetensi_history()

class partner_employee_diklat_type(osv.Model):
    _name = "diklat.type"
    _description = "Jenjang Jenis Diklat"
    _columns = {
        'name': fields.char("Jenjang Diklat", size=250,required=True ),
        'type': fields.selection([('kepemimpinan', 'Kepemimpinan'), 
                                  ('fungsional', 'Fungsional'), 
                                  ('teknik', 'Teknik'),
                                  ('kompetensi', 'Kompetensi'),], 'Tipe Diklat',required=True),
                
    }
partner_employee_diklat_type()

class partner_employee_bidang_kompetensi_type(osv.Model):
    _name = "bidang.kompetensi.type"
    _description = "Jenis Bidang Kompetensi"
    _columns = {
        'code': fields.char("Kode", size=5,required=True ),
        'name': fields.char("Bidang Kompetensi", size=250,required=True ),
    }

partner_employee_bidang_kompetensi_type()

